# Dungeons of Derp

## Technical overview

All of the firebase code was written for firebase 2, which was finally turned
down recently. It hasn't been hooked up again, so none of it works.

I did a bunch of modernizing recently, and it is still in progress.  Switched
over to es6 modules instead of namespace-plus.

This turned on strict mode which is catching a lot of our errors.  If you see
one, please file an issue with the stack trace.

Everything in src/\*\*/\* is es6, transpiled to es5 with babel, and combined using
browserify.  src/ is for raw source, lib/ is for post-babel code, dist/ is for
final scripts, public/ is what is visible to the client.  src/\*\*/\*.js is run
through babel and outputted to lib/\*\*/\*.js.  Those files are then combined into
dist/bundle.js, and copied into public/bundle.js.  See Makefile for more
details.

The only thing that's mostly guaranteed to work is public/index.html which runs
the stuff in src/\*\*/\*.

Test coverage is practically non-existant.  There are old tests in test.html,
but they were failing and I haven't gotten them working again.  I am putting more
tests in src/test/*.

## Developer setup

Linux / mac:

Make sure you have node and npm.

# **Remember to source the tools**

```source tools/use``` or ```. tools/use``` from the main project directory.

Now run

```
configure-project
```

then

```
make all
```

### Commands

- run-tests

duh

- reformat

Runs clang format on every js file in src/.  Do this whenever, but most
imporantly before committing your code.

- make all

Builds and bundles everything

Open public/index.html in a web browser to test locally.

### Deploying

```firebase use prod```

```firebase deploy```

## Notes for devs

Views should never update themselves unless they are given a turn.  They can listen to events, but
only to mark stuff for redraw.

### File ordering

Library imports
Other file imports
Constants
Exported variables
Not exported variables
Exported functions
Not exported functions

### Constants

* Put constants one may want to tweak in constants.js
* Put constants used in a file at the top of the file after imports
* Constants are in SCREAMING_SNAKE_CASE
* Constants should probably not be shared between files, kinda weird.

