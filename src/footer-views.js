import * as Backbone from 'backbone';
import $ from 'jquery';
import * as _ from 'underscore';

import {gl} from './globals';
import log from './log';
import * as utils from './utils';

var FOOTER_HEIGHT = 114;
var HFBW = 161; // hero footer bar width

var StatBar = Backbone.View.extend({
  tagName : 'div',
  className : 'barHolder',
  template : _.template($('#stat-bar-template').html(), utils),

  height : 20,
  width : HFBW,
  fontSize : 14,
  fontColor : 'rgba(170, 170, 160, 1)',
  bgColor : '#f00',

  initialize : function(options) {
    this.$el.html(this.template);
    this.$bar = this.$('.bar');
    this.$text = this.$('.text');

    this.$el.css({width : this.width, height : this.height});
    this.$bar.css({'background-color' : this.bgColor, 'width' : 0});
    this.$text.css({
      color : this.fontColor,
      'font-size' : this.fontSize,
      'line-height' : this.height + 'px'
    });

    this.lastWidth = 0;
  },

  _render : function(cur, max) {
    if (cur < 0) {
      cur = 0;
    }
    this.$text.html(this.getText(cur, max));
    this.setWidth(cur, max);
    return this;
  },

  setWidth : function(cur, max) {
    var nw = Math.floor(this.width * cur / max);
    if (nw !== this.lastWidth) {
      this.lastWidth = nw;
      this.$bar.css('width', nw);
    }
  },

  getText : function(
      cur,
      max) { return utils.prettifyNum(cur) + '/' + utils.prettifyNum(max); }
});

var HpBar = StatBar.extend({
  bgColor : '#B22222',
  render :
      function() { return this._render(this.model.hp, this.model.spec.maxHp); },
});

var ManaBar = StatBar.extend({
  bgColor : '#0000AB',
  render : function() {
    return this._render(this.model.mana, this.model.spec.maxMana);
  },
});

var XpBar = StatBar.extend({
  bgColor : '#B8860B',
  width : 453,
  render : function() {
    return this._render(this.model.spec.xp, this.model.spec.getNextLevelXp());
  },
});

var ZoneBar = StatBar.extend({
  fontSize : 14,
  bgColor : '#B8660B',
  render : function() {
    return this._render(this.model.heroPos, this.model.rooms.length);
  },
  getText :
      function() { return this.model.nameStr + ' (' + this.model.level + ')'; }
});

var NameLevelView = Backbone.View.extend({
  tagName : 'div',
  className : 'name-level',
  render : function() {
    this.$el.html('<div class="name"><div>' + this.model.name +
                  '</div></div><div class="level">' + this.model.level +
                  '</div>');
    this.$('.name').css('width', HFBW - 5 - this.$('.level').width());
    return this;
  },
});

var VitalsView = Backbone.View.extend({
  tagName : 'div',
  className : 'vitals',

  initialize : function(options, hero, zone) {
    this.hero = hero;
    this.zone = zone;
    this.nameLevel = new NameLevelView({model : this.hero.spec});
    this.hpBar = new HpBar({model : this.hero});
    this.manaBar = new ManaBar({model : this.hero});
    this.zoneBar = new ZoneBar({model : this.zone});

    this.listenTo(gl.DirtyListener, 'rename',
                  this.nameLevel.render.bind(this.nameLevel));
    this.listenTo(gl.DirtyListener, 'hero:hp',
                  this.hpBar.render.bind(this.hpBar));
    this.listenTo(gl.DirtyListener, 'hero:mana',
                  this.manaBar.render.bind(this.manaBar));
    this.listenTo(gl.DirtyListener, 'zone:new',
                  this.zoneBar.render.bind(this.zoneBar));
    this.listenTo(gl.DirtyListener, 'zone:nextRoom',
                  this.zoneBar.render.bind(this.zoneBar));

    gl.DirtyQueue.mapMark([ 'hero:levelup', 'revive', 'computeAttrs' ],
                          'heroFooterRender');
    this.listenTo(gl.DirtyListener, 'heroFooterRender', this.render);

    this.renderedOnce = false;
  },

  render : function() {
    if (!this.renderedOnce) {
      this.renderedOnce = true;
      this.$el.append(this.nameLevel.render().el);
      this.$el.append(this.hpBar.render().el);
      this.$el.append(this.manaBar.render().el);
      this.$el.append(this.zoneBar.render().el);
    } else {
      this.nameLevel.render();
      this.hpBar.render();
      this.manaBar.render();
      this.zoneBar.render();
    }
    return this;
  },
});

var SkillView = Backbone.View.extend({
  tagName : 'div',
  className : 'skill',
  template : _.template($('#skill-footer-template').html(), utils),

  events : {'mouseenter' : 'onMouseenter', 'mouseleave' : 'onMouseleave'},

  onMouseenter : function() {
    gl.UIEvents.trigger('itemSlotMouseenter', {model : this.model.spec});
  },

  onMouseleave : function() { gl.UIEvents.trigger('itemSlotMouseleave'); },

  initialize : function(options, hero) {
    this.hero = hero;
    this.listenTo(gl.DirtyListener, 'tick', this.adjust);
  },

  adjust : function() {
    var SIZE = 73;
    var cdHeight = 0;
    var useWidth = 0;

    if (this.model.coolAt > gl.time) {
      var durPct = (this.hero.nextAction - gl.time) / this.hero.lastDuration;

      // cooling down but doesn't have cooldown, must be last used
      if (this.model.spec.cooldownTime === 0) {
        useWidth = durPct; // grep in use wipe while being in use
        cdHeight = 0;      // red no cooldown wipe
      } else {
        cdHeight = (this.model.coolAt - gl.time) / this.model.spec.cooldownTime;
        if (cdHeight > 1) { // if in use and has cooldown, cap cooldown wipe
                            // height, grey in use wipe
          useWidth = durPct;
          cdHeight = 1;
        } else {
          useWidth = 0; // if just cooling down, no in use wipe
        }
      }
      useWidth *= SIZE;
      cdHeight *= SIZE;
    }

    if (this.model.oom) {
      this.$el.addClass('oom')
    } else {
      this.$el.removeClass('oom');
    }
    if (this.model.oor) {
      this.$el.addClass('oor')
    } else {
      this.$el.removeClass('oor');
    }

    this.$cd.css('height', cdHeight);
    this.$use.css('width', useWidth);
  },

  render : function() {
    this.$el.html(this.template(Object.assign({}, this, utils)));
    this.$cd = this.$('.cooldown');
    this.$use = this.$('.use-bar');
    this.adjust();
    return this;
  }
});

var SkillchainView = Backbone.View.extend({
  tagName : 'div',
  className : 'skillchain',

  initialize : function(options, hero) {
    this.hero = hero;
    this.views = [];
    this.listenTo(gl.DirtyListener, 'bodySkillchainUpdated', this.render);
  },

  render : function() {
    this.$el.empty();

    _.invoke(this.views, 'remove');

    this.views = _.map(
        this.hero.skills,
        function(model) { return new SkillView({model : model}, this.hero); },
        this);
    var frag = document.createDocumentFragment();
    _.each(this.views, function(view) { frag.appendChild(view.render().el); });
    this.$el.append(frag);

    return this;
  }
});

var PotionView = Backbone.View.extend({
  tagName : 'div',
  className : 'potion-holder',
  template : _.template($('#potion-template').html(), utils),

  events : {'mousedown' : 'use'},

  initialize : function(options, hero) {
    this.hero = hero;
    this.listenTo(gl.DirtyListener, 'tick', this.adjust);
  },

  use : function() { this.hero.tryUsePotion(); },

  adjust : function() {
    var SIZE = 73;
    var pct = (this.hero.potionCoolAt - gl.time) / 10000;
    this.$cd.css('height', pct * SIZE);
  },

  render : function() {
    this.$el.html(this.template);
    this.$cd = this.$('.cooldown');
    return this;
  }
});

var CenterView = Backbone.View.extend({
  tagName : 'div',
  className : 'footer-center',

  initialize : function(options, game) {
    this.renderedOnce = false;

    this.skillchainView = new SkillchainView({}, game.zone.hero);
    this.potionView = new PotionView({}, game.zone.hero);
    this.xpBar = new XpBar({model : game.zone.hero});

    this.listenTo(gl.DirtyListener, 'hero:xp',
                  this.xpBar.render.bind(this.xpBar));

    this.listenTo(gl.DirtyListener, 'throttledResize', this.resize);
    this.resize();
  },

  render : function() {
    if (!this.renderedOnce) {
      this.$el.append(this.xpBar.render().el);
      this.$el.append(this.skillchainView.render().el);
      this.$el.append(this.potionView.render().el);
      this.renderedOnce = true;
    } else {
      this.xpBar.render();
      this.skillchainView.render();
      this.potionView.render();
    }
    return this;
  },

  resize : function() {
    this.$el.css('left', Math.floor(window.innerWidth / 2 - 456 / 2));
  }
});

var DropAreaView = Backbone.View.extend({
  tagName : 'div',
  className : 'drop-area',
  template : _.template($('#item-drop-area-template').html(), utils),

  initialize : function(options, game, itdh, ctdh, craftTab) {
    this.equipped = game.hero.equipped;
    this.skillchain = game.hero.skillchain;
    this.craftTab = craftTab;

    this.listenTo(itdh, 'dragstart', this.onItemDragStart);
    this.listenTo(ctdh, 'dragstart', this.onCardDragStart);
    this.listenTo(itdh, 'drop', this.onItemDrop);
    this.listenTo(ctdh, 'drop', this.onCardDrop);
  },

  onItemDragStart : function() {
    this.$el.css('display', 'block');
    this.$('.craft-drop').css('display', 'none');
  },

  onCardDragStart : function() {
    this.$el.css('display', 'block');
    this.$('.craft-drop').css('display', 'block');
  },

  onItemDrop : function(pos, model) {
    // log.warning('on item drop');
    this.checkRecycleDrop(pos, model);
    this.$el.css('display', 'none');
  },

  onCardDrop : function(pos, model) {
    var off = this.$('.craft-drop').offset();
    if (pos.x > off.left && pos.y > off.top && pos.x < off.left + 73 &&
        pos.y < off.top + 73) {
      gl.UIEvents.trigger('footer:buttons:craft');
      this.craftTab.forceFocus(model);
    } else {
      this.checkRecycleDrop(pos, model);
    }
    this.$el.css('display', 'none');
  },

  checkRecycleDrop : function(pos, model) {
    var off = this.$('.recycle-drop').offset();
    if (pos.x > off.left && pos.y > off.top && pos.x < off.left + 73 &&
        pos.y < off.top + 73) {
      model.inRecycle = true;
      gl.DirtyQueue.mark('recycleChange');
      this.craftTab.forceDeselect(model);
    }
  },

  render : function() {
    this.$el.html(this.template);
    return this;
  }
});

var SpeedView = Backbone.View.extend({
  tagName : 'div',
  className : 'speed-control',
  template : _.template($('#speed-control-template').html(), utils),

  events : {
    'mousedown .up' : 'onUp',
    'mousedown .down' : 'onDown',
    'mousedown .play-pause' : 'onPP',
  },

  initialize : function(options, game, itdh, ctdh) {
    this.game = game;
    this.listenTo(gl.DirtyListener, 'tick', this.render);
    this.renderedOnce = false;

    this.listenTo(itdh, 'dragstart', this.onDragStart);
    this.listenTo(ctdh, 'dragstart', this.onDragStart);
    this.listenTo(itdh, 'drop', this.onDragDrop);
    this.listenTo(ctdh, 'drop', this.onDragDrop);
  },

  onDragStart : function() { this.$el.css('display', 'none'); },
  onDragDrop : function() { this.$el.css('display', 'block'); },

  onUp : function() { this.game.adjustSpeed('up'); },
  onDown : function() { this.game.adjustSpeed('down'); },
  onPP : function() { this.game.adjustSpeed('play-pause'); },

  render : function() {
    if (!this.renderedOnce) {
      this.$el.html(this.template);
      this.$timer = this.$('.timer');
      this.$pp = this.$('.play-pause');
      this.$speed = this.$('.speed');
      this.renderedOnce = true;
    }
    var sec = Math.floor((this.game.curTime - this.game.gameTime) / 1000);
    var s = sec % 60;
    sec -= s;
    var ms = sec % 3600;
    var hs = sec - ms;
    this.$timer.html(sprintf('%02d:%02d:%02d', hs / 3600, ms / 60, s));
    this.$speed.html(this.game.gameSpeed + 'x');

    if (this.game.gameSpeed === 0) {
      this.$pp.addClass('paused');
    } else {
      this.$pp.removeClass('paused');
    }

    if (this.game.gameSpeed <= 2 || this.game.settings.disableShake) {
      this.$el.removeClass('shake shake-little shake-constant');
    } else if (this.game.gameSpeed === 10) {
      if (!this.$el.hasClass('shake')) {
        this.$el.addClass('shake shake-constant');
      }
      if (!this.$el.hasClass('shake-little')) {
        this.$el.addClass('shake-little');
      }
    } else if (this.game.gameSpeed >= 50) {
      if (!this.$el.hasClass('shake')) {
        this.$el.addClass('shake shake-constant');
      }
      if (this.$el.hasClass('shake-little')) {
        this.$el.removeClass('shake-little');
      }
    }
    return this;
  }
});

export var FooterView = Backbone.View.extend({
  tagName : 'div',
  className : 'footer',

  events : {'mouseenter' : 'onMouseenter'},

  onMouseenter : function() { gl.UIEvents.trigger('itemSlotMouseleave');},

  initialize : function(options, game, itemTabDH, cardTabDH, craftTab) {
    this.resize();
    this.listenTo(gl.DirtyListener, 'throttledResize', this.resize);

    this.heroBodyView = new VitalsView({}, game.zone.hero, game.zone);
    this.centerView = new CenterView({}, game);
    this.speedView = new SpeedView({}, game, itemTabDH, cardTabDH);
    this.dropAreaView =
        new DropAreaView({}, game, itemTabDH, cardTabDH, craftTab);
  },

  resize : function() {
    this.$el.css(
        {width : window.innerWidth, top : window.innerHeight - FOOTER_HEIGHT});
  },

  render : function() {
    var frag = document.createDocumentFragment();
    frag.appendChild(this.heroBodyView.render().el);
    frag.appendChild(this.centerView.render().el);
    frag.appendChild(this.speedView.render().el);
    frag.appendChild(this.dropAreaView.render().el);
    this.$el.html(frag);
    return this;
  },
});
