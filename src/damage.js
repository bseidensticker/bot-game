import * as _ from 'underscore';

import {gl} from './globals';
import {Point} from './vectorutils';

var colorLookup = {
  phys : 'rgba(230, 0, 0, 0.8)',
  cold : 'rgba(0, 255, 255, 0.8)',
  fire : 'rgba(255, 165, 0, 0.8)',
  light : 'rgba(193, 17, 214, 0.8)',
  pois : 'rgba(85, 255, 139, 0.8)'
};

export function Damage(start, end, vector, height, type) {
  this.end = end;
  if (end.equal(start)) {
    this.vect = vector.unitVector();
  } else {
    this.vect = end.sub(start).unitVector();
  }
  this.vect = this.vect.rotate((Math.random() - 0.5) * 60);
  this.startTime = gl.time;
  this.height = height;
  this.type = type;
  this.color = colorLookup[type];

  this.getY = Damage.prototype[this.type + 'Y'].bind(this);

  this.k = -3 * this.height * (0.75 + Math.random() / 2);
  this.p = 0.75 * this.height * (0.75 + Math.random() / 2);
}

Damage.prototype.physY = function() {
  var elapsed = (gl.time - this.startTime) / 1000;
  return (-3 * this.height) * Math.pow((elapsed - 0.2), 2) +
         (0.75 * this.height);
};

Damage.prototype.fireY = function() {
  var elapsed = (gl.time - this.startTime) / 1000;
  return 1.5 * this.height * Math.pow((elapsed - 0.75), 3) + this.height;
};

Damage.prototype.lightY = function() {
  var elapsed = (gl.time - this.startTime) / 1000;
  return this.height * elapsed +
         this.height / 4 * Math.sin(elapsed * Math.PI * 4) + this.height / 2;
};

Damage.prototype.coldY = function() {
  var elapsed = (gl.time - this.startTime) / 1000;
  return (-1.5 * this.height) * Math.pow((elapsed - 0.5), 2) +
         this.height * 1.2;
};

Damage.prototype.poisY = function() {
  var elapsed = (gl.time - this.startTime) / 1000;
  return (-2 * this.height) * Math.pow(elapsed, 2) + this.height * 0.65;
};

Damage.prototype.getBase = function() {
  var elapsed = (gl.time - this.startTime);
  var base = this.end.add(this.vect.mult(4 * elapsed));
  return base;
};

export function DamageDealt(attack, spec) {
  var physDmg = attack.physDmg;
  this.phys = physDmg * physDmg / (physDmg + spec.armor);
  this.light =
      attack.lightDmg * attack.lightDmg / (attack.lightDmg + spec.lightResist);
  this.cold =
      attack.coldDmg * attack.coldDmg / (attack.coldDmg + spec.coldResist);
  this.fire =
      attack.fireDmg * attack.fireDmg / (attack.fireDmg + spec.fireResist);
  this.pois =
      attack.poisDmg * attack.poisDmg / (attack.poisDmg + spec.poisResist);
  this.total = this.phys + this.light + this.cold + this.fire + this.pois;
}
