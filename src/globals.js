import * as Backbone from 'backbone';
import * as _ from 'underscore';

import {DirtyQueueClass} from './events';

export var gl = {};

gl.DirtyQueue = new DirtyQueueClass();

gl.DirtyListener = _.extend({}, Backbone.Events);

gl.GameEvents = _.extend({}, Backbone.Events);
gl.ItemEvents = _.extend({}, Backbone.Events);
gl.EquipEvents = _.extend({}, Backbone.Events);
gl.UIEvents = _.extend({}, Backbone.Events);
gl.MessageEvents = _.extend({}, Backbone.Events);
