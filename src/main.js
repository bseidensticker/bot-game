/*
   Version Number Order:
   v0-1-1b, 0-1-2, 0-1-3, 0-1-4, 0-1-5, 0-1-6, 0-1-7
   0-1-8, 0-1-9, 0-1-10, 0-1-11, 0-1-12, 0-1-13, 0-1-14, 0-1-15,
   0-1-16, 0-1-17, 0-1-18, 0-1-19 0-1-20 0-1-21, 0-1-22, 0-1-23,
   0-1-24, 0-1-25
   0-2-0, 0-2-1, 0-2-2, 0-2-3, 0-2-4, 0.2.5, 0.2.6, 0.2.7, 0.2.8
   0-2-9, 0-2-10, 2-1-11, 0-2-12, 0-2-13, 0-2-14, 0-2-15, 0-2-16
   0-2-17, 0-2-18, 0-2-19, 0-2-20, 0-2-21, 0-2-22, 0-2-23, 0-2-24
   0-2-25, 0-2-26, 0-2-27, 0-2-28, 0-2-29, 0-2-30, 0-2-31
   0-3-0
 */

import {ref} from 'itemref/itemref';
import $ from 'jquery';
import * as _ from 'underscore';

import * as dropsLib from './drops';
import * as entity from './entity';
import {gl} from './globals';
import * as inv from './inventory';
import log from './log';
import {Model} from './model';
import * as storage from './storage';
import * as views from './views';
import * as zone from './zone';
import {gameRateTracker} from './stats-tracker';

let STEP_SIZE = 10;
let MAX_STEP = 500;
let SPEEDS_UP = {
  0 : 0.1,
  0.1 : 0.2,
  0.2 : 0.5,
  0.5 : 1,
  1 : 2,
  2 : 5,
  5 : 10,
  10 : 50,
  50 : 250,
  250 : 1000,
  1000: 1000
};
let SPEEDS_DOWN = {
  1000 : 250,
  250 : 50,
  50 : 10,
  10 : 5,
  5 : 2,
  2 : 1,
  1 : 0.5,
  0.5 : 0.2,
  0.2 : 0.1,
  0.1 : 0,
  0 : 0
};


var globalStart = new Date().getTime();

export function onReady() {
  gl.time = 0;
  gl.settings = {};
  gl.sessionId = Math.floor(Math.random() * 10000);
  if (localStorage.getItem('clientId') == null) {
    var newClientId = Math.floor(Math.random() * 10000000);
    localStorage.setItem('clientId', newClientId);
    gl.clientId = newClientId;
  } else {
    gl.clientId = localStorage.getItem('clientId');
  }
  gl.FB = new Firebase('https://fiery-heat-4226.firebaseio.com');
  var auth = gl.FB.getAuth();
  console.log('auth', auth);
  if (auth !== null) {
    if (auth.uid === undefined) {
      throw 'how do i have valid auth but no uid?';
    }
    gl.FB.child('accounts')
        .child(auth.uid)
        .child('lastClientId')
        .set(gl.clientId);
    gl.FB.child('accounts')
        .child(auth.uid)
        .child('clientIds')
        .child(gl.clientId)
        .set(Firebase.ServerValue.TIMESTAMP);
    gl.FB.child('accounts')
        .child(auth.uid)
        .child('email')
        .set(auth.password.email);

    if (auth !== null) {
      console.log('signing in as ' + gl.FB.getAuth().uid);
      if (auth !== gl.accountId) {
      }
      gl.accountId = auth.uid;
    } else {
      console.log('have account, but not signed in');
    }

    // code for taking control of account goes here
  }

  gl.VERSION_NUMBER = '0-3-1';

  log.init(gl.VERSION_NUMBER, gl.sessionId);

  gl.ZONE_LEVEL_SPACING = 5;
  $('title').html('Dungeons of Derp v' + gl.VERSION_NUMBER.replace(/\-/g, '.') +
                  ' ALPHA');

  log.info('onReady');

  var gameModel = new GameModel();
  // gl.game = gameModel;
  // gl.validateZones = gameModel.validateZones.bind(gameModel);

  var gameView = new views.GameView({}, gameModel);

  // TODO remove
  gl.gameView = gameView;

  var keyHandler = new KeyHandler(gameModel);
  $(window).on('keydown', keyHandler.onKeydown.bind(keyHandler));
}

export var GameModel = Model.extend({
  initialize : function() {
    this.lastSave = 0;

    gl.builds = [];

    this.inv = new inv.ItemCollection();
    this.cardInv = new inv.CardCollection();
    this.recycleManager = new inv.RecycleManager(this.inv, this.cardInv);
    this.matInv = new inv.MaterialManager(this.cardInv);
    gl.canLevelCard = this.matInv.canLevelCard.bind(this.matInv);

    this.newStateManager = new inv.NewStateManager(this.inv, this.cardInv);
    this.hero = new entity.newHeroSpec(this.inv, this.cardInv, this.matInv);
    this.cardInv.equipped = this.hero.equipped;
    this.cardInv.skillchain = this.hero.skillchain;
    gl.cardSort = this.cardInv.sort.bind(this.cardInv);
    gl.pause = this.triggerPause.bind(this);
    gl.setMoveAngle = this.setHeroMoveAngle.bind(this);
    this.settings = this.defaultSettings();
    this.zone = new zone.ZoneManager(this.hero, this.settings);
    gl.getMonStats = this.zone.getMonStats;
    gl.valMons = this.zone.valMons;

    gl.saveBuild = this.saveBuild.bind(this);
    gl.loadBuild = this.loadBuild.bind(this);
    gl.renameBuild = this.renameBuild;

    this.recycleManager.zone = this.zone;

    this.gameTime = new Date().getTime();
    this.curTime = this.gameTime;
    this.gameSpeed = 1;

    var loadSuccess = this.load();
    if (!loadSuccess) {
      this.noobGear();
      log.error('No save data found, starting new character with sessionId: %s',
                gl.sessionId);
    }

    this.zone.settings = this.settings; // Hack, zone needs a ref to settings
                                        // and loading erases it
    gl.settings = this.settings;
    this.zone.newZone(this.zone.nextZone);

    this.zonesCleared = 0;
    this.deaths = 0;

    this.mspf = 16;

    this.listenTo(gl.GameEvents, 'reportData', this.reportData);
    this.listenTo(gl.GameEvents, 'beatgame', this.beatGame);
    this.listenTo(gl.GameEvents, 'zoneClear', this.zoneClear);
    setInterval(this.intervalTick.bind(this), 1000);

    requestAnimFrame(this.onFrame.bind(this));
  },

  defaultSettings : function() {
    return {
      'enableBuildHotkeys' : true,
      'autoAdvance' : true,
      'disableShake' : false,
      'autoCraft' : true,
      'pauseOnDeath' : false,
      'backOnDeath' : false,
      'bossPause' : false,
      'zonesBack' : 1,
      'enableHeroDmgMsgs' : true,
      'enableMonDmgMsgs' : true,
      'enableMatMsgs' : true,
    };
  },

  fbFullSave : function() {
    console.log('main fullsave');
    var data = this.toJSON();
    gl.FB.child('accounts').child(gl.accountId).child('fullData').set(data);
  },

  fbFullLoad : function() {
    gl.FB.child('accounts')
        .child(gl.accountId)
        .child('fullData')
        .once('value',
              function(dataSnapshot) {
                console.log(dataSnapshot.val());
                localStorage.setItem(
                    'data', storage.enc(JSON.stringify(dataSnapshot.val())));
                location.reload();
              },
              function(err) { console.log(err); });
  },

  trySave : function() {
    var now = new Date().getTime();
    if (now - this.lastSave < 1000) {
      return;
    }
    this.lastSave = now;
    localStorage.setItem('data', storage.enc(JSON.stringify(this.toJSON())));
  },

  toJSON : function() {
    var data = {
      builds : gl.builds,
      settings : this.settings,
      version : gl.VERSION_NUMBER,
      cardInv : this.cardInv.toJSON(),
      inv : this.inv.toJSON(),
      matInv : this.matInv.toJSON(),
      zone : this.zone.toJSON(),
      hero : this.hero.toJSON(),
      skillchain : this.hero.skillchain.toJSON(),
      equipped : this.hero.equipped.toJSON(),
      cheats : 1,
      userXpMult : 1,
      timeCoef : 1,
      gameTime : this.gameTime
    };
    return data;
  },

  reportData : function() { log.reportData(this);},

  saveBuild : function(buildSlot) {
    if (gl.builds[buildSlot] !== undefined && gl.builds[buildSlot] !== null &&
        gl.builds[buildSlot].name !== undefined) {
      var name = gl.builds[buildSlot].name;
    }
    var build = {};
    var data = this.toJSON();
    build.equipped = data.equipped;
    build.skillchain = data.skillchain;
    build.inv = _.filter(data.inv, function(m) { return m.cardNames.length; });
    if (buildSlot >= 0) {
      gl.builds[buildSlot] = build;
      log.warning('Build saved to slot %d', buildSlot);
      if (name !== undefined) {
        gl.builds[buildSlot].name = name;
      }
    }
    gl.UIEvents.trigger('buildsave');
    return build;
  },

  loadBuild : function(buildSlot) {
    var items, invItem;
    var build = gl.builds[buildSlot];
    if (build !== undefined && build !== null) {
      if (build.name) {
        gl.lastBuildLoaded = build.name;
      } else {
        gl.lastBuildLoaded = "Build " + buildSlot;
      }
      log.reportBuild(buildSlot, build);
      _.each(this.hero.equipped.slots,
             function(slot) { this.hero.equipped.unequip(slot); }, this);
      _.each(_.range(5),
             function(i) { this.hero.skillchain.equip(undefined, i); }, this);

      this.hero.skillchain.fromJSON(build.skillchain, this.inv);
      this.hero.equipped.fromJSON(build.equipped, this.inv);
      _.each(build.inv, function(loadItem) {
        var invItem = _.findWhere(this.inv.getModels(), {name : loadItem.name});
        if (invItem) {
          invItem.loadCards(loadItem.cardNames, this.cardInv);
        }
      }, this);
      log.warning('Build loaded from slot %d', buildSlot);
      gl.UIEvents.trigger('buildload');
      return;
    }
    log.warning('No build loaded, slot %d empty or invalid', buildSlot);
  },

  renameBuild : function() {
    var buildname = $('#renamebuild').val();
    var buildnum = $('#renamebuildnum').val();
    if (gl.builds[buildnum]) {
      gl.builds[buildnum].name = buildname;
    }
    gl.UIEvents.trigger('buildsave');
  },

  beatGame : function() {
    // var uid = localStorage.getItem('uid');
    // var msg = '' + this.zone.unlockedZones + ' - ' +
    // this.zone.getZoneFromNum(this.zone.unlockedZones).nameStr;
    log.reportWinner(this.hero, this.zone);
  },

  deathPause : function() {
    if (gl.settings.pauseOnDeath) {
      this.gameSpeed = 0;
    }
  },

  triggerPause : function() { this.gameSpeed = 0;},

  setHeroMoveAngle : function(angle) { this.hero.moveAngle = angle;},

  zoneClear : function() { log.reportClear(this);},

  load : function() {
    var data = storage.getData();
    if (data) {
      log.warning('found some data');

      if (data.cheats !== undefined && data.cheats != 0) {
        log.error('cheat mode activated');
      }
      if (data.timeCoef !== undefined && data.timeCoef != 1) {
        log.error('time stretching activated');
      }
      if (data.userXpMult !== undefined && data.userXpMult != 1) {
        log.error('xp bonus activated');
      }

      gl.builds = (data.builds !== undefined) ? data.builds : [];
      this.settings = (data.settings !== undefined) ? data.settings
                                                    : this.defaultSettings();
      data = this.upgradeData(data);
      this.cardInv.fromJSON(data.cardInv);
      this.inv.fromJSON(data.inv, this.cardInv);
      this.matInv.fromJSON(data.matInv);
      this.zone.fromJSON(data.zone);
      this.hero.fromJSON(data.hero);
      this.hero.skillchain.fromJSON(data.skillchain, this.inv);
      this.hero.equipped.fromJSON(data.equipped, this.inv);
      this.gameTime = data.gameTime;
      this.hero.computeAttrs();
      this.cardInv.sort();
      return true;
    }
    return false;
  },

  upgradeData : function(data) {
    switch (data.version) {
    case undefined:
      log.error('Upgrading data from v0-1-1b to 0-1-2');
      data =
          JSON.parse(JSON.stringify(data).replace(/putrified/g, 'putrefied'));
      _.each(data.cardInv, function(card) { card.qp = 0; });
    case '0-1-2':
    case '0-1-3':
    case '0-1-4':
      log.error('Upgrading data from v0-1-3 to 0-1-4');
      var order = ref.zoneOrder.order;
      var fromNextZone = order.indexOf(data.zone.nextZone);
      var ul = Math.max(fromNextZone, data.zone.unlockedZones);
      if (ul >= order.length) {
        ul = order.length - 1;
      }
      data.zone.unlockedZones = ul;
    case '0-1-5':
      log.error('Upgrading data from v0-1-4 to 0-1-5');
      data.settings = this.defaultSettings();
    case '0-1-6':
      log.error('Upgrading data from v0-1-5 to 0-1-6');
      data.settings.autoAdvance = false;
    case '0-1-7':
    case '0-1-8':
    case '0-1-9':
      this.hero.versionCreated = 'legacy';
    case '0-1-10':
    case '0-1-11':
    case '0-1-12':
    case '0-1-13':
    case '0-1-14':
    case '0-1-15':
    case '0-1-16':
      data.gameTime = new Date().getTime();
    case '0-1-17':
    case '0-1-18':
    case '0-1-19':
    case '0-1-20':
    case '0-1-21':
    case '0-1-22':
    case '0-1-23':
    case '0-1-24':
    case '0-2-0':
    case '0-2-1':
    case '0-2-2':
    case '0-2-3':
    case '0-2-4':
    case '0-2-5':
      data.settings.autoCraft = false;
    case '0-2-6':
    case '0-2-7':
    case '0-2-8':
      data.settings.pauseOnDeath = false;
    case '0-2-9':
    case '0-2-10':
    case '0-2-11':
    case '0-2-12':
      data.settings.bossPause = false;
      data.settings.backOnDeath = false;
    case '0-2-13':
      data.settings.zonesBack = 1;
    case '0-2-14':
    case '0-2-15':
    case '0-2-16':
    case '0-2-17':
    case '0-2-18':
    case '0-2-19':
    case '0-2-20':
    case '0-2-21':
    case '0-2-22':
    case '0-2-23':
    case '0-2-24':
      data.settings.enableHeroDmgMsgs = true;
      data.settings.enableMonDmgMsgs = true;
    case '0-2-25':
    case '0-2-26':
    case '0-2-27':
    case '0-2-28':
    case '0-2-29':
      data.settings.enableMatMsgs = true;
    case '0-2-30':
    case '0-2-31':
    case '0-3-0':
      break;

    default:
      log.error('No upgrade required');
      break;
    }
    data.version = gl.VERSION_NUMBER;
    log.error('Data is up to version %s spec', data.version);
    return data;
  },

  noobGear : function() {
    log.warning('noob gear');
    this.inv.noobGear();
    this.cardInv.noobGear();

    var items = this.inv.getModels();
    this.hero.equipped.equip(_.findWhere(items, {name : 'cardboard sword'}),
                             'weapon');
    this.hero.equipped.equip(_.findWhere(items, {name : 'balsa helmet'}),
                             'head');
    this.hero.equipped.equip(_.findWhere(items, {name : 'latex gloves'}),
                             'hands');
    this.hero.equipped.equip(_.findWhere(items, {name : 't-shirt'}), 'chest');
    this.hero.equipped.equip(_.findWhere(items, {name : 'jeans'}), 'legs');
    this.hero.skillchain.equip(_.findWhere(items, {name : 'basic melee'}), 0);
  },

  intervalTick : function() {
    var thisTime = new Date().getTime();
    if (thisTime - this.curTime > 2000) {
      this.modelTick();
    }
  },

  onFrame : function() {
    if (new Date().getTime() - this.curTime > this.mspf) {
      this.modelTick();
      this.visTick();
    }

    this.trySave();
    requestAnimFrame(this.onFrame.bind(this));
  },

  computeTickTime : function(now) {
    let dt = now - this.curTime;
    this.curTime = now;

    // If this step is small enough, do the whole thing.
    if (dt < MAX_STEP) {
      return dt;
    }

    // Otherwise, step the MAX_STEP, and put the rest of the time back on the clock.
    log.error(sprintf('putting %dms back on the clock', Math.floor(dt - MAX_STEP)));
    this.gameTime -= (dt - MAX_STEP)

    return MAX_STEP;
  },

  modelTick : function() {
    let now = new Date().getTime();
    let newTime = this.computeTickTime(now);
    let availTime = now - this.gameTime;

    if (newTime <= 0) {
      return;
    }

    let dt = newTime * this.gameSpeed;
    let cost = dt;

    if (cost > availTime) {
      dt *= availTime / cost;
      cost = availTime;
      this.gameSpeed = 1;
      gl.MessageEvents.trigger('message', {
        text : 'No time left!',
        type : 'timeleft',
        pos : this.zone.hero.pos,
        color : 'rgba(230, 10, 10, 0.8)',
        lifespan : 2000,
        verticalOffset : 0,
        time : gl.time,
        expires : gl.time + 2000
      });
    }

    gameRateTracker.updateRates(cost);

    this.gameTime += cost;

    while (dt > 0) {
      let incBy = dt > STEP_SIZE ? STEP_SIZE : dt;
      gl.time += incBy;
      gl.lastTimeIncr = incBy;
      dt -= incBy;
      this.zone.zoneTick();
    }
  },

  visTick : function() {
    gl.DirtyQueue.mark('tick');
    gl.DirtyQueue.triggerAll(gl.DirtyListener);
    gl.DirtyQueue.triggerAll(gl.DirtyListener);
    gl.DirtyQueue.triggerAll(gl.DirtyListener);
  },

  bestGear : function(itemType, type) {
    var items = _.where(this.inv.getModels(), {itemType : itemType});
    items = _.where(items, {type : type});
    items = _.sortBy(items, function(item) { return item.level; });
    if (items.length > 0) {
      return items.pop();
    }
    return undefined;
  },

  autoEquip : function() {
    this.hero.equipped.equip(this.bestGear('weapon', 'melee'), 'weapon');
    this.hero.equipped.equip(this.bestGear('armor', 'head'), 'head');
    this.hero.equipped.equip(this.bestGear('armor', 'chest'), 'chest');
    this.hero.equipped.equip(this.bestGear('armor', 'hands'), 'hands');
    this.hero.equipped.equip(this.bestGear('armor', 'legs'), 'legs');

    var skills = _.where(this.inv.getModels(), {itemType : 'skill'});
    skills = _.sortBy(skills, function(skill) { return -skill.cooldownTime; });
    log.error('skill names: %s', _.pluck(skills, 'name').join(', '));
    log.error('skill cooldownTimes: %s',
              _.pluck(skills, 'cooldownTime').join(', '));
    _.each(skills.slice(0, 5),
           function(skill, i) { this.hero.skillchain.equip(skill, i); }, this);
  },

  adjustSpeed : function(dir, godmode) {
    if (dir === 'up') {
      this.gameSpeed = SPEEDS_UP[this.gameSpeed];
      if (!godmode) {
        this.gameSpeed = Math.min(50, this.gameSpeed);
      }
    } else if (dir === 'down') {
      this.gameSpeed = SPEEDS_DOWN[this.gameSpeed];
    } else if (dir === 'play-pause') {
      this.gameSpeed = this.gameSpeed === 0 ? 1 : 0;
    }
  },

  validateZones : function() {
    console.log(this.zone);
    var res = this.zone.statResult;
    console.log(res);
    _.each(res.avgs, function(statval, stat) {
      if (statval === 0) {
        return;
      }
      console.log(stat + ' ratio : ' + (res.maxs[stat] / res.avgs[stat]));
    });
  },
});

function KeyHandler(gameModel) { this.gameModel = gameModel; }

KeyHandler.prototype.liveKeys = function(event, godmode) {
  let key = event.keyCode;
  var SPACE = 32, UP = 38, DN = 40, HKEY = 72, PKEY = 80, CKEY = 67, IKEY = 73,
      FKEY = 70, RKEY = 82, MKEY = 77, SKEY = 83, GKEY = 21;
  if (key === SPACE) {
    this.gameModel.adjustSpeed('play-pause', godmode);
  } else if (key === UP) {
    this.gameModel.adjustSpeed('up', godmode);
  } else if (key === DN) {
    this.gameModel.adjustSpeed('down', godmode);
  } else if (key >= 48 && key <= 57 &&
             this.gameModel.settings.enableBuildHotkeys) {
    var buildSlot = key - 48;
    if (event.shiftKey) {
      gl.saveBuild(buildSlot);
    } else {
      gl.loadBuild(buildSlot);
    }
  } else if (key == PKEY) {
    this.gameModel.zone.hero.tryUsePotion();
  } else if (key == CKEY) {
    gl.UIEvents.trigger('footer:buttons:cards');
  } else if (key == IKEY) {
    gl.UIEvents.trigger('footer:buttons:inv');
  } else if (key == FKEY) {
    gl.UIEvents.trigger('footer:buttons:craft');
  } else if (key == RKEY) {
    gl.UIEvents.trigger('footer:buttons:recycle');
  } else if (key == MKEY) {
    gl.UIEvents.trigger('footer:buttons:map');
  } else if (key == HKEY) {
    gl.UIEvents.trigger('footer:buttons:help');
  } else if (key == SKEY) {
    gl.UIEvents.trigger('footer:buttons:stats');
  } else if (key == GKEY) {
    gl.UIEvents.trigger('footer:buttons:config');
  } else if (key == 192 && event.shiftKey) {
    /*if(this.gameModel.hero.level < 100) {
       log.warning('you must be at least level 100 to get test card');
       }
       if(_.findWhere(this.gameModel.cardInv.models, {'name': 'colossus'}) ==
       undefined) { this.gameModel.cardInv.addDrops([
       dropsLib.dropFactory('card', ['colossus', 1]),
       ]);
       log.warning('dropping colossus');
       } else {
       log.warning('you already have it');
       }*/
  }
};

KeyHandler.prototype.onKeydown = function(event) {
  if (document.activeElement.nodeName !== 'BODY') {
    return;
  }

  var godmode = isNaN(parseInt(localStorage.getItem('uid')));
  this.liveKeys(event, godmode);

  if (!godmode) {
    return;
  }

  var gameModel = this.gameModel;
  var SPACE = 32, EKEY = 69, TKEY = 84, UP = 38, DN = 40, BKEY = 66, XKEY = 88,
      VKEY = 86, ZKEY = 90;
  var key = event.key;

  log.info('keydown, key: %d', key);

  if (key === 'e') {
    // Cheat for adding 1000xp (for easier testing)
    log.error('XP Cheat!');
    this.gameModel.hero.applyXp(this.gameModel.hero.getNextLevelXp());
  } else if (key === 't') {
    log.error('Time Cheat');
    this.gameModel.curTime -= 3600000;
  } else if (key === 'z') {
    let TARGSTAT = undefined;
    let res = this.gameModel.zone.statResult;
    if (TARGSTAT !== undefined) {
      log.warning('%s: avg: %f, max: %f, min: %f', TARGSTAT, res.avgs[TARGSTAT],
                  res.maxs[TARGSTAT], res.mins[TARGSTAT]);
      console.log(res.mons);
    } else {
      console.log('avgs', res.avgs);
      console.log('maxs', res.maxs);
      console.log('mins', res.mins);
    }
  } else if (key === 'b' || key === 'x' || key === 'v') {
    log.error('Melee Equipment cheat');
    var items = this.gameModel.inv.getModels();
    var egm = this.gameModel.hero.equipped;
    var sc = this.gameModel.hero.skillchain;

    if (key === 'b') {
      this.gameModel.inv.addDrops([
        dropsLib.dropFactory('item', [ 'weapon', 'spikey mace' ]),
        dropsLib.dropFactory('skill', 'lethal strike'),
        dropsLib.dropFactory('skill', 'flaming debris'),
        dropsLib.dropFactory('skill', 'ground smash'),
      ]);
      egm.equip(_.findWhere(items, {name : 'spikey mace'}), 'weapon');
      sc.equip(_.findWhere(items, {name : 'ground smash'}), 0);
      sc.equip(_.findWhere(items, {name : 'lethal strike'}), 1);
      sc.equip(_.findWhere(items, {name : 'flaming debris'}), 2);
      sc.equip(_.findWhere(items, {name : 'basic melee'}), 4);
    } else if (key === 'x') {
      this.gameModel.inv.addDrops([
        dropsLib.dropFactory('item', [ 'weapon', 'composite bow' ]),
        dropsLib.dropFactory('skill', 'headshot'),
        dropsLib.dropFactory('skill', 'speed shot'),
        dropsLib.dropFactory('skill', 'piercing shot'),
        dropsLib.dropFactory('skill', 'explonential shot'),
      ]);
      this.gameModel.cardInv.addDrops([
        dropsLib.dropFactory('card', [ 'more projectiles', 2 ]),
      ]);
      egm.equip(_.findWhere(items, {name : 'composite bow'}), 'weapon');
      sc.equip(_.findWhere(items, {name : 'headshot'}), 0);
      sc.equip(_.findWhere(items, {name : 'speed shot'}), 1);
      sc.equip(_.findWhere(items, {name : 'basic range'}), 4);
    } else if (key === 'v') {
      this.gameModel.inv.addDrops([
        dropsLib.dropFactory('item', [ 'weapon', 'star wand' ]),
        dropsLib.dropFactory('skill', 'fire ball'),
        dropsLib.dropFactory('skill', 'poison ball'),
        dropsLib.dropFactory('skill', 'lightning ball'),
        dropsLib.dropFactory('skill', 'ice ball'),
        dropsLib.dropFactory('skill', 'ice blast'),
        dropsLib.dropFactory('skill', 'nova'),
      ]);
      egm.equip(_.findWhere(items, {name : 'star wand'}), 'weapon');
      sc.equip(_.findWhere(items, {name : 'nova'}), 0);
      sc.equip(_.findWhere(items, {name : 'poison ball'}), 1);
      sc.equip(_.findWhere(items, {name : 'ice ball'}), 2);
      sc.equip(_.findWhere(items, {name : 'lightning ball'}), 3);
      sc.equip(_.findWhere(items, {name : 'basic spell'}), 4);
    }

    this.gameModel.inv.addDrops([
      dropsLib.dropFactory('item', [ 'armor', 'crusader helm' ]),
      dropsLib.dropFactory('item', [ 'armor', 'leatherplate armor' ]),
      dropsLib.dropFactory('item', [ 'armor', 'buckaneer boots' ]),
      dropsLib.dropFactory('item', [ 'armor', 'goldenscale gauntlets' ]),
    ]);

    egm.equip(_.findWhere(items, {name : 'crusader helm'}), 'head');
    egm.equip(_.findWhere(items, {name : 'leatherplate armor'}), 'chest');
    egm.equip(_.findWhere(items, {name : 'buckaneer boots'}), 'legs');
    egm.equip(_.findWhere(items, {name : 'goldenscale gauntlets'}), 'hands');

    this.gameModel.cardInv.addDrops([
      dropsLib.dropFactory('card', [ 'heart juice', 4 ]),
      dropsLib.dropFactory('card', [ 'brain juice', 4 ]),
      dropsLib.dropFactory('card', [ 'hot sword', 4 ])
    ]);
  }
};

// exports.extend({onReady : onReady, GameModel : GameModel});
