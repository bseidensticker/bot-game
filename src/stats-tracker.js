import {computeMovingAvg, computeMovingEwma, computeMovingPowma} from './utils';


class GameRateTracker {
  constructor() {
    this.powma3 = 1;
    this.powma30 = 1;
    this.lastNow = new Date().getTime();
  }

  updateRates(cost) {
    let now = new Date().getTime();
    let diff = now - this.lastNow;
    let rate = cost / diff;

    this.lastNow = now;

    this.powma3 = computeMovingPowma(this.powma3, rate, 3000, diff);
    this.powma30 = computeMovingPowma(this.powma30, rate, 30000, diff);
  }
}

export var gameRateTracker = new GameRateTracker();
