var React = require('react');
var ReactDOM = require('react-dom');
var main = require('./main');

function initIndex() {
  document.addEventListener('DOMContentLoaded', function() {
    ReactDOM.render(React.createElement(main.Main, {}),
                    document.getElementById('main'));
  });
}

module.exports = {
  initIndex : initIndex
};
