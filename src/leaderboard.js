import $ from 'jquery';
//$(initLeaderboard);

function initLeaderboard() {
  var leaderboard;
  var raceVersion = '0-3-1';
  var raceName = 'BARRACKS2';
  var leaderboardRef = new Firebase(
      'https://fiery-heat-4226.firebaseio.com/leaderboard/' + raceName);
  var blacklist =
      new Firebase('https://fiery-heat-4226.firebaseio.com/blacklist');
  var bl = [];

  blacklist.on('value', function(s) {
    bl = [];
    _.each(s.val(), function(v) { bl.push(v); });
    drawTable();
    // console.log(s.val());
  });

  var escapeHtml = function(str) {
    var entityMap =
        {"&" : "", "<" : "", ">" : "", '"' : '', "'" : '', "/" : '', ":" : ''};
    return String(str).replace(/[&<>"'\/]/g,
                               function(s) { return entityMap[s]; });
  };

  var drawTable = function() {
    lb = leaderboard;
    // console.log(lb);
    var str =
        '<table><tr><td>Pos</td><td>Level</td><td>Name</td><td>Highest Zone Cleared</td><td>Last Zone Cleared</td><td>Last Death</td><td>Deaths</td><td>Potions</td><td>Skillchain</td></tr>';
    var participants = [];

    _.each(lb, function(v, k) {
      if (isNaN(parseInt(k))) {
        return;
      }
      var splits = v.split(':');
      participants.push({
        //'score': parseInt(splits[0]),
        'name' : escapeHtml(splits[1]),
        'score' : parseInt(splits[2]), // old hlvl
        'highestZone' : splits[3],
        'currZone' : splits[4],
        'deaths' : splits[5],
        'potions' : splits[6],
        'uid' : k,
        'skillStr' : splits[7] === undefined ? '' : splits[7],
        'lastDeath' : splits[8] === undefined ? '' : splits[8],
      });
    });
    participants = _.sortBy(participants, function(par) { return -par.score; });
    var rank = 1;
    _.each(participants, function(par) {
      if (bl.indexOf(parseInt(par.uid)) !== -1) {
        return;
      }
      str += '<tr><td>' + rank + '</td><td>' + par.score + '</td><td>' +
             par.name + '</td><td>' + par.highestZone + '</td><td>' +
             par.currZone + '</td><td>' + par.lastDeath + '</td><td>' +
             par.deaths + '</td><td>' + par.potions + '</td><td>' +
             par.skillStr + '</td></tr>';
      rank += 1;
    });
    str += '</table>';

    if (leaderboard === null) {
      str += 'No qualifiers yet, be the first to clear a zone!'
    }

    $('#board').empty();
    $('#board').html(str);
  };

  leaderboardRef.on('value', function(snapshot) {
    leaderboard = snapshot.val();
    drawTable(leaderboard);
  });
}
