import {Firebase} from 'firebase';
import * as _ from 'underscore';

import {gl} from './globals';
import {firstCap} from './utils';
import {LOG_LEVEL} from './constants';

var LOG_FNS = [ debug, info, UI, warning, error, stack ];
var LOG_NAMES = [ 'debug', 'info', 'UI', 'warning', 'error', 'stack' ];

var FB;
var FBL;
var timeLog;
var uid;
var sessionId;
var version;
var deathCount = '';
var potionCount = '';

function init(v, sid) {
  console.log('log init');
  version = v;
  sessionId = sid;
  uid = localStorage.getItem('uid');

  if (uid) {
    warning('Recovered UID, resuming.');
  } else {
    warning('UID not found, creating new one.');
    uid = Math.floor(Math.random() * Math.pow(2, 32));
    localStorage.setItem('uid', uid);
  }

  FB = new Firebase('https://fiery-heat-4226.firebaseio.com');
  FBL = FB.child('logs').child(uid).child(version);
  FBL.child('logs').push('starting');
  FBL.child('sessionOrder').push(sessionId);
  this.FB = FB;
  timeReport();
}

function timeReport(tc) {
  var timeLog =
      FBL.child('time').child(sessionId).child(Math.floor(gl.time).toString());
  timeLog.child('servertime').set(Firebase.ServerValue.TIMESTAMP);
  timeLog.child('local-time').set(new Date().getTime());
  if (tc) {
    timeLog.child('timeCoef').set(tc);
  }
}

function enterZone(zoneName) { FBL.child('currentZone').set(zoneName); }

function prestige(prestigeAmount) {
  FBL.child('prestiges').push('' + new Date + ' - ' + prestigeAmount);
}

function reportData(game) {
  FBL.child('name').set(game.hero.name);
  FBL.child('version').set(gl.VERSION_NUMBER);
  FBL.child('lastReport').set(String(new Date()));
  FBL.child('level').set(game.hero.level);
  var data = game.toJSON();
  FBL.child('equipped').set(data.equipped);
  _.each(data.equipped, function(name, slot) {
    var cards = _.findWhere(data.inv, {'name' : name});
    FBL.child('cards').child(slot + 'cards').set(cards.cardNames.join(', '));
  });
  FBL.child('skillchain').set(data.skillchain);
  _.each(data.skillchain, function(name, slot) {
    var cards = _.findWhere(data.inv, {'name' : name});
    FBL.child('cards')
        .child('s' + slot + 'cards')
        .set(cards.cardNames.join(', '));
  });
  FBL.child('zone').set(data.zone.nextZone);
  FBL.child('unlockedZones').set(data.zone.unlockedZones);

  timeReport(game.timeCoefficient);
  if (gl.ZONE_LEVEL_SPACING !== 5) {
    FB.child('panel').child('weird').push(
        localStorage.getItem('uid') +
        ' - gl.ZONE_LEVEL_SPACING: ' + gl.VERSION_NUMBER);
  }
}

function reportWinner(hero, zone) {
  if (hero.name === 'some newbie') {
    error('default name, not elligible for leaderboard');
    return;
  }
  var msg = zone.unlockedZones + ':' + hero.name + ':' + hero.level + ':' +
            zone.getZoneFromNum(zone.unlockedZones).nameStr + ':' +
            zone.getZoneFromNum(zone.nextZone).nameStr + ':' + deathCount +
            ':' + potionCount + ':';

  _.each(hero.skillchain.skills, function(skill) {
    if (skill === undefined) {
      return;
    }
    msg += firstCap(skill.name) + ', ';
  });
  msg = msg.slice(0, msg.length - 2);
  msg += ':' + firstCap(hero.lastDeath);
  // error('leaderboard push: ' + msg);

  if (hero.versionCreated === '0-3-1') {
    FB.child('leaderboard').child('BARRACKS2').child(uid).set(msg);
  } else {
    FB.child('leaderboard')
        .child('legacy')
        .child(hero.versionCreated)
        .child(uid)
        .set(msg);
  }
}

function reportNewBuild() {
  FBL.child('deathCount').set(0);
  FBL.child('potionCount').set(0);
  FB.child(version)
      .child('newBuilds')
      .child(uid)
      .set(Firebase.ServerValue.TIMESTAMP);
}

function levelUp(lvl) { FBL.child('level').set(lvl); }

function reportClear(game) {
  var equipped = [];
  var build = game.saveBuild();
  _.each(build.equipped, function(val, key) { equipped.push(val); });
  _.each(build.skillchain, function(val, key) { equipped.push(val); });
  _.each(build.inv,
         function(item) { equipped = equipped.concat(item.cardNames); });

  var clearLog = '' + localStorage.getItem('uid') + ':' + game.hero.level +
                 ':' + game.zone.nameStr + ':' + equipped.join(',');
  // warning('reporting clear: ' + clearLog);
  FBL.child('clearLogs').child(clearLog).set(Firebase.ServerValue.TIMESTAMP);
  // TODO - add time to zone clear
}

function handleDonationToken(token, amount) {
  var id = uid || localStorage.getItem('uid');
  var savedDonation = {
    amount : amount,
    uid : id,
    date : new Date().toString(),
    tokenId : token.id,
    email : token.email,
    type : token.type,
    version : gl.VERSION_NUMBER
  };
  FB.child('panel').child('payments').child(uid).set(savedDonation);
  error('Donation! token: %s', JSON.stringify(token));
}

function donateAttempt(amount) {
  FB.child('panel')
      .child('donateAttempts')
      .push(localStorage.getItem('uid') + ': ' + new Date() + ' ' + amount);
}

function weird(s) {
  // fn used for reporting hacking and similar attempts
  FB.child('panel').child('weird').child(s).push(localStorage.getItem('uid') +
                                                 ': ' + new Date());
}

function reportDeath(msg) {
  warning(msg);
  FBL.child('deaths').push(msg);
  var dc = FBL.child('deathCount');
  dc.transaction(function(curr_val) {
    deathCount = (curr_val || 0) + 1;
    return deathCount;
  });
}

function reportPotion() {
  var dc = FBL.child('potionCount');
  dc.transaction(function(curr_val) {
    potionCount = (curr_val || 0) + 1;
    return potionCount;
  });
}

function reportBuild(slot, build) {
  FBL.child('loadedBuilds').child(slot).set(build);
}

function feedback(msg) {
  if (msg && msg.length) {
    var hasContent = msg.indexOf('says: ') < msg.length - 6;
    if (hasContent) {
      FB.child('panel')
          .child('feedback')
          .child(gl.VERSION_NUMBER)
          .push(uid + ' - ' + msg);
    }
  }
}

function fileLine() {
  var s = new Error().stack.split('\n')[3];
  return s.slice(s.indexOf('(') + 1, s.length - 1);
}

function dateStr() { return (new Date()).toString().slice(4, -15); }

function debug() {
  var a = arguments;
  a[0] = 'DEBUG ' + fileLine() + ' ' + a[0];
  console.log('%c' + sprintf.apply(null, a), 'color: blue');
}

function info() {
  var a = arguments;
  a[0] = 'INFO ' + fileLine() + ' ' + a[0];
  console.log('%c' + sprintf.apply(null, a), 'color: green');
}

function warning() {
  var a = arguments;
  a[0] = 'WARNING ' + fileLine() + ' ' + a[0];
  console.log('%c' + sprintf.apply(null, a), 'color: orange');
}

function error() {
  var a = arguments;
  logFB('ERROR:' + sprintf.apply(null, a) + '  @' + gl.time);
  a[0] = 'ERROR ' + fileLine() + ' ' + a[0];
  console.log('%c' + sprintf.apply(null, a), 'color: red');
}

//  call with 'log.line(new Error(), 'your text here');
function stack() {
  var a = arguments;
  a[0] =
      new Error().stack.replace(/   at /g, '').split('\n').slice(2).join('\n') +
      '\n  ' + a[0];
  console.log('%c' + sprintf.apply(null, a), 'color: purple');
}

function UI() {
  var a = arguments;
  a[0] = 'UI: ' + fileLine() + ' ' + a[0];
  console.log('%c' + sprintf.apply(null, a), 'color: cyan');
}

function logFB(str) {
  // FBL.child('logs').push(str);
}

function tmpr(s) {
  var id = uid || localStorage.getItem('uid');
  var name = 'Unable to retreive name';
  try {
    name = JSON.parse(s).hero.name;
  } catch (e) {
    error('%s when trying to get hero name', String(e));
  }
  FB.child('tmprs').child(id).push(version + ' - ' + name + ' - ' +
                                   String(new Date()));
}

var extender = {
  init : init,
  timeReport : timeReport,
  reportData : reportData,
  reportWinner : reportWinner,
  reportClear : reportClear,
  reportPotion : reportPotion,
  reportNewBuild : reportNewBuild,
  enterZone : enterZone,
  levelUp : levelUp,
  handleDonationToken : handleDonationToken,
  donateAttempt : donateAttempt,
  feedback : feedback,
  logFB : logFB,
  reportDeath : reportDeath,
  reportBuild : reportBuild,
  tmpr : tmpr,
  weird : weird,
  prestige : prestige
};

for (var key in extender) {
  extender[key] = function() {};
}

var clear = false;

for (var i = 0; i < LOG_FNS.length; i++) {
  if (clear || LOG_LEVEL === LOG_NAMES[i]) {
    extender[LOG_NAMES[i]] = LOG_FNS[i];
    clear = true;
  } else {
    extender[LOG_NAMES[i]] = function() {};
  }
}

Object.assign(module.exports, extender);
