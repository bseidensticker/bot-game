import {assert} from 'chai';
import * as _ from 'underscore';

import {ref} from '../itemref/itemref';
import {armor} from '../itemref/itemref_armor';
import {card} from '../itemref/itemref_card';
import {monster} from '../itemref/itemref_monster';
import {skill} from '../itemref/itemref_skill';
import {weapon} from '../itemref/itemref_weapon';
import {zone} from '../itemref/itemref_zone';

describe('Itemref', function() {
  it('does not crash', function() { assert.equal(true, true); });
});

describe('armor', function() {
  it('has the same identity', function() { assert.equal(ref.armor, armor); });

  describe('should all have mods and weaponType', function() {
    for (let [name, a] of Object.entries(armor)) {
      assert.isArray(a.mods);
      for (let mod of a.mods) {
        assert.isString(mod, 'armor ${name} ${mod}');
        assert.isNotEmpty(mod);
      }
      assert.isString(a.slot);
      assert.include([ 'head', 'chest', 'legs', 'hands' ], a.slot)
    }
  });
});

describe('card', function() {
  it('has the same identity', function() { assert.equal(ref.card, card); });

  describe('should all have mods and weaponType', function() {
    for (let [name, c] of Object.entries(card)) {
      assert.isArray(c.mods);

      for (let mod of c.mods) {
        assert.isString(mod, `card ${name} ${mod}`);
        assert.isNotEmpty(mod);
      }
      // cards whose names begin with 'proto' only have a mods array
      if (name.includes('proto', 0)) {
        continue;
      }
      assert.isString(c.slot, `slot is: ${c.slot}`);
      assert.include([ 'head', 'chest', 'legs', 'hands', 'skill', 'weapon' ],
                     c.slot)
    }
  });
});

describe('weapon', function() {
  it('has the same identity', function() { assert.equal(ref.weapon, weapon); });

  describe('should all have mods and weaponType', function() {
    for (let [name, w] of Object.entries(weapon)) {
      assert.isArray(w.mods);

      for (let mod of w.mods) {
        assert.isString(mod, 'weapon ${name} ${mod}');
        assert.isNotEmpty(mod);
      }
      assert.isString(w.weaponType);
      assert.isNotEmpty(w.weaponType);
    }
  });
});
