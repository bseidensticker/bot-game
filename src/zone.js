import {ref} from 'itemref/itemref';
import * as _ from 'underscore';

import {AttackManager} from './attacks';
import {HeroBody, MonsterBody} from './bodies';
import {Damage} from './damage';
import * as entity from './entity';
import {gl} from './globals';
import log from './log';
import {Model} from './model';
import * as prob from './prob';
import {Point} from './vectorutils';

var TEAM_HERO = 0;
var TEAM_MONSTER = 1;

export var ZoneManager = Model.extend({
  initialize : function(hero, settings) {
    this.allZones = ref.zone;
    this.zoneOrder = ref.zoneOrder.order; // to be used later for rank increases

    this.settings = settings;
    this.hero = new HeroBody(hero, this);
    this.attackManager = new AttackManager(this.ensureRoom.bind(this));

    // Expose attack manager functions for EntityBodies and chaining
    // projectiles
    gl.addAttack = this.attackManager.addAttack.bind(this.attackManager);
    this.messages = new ZoneMessages();
    this.waitingUntil = 0;

    this.nextZone = 0;
    this.unlockedZones = 0;
    this.newZone(this.nextZone);
  },

  toJSON : function() {
    return {nextZone : this.nextZone, unlockedZones : this.unlockedZones};
  },

  fromJSON : function(data) {
    _.extend(this, data);
    this.newZone(this.nextZone); // this is redundent
  },

  getZoneFromNum : function(zoneNum) {
    var offset, upgradeCount, zoneI;
    if (zoneNum < 6) {
      offset = 0;
      upgradeCount = 0;
    } else if (zoneNum < 13) {
      offset = 6;
      upgradeCount = 1;
    } else if (zoneNum < 21) {
      offset = 13;
      upgradeCount = 2;
    } else if (zoneNum < 30) {
      offset = 21;
      upgradeCount = 3;
    } else if (zoneNum < 40) {
      offset = 30;
      upgradeCount = 4;
    } else if (zoneNum < 51) {
      offset = 40;
      upgradeCount = 5;
    } else {
      offset = 51;
      upgradeCount = 6 + Math.floor((zoneNum - offset) / 12);
    }
    var zoneCount = this.zoneOrder.length;
    var zoneI = (zoneNum - offset) % zoneCount;
    var name = this.zoneOrder[zoneI];

    if (name === 'beginners field') {
      upgradeCount -= 1;
    } else if (name === 'gothic castle') {
      upgradeCount -= 2;
    } else if (name === 'decaying temple') {
      upgradeCount -= 3;
    } else if (name === 'lich tower') {
      upgradeCount -= 4;
    } else if (name === 'wicked dream') {
      upgradeCount -= 5;
    } else if (name === 'imperial barracks') {
      upgradeCount -= 6;
    }

    var nameStr = name + (upgradeCount > 0 ? ' ' + (upgradeCount + 1) : '');
    // console.log(zoneNum, nameStr);
    return {
      nameStr : nameStr,
      upgradeCount : upgradeCount,
      zoneI : zoneI,
      name : name
    };
  },

  getMonStats : function(name, level, verbose) {
    verbose = verbose === false ? false : true;
    var mbod = new MonsterBody(name, level, this);
    var bodyStats = entity.defKeys.concat(entity.eleResistKeys);
    var attStats = entity.dmgKeys
    _.each(bodyStats, function(stat) {
      if (verbose) {
        console.log(stat + ": " + mbod.spec[stat]);
      }
      if (mbod.spec[stat] === undefined || isNaN(mbod.spec[stat])) {
        throw name + " has invalid " + stat;
      }
    });
    _.each(mbod.spec.skillchain.skills, function(skill) {
      if (skill === undefined) {
        return;
      }
      if (verbose) {
        console.log(skill.name);
      }
      _.each(attStats, function(stat) {
        if (verbose) {
          console.log(stat + ': ' + skill[stat]);
        }
        if (skill[stat] === undefined || isNaN(skill[stat])) {
          throw name + " -  " + skill.name + " has invalid " + stat;
        }
      });
    });
  },

  valMons : function(level) {
    var mons = ref.monster;
    _.each(mons, function(mon, name) {
      this.getMonStats(name, level, false);
      _.each(mon.sourceCards, function(card) {
        if (ref.card[card[0]] === undefined) {
          console.log('monster' + name + ' has weird card', card)
        }
      });
    }, this);
    console.log(this.statResult);
    return 'done';
  },

  getZoneStats : function() {
    var monsters = {};
    var EXCLUDE_BOSSES = true;
    var skillWeights = 0;
    var rooms = EXCLUDE_BOSSES ? this.rooms.slice(0, this.rooms.length - 1)
                               : this.rooms;

    _.each(this.weights, function(weight, i) {
      let monName = this.choices[i];
      let mon = new MonsterBody(this.choices[i], this.level, this);
      if (monsters[mon.spec.name] === undefined) {
        mon.weight = weight;
        monsters[mon.spec.name] = mon;
      }
    }, this);

    var defStats = [ 'moveSpeed', 'maxHp', 'armor', 'dodge', 'eleResistAll' ];

    var attStats = [
      'physDmg', 'fireDmg', 'coldDmg', 'poisDmg', 'lightDmg', 'accuracy',
      'speed'
    ];

    var maxs = {};
    var avgs = {};
    var mins = {};
    var maxnames = {};
    var minnames = {};
    var totalMonWeight = 0;

    _.each(monsters, function(mon) {
      if (mon.spec.name == 'buddha') {
        return;
      }
      totalMonWeight += mon.weight;
      _.each(defStats, function(stat) {
        if (maxs[stat] === undefined || mon.spec[stat] > maxs[stat]) {
          maxs[stat] = mon.spec[stat];
          maxnames[stat] = mon.spec.name + ' ' + mon.spec[stat];
        }
        if (mins[stat] === undefined || mon.spec[stat] < mins[stat]) {
          mins[stat] = mon.spec[stat];
          minnames[stat] = mon.spec.name + ' ' + mon.spec[stat];
        }
        if (avgs[stat] === undefined) {
          avgs[stat] = mon.spec[stat] * mon.weight;
        } else {
          avgs[stat] += mon.spec[stat] * mon.weight;
        }
      }, this);
      _.each(mon.spec.skillchain.skills, function(skill) {
        if (skill === undefined) {
          return;
        }
        skillWeights += mon.weight;
        _.each(attStats, function(stat) {
          if (maxs[stat] === undefined || skill[stat] > maxs[stat]) {
            maxs[stat] = skill[stat];
            maxnames[stat] =
                mon.spec.name + ' ' + skill.name + ' ' + skill[stat];
          }
          if (mins[stat] === undefined || skill[stat] < mins[stat]) {
            mins[stat] = skill[stat];
            minnames[stat] =
                mon.spec.name + ' ' + skill.name + ' ' + skill[stat];
          }
          if (avgs[stat] === undefined) {
            avgs[stat] = skill[stat] * mon.weight;
          } else {
            avgs[stat] += skill[stat] * mon.weight;
          }
        }, this);
      }, this);
    }, this);

    _.each(defStats, function(stat) { avgs[stat] /= totalMonWeight; });

    _.each(attStats, function(stat) {
      avgs[stat] /= skillWeights;
      if (isNaN(avgs[stat])) {
        console.log('weirdstat', stat);
      }
    });

    // console.log(maxs);
    // console.log(mins);
    // console.log(avgs);
    // console.log("max", maxs);
    // console.log("min", minnames);
    // console.log("avg", avgs);

    this.statResult = {
      'avgs' : avgs,
      'maxs' : maxs,
      'mins' : mins,
      'mons' : monsters,
    };

    return this.statResult;
    // console.log(this.statResult);
    // console.log(totalMonWeight);
  },

  newZone : function(zoneNum) {
    gl.DirtyQueue.mark('zone:start');
    if (typeof (zoneNum) !== 'number') {
      zoneNum = 0;
      this.nextZone = 0;
    }

    this.iuid = _.uniqueId('inst');

    var i, j, rooms, monsters, count, data;

    var currentZone = this.getZoneFromNum(zoneNum);
    var zoneCount = this.zoneOrder.length;
    var zoneI = currentZone.zoneI;
    var upgradeCount = currentZone.upgradeCount;

    this.name = currentZone.name;

    this.nameStr = currentZone.nameStr;
    log.enterZone(this.nameStr);
    // console.log(zoneCount, upgradeCount, zoneI, this.name, this.level);
    _.extend(this, this.allZones[this.name]);
    this.level =
        Math.max(1, zoneNum * 5); // 5 is constant for zone level spacing

    // Overriding roomcount to scale with zone number
    this.roomCount = 5 + Math.floor(Math.sqrt(zoneNum));

    this.rooms = this.generator();
    var choices = [];
    var weights = [];
    _.each(this.choices, function(mon, i) {
      var monref = ref.monster[mon];
      if (!monref.minLevel || monref.minLevel <= this.level) {
        choices.push(mon);
        weights.push(this.weights[i]);
      }
    }, this);
    var newMon;
    for (i = 0; i < this.rooms.length; i++) {
      monsters = [];
      if (i % 2 === 0) { // if this is not a corridor
        count =
            this.quantity[0] + prob.pProb(this.quantity[1] + (upgradeCount / 2),
                                          this.quantity[2] + upgradeCount);
        // max room pop is i+1 (first room always only one monster)
        count =
            Math.min((i / 2 + 1) * (this.quantity[0] + upgradeCount), count);
        for (var j = 0; j < count; j++) {
          newMon =
              new MonsterBody(choices[prob.pick(weights)], this.level, this);
          newMon.spec.materials = this.getZoneMats(newMon.spec.rarity);
          monsters.push(newMon);
        }
        if (i === this.rooms.length - 1) {
          newMon = new MonsterBody(this.boss, this.level, this);
          newMon.spec.materials = this.getZoneMats('boss');
          newMon.spec.rarity = 'boss';
          monsters.push(newMon);
        }
      }
      _.extend(this.rooms[i],
               {monsters : monsters, heros : {}, attackManager : undefined});
      _.each(this.rooms[i].monsters,
             function(mon) { mon.initPos(this.rooms[i]); }, this);
    }

    this.heroPos = 0;
    this.rooms[0].heros[this.hero.id] = this.hero;
    this.hero.revive();
    this.hero.initPos(this.rooms[0]);
    this.attackManager.nextRoom(this.rooms[0]);
    gl.DirtyQueue.mark('zone:new');
    this.getZoneStats();
  },

  getZoneMats : function(rarity) {
    var categoryRates = ref.matDropRates;

    if (rarity === undefined) {
      rarity = 'normal';
    }

    var droppableMats = [];
    _.each(this.materials, function(mat) {
      var matRef = ref.materials[mat];
      if (matRef === undefined) {
        log.error('mat %s not in itemref', mat);
      }

      var catI = Math.abs(matRef.category - 4);
      var rate = categoryRates[rarity][catI];
      if (rate > 0) {
        droppableMats.push(mat);
      }
    }, this);

    return droppableMats;
  },

  generator : function() {
    // windyness
    // lrange
    // hrange
    // scale

    var lrange = [ 10, 20 ];
    var hrange = [ 7, 10 ];
    var scale = 1000;
    var weights;
    var dir = 1; // [up, right, down, left], just like margins/padding in css
    var width, height;

    var rooms = [];
    var room;

    var size, pos, ent, exit, absEnt;

    size = new Point(prob.rand(lrange[0], lrange[1]),
                     prob.rand(hrange[0], hrange[1]));
    pos = new Point(0, 0);
    ent = new Point(0, prob.middle50(size.y));

    room = {size : size, pos : pos, ent : ent};

    rooms.push(room);

    while (rooms.length < this.roomCount * 2 - 1) {
      // Pick a new direction
      dir = prob.rand(0, 1);

      // get a width + height, swap height and len ranges if we aren't going
      // right, so the l/w ranges
      //   stay the same wrt the player
      // set the abs exit for the old room
      if (dir === 0) {
        room.exit = new Point(prob.middle50(room.size.x), 0);
      } else {
        room.exit = new Point(room.size.x, prob.middle50(room.size.y));
      }
      // old room is done
      // make corridor in the dir chosen

      absEnt = room.pos.add(room.exit);
      if (dir === 0) {
        size = new Point(2, 5);
        ent = new Point(1, 5);
        pos = absEnt.sub(ent);
        exit = new Point(1, 0);
      } else {
        size = new Point(5, 2);
        ent = new Point(0, 1);
        pos = absEnt.sub(ent);
        exit = new Point(5, 1);
      }
      room = {size : size, pos : pos, ent : ent, exit : exit};
      rooms.push(room);

      size = new Point(prob.rand(lrange[0], lrange[1]),
                       prob.rand(hrange[0], hrange[1]));
      absEnt = room.pos.add(room.exit);

      if (dir === 0) {
        size = size.flip();
        ent = new Point(prob.middle50(size.x), size.y);
        pos = absEnt.sub(ent);
      } else {
        ent = new Point(0, prob.middle50(size.y));
        pos = absEnt.sub(ent);
      }
      room = {size : size, pos : pos, ent : ent};
      rooms.push(room);
    }

    for (var i = 0; i < rooms.length; i++) {
      rooms[i].size = rooms[i].size.mult(scale);
      rooms[i].pos = rooms[i].pos.mult(scale);
      rooms[i].ent = rooms[i].ent.mult(scale);
      if (rooms[i].exit) {
        rooms[i].exit = rooms[i].exit.mult(scale);
      }
    }

    return rooms;
  },

  ensureRoom : function() {
    if (this.waitingUntil) {
      return this.rooms[this.heroPos];
    }

    if (this.roomCleared() && this.atExit()) {
      var room = this.rooms[this.heroPos];
      delete room.heros[this.hero.id];
      room.attackManager = undefined;
      this.heroPos += 1;
      room = this.rooms[this.heroPos];
      this.hero.initPos(room);
      room.heros[this.hero.id] = this.hero;
      this.attackManager.nextRoom(this.rooms[this.heroPos]);
      room.attackManager = this.attackManager;
      _.each(room.monsters, function(mon) { mon.revive(); });
      if (gl.settings.bossPause) {
        if (this.heroPos < this.rooms.length - 1) {
          var anyBoss = false;
          _.each(this.rooms[this.heroPos + 1].monsters, function(mon) {
            if (mon.spec.rarity === "boss") {
              anyBoss = true;
            }
          });
          if (anyBoss) {
            gl.pause();
          }
        }
      }

      gl.DirtyQueue.mark('zone:nextRoom');
    }
    return this.rooms[this.heroPos];
  },

  checkDone : function() {
    if (this.waitingUntil) {
      return;
    }

    if (!this.hero.isAlive()) {
      this.startWaiting({
        text : 'You Died!',
        type : 'death',
      });
      if (gl.settings.backOnDeath === true) {
        var zonesBack = Math.max(gl.settings.zonesBack, 0);
        this.nextZone -= zonesBack;
        this.nextZone = Math.max(this.nextZone, 0);
      }
    } else if (this.done()) {
      log.error('Zone %s index: %d cleared', this.nameStr, this.nextZone);
      this.tryUnlockNextZone();
      gl.DirtyQueue.mark('zone:nextRoom');
      if (this.settings.autoAdvance) {
        this.nextZone += 1;
      }
      gl.GameEvents.trigger('zoneClear');
      gl.GameEvents.trigger('reportData');

      this.startWaiting({
        text : 'Zone Cleared!',
        type : 'clear',
      });
    }
  },

  startWaiting : function(msgBase) {
    this.hero.moveStart = -1;
    this.messages.addMessage(_.extend(msgBase, {
      pos : this.hero.pos,
      color : '#FFF',
      lifespan : 2000,
      verticalOffset : 0,
      time : gl.time,
      expires : gl.time + 2000
    }));
    this.waitingUntil = gl.time + 2000;
  },

  tryUnlockNextZone : function() {
    gl.GameEvents.trigger('beatgame');
    if (this.nextZone === this.unlockedZones) {
      this.unlockedZones += 1;
      /*this.messages.addMessage({
          text: 'New Map Unlocked!',
          type: 'newlevel',
          pos: this.hero.pos,
          color: '#FFF',
          lifespan: 5000,
          verticalOffset: 0,
          time: gl.time,
          expires: gl.time + 5000});*/
      gl.DirtyQueue.mark('zone:unlocked');
      log.error('New map unlocked - #%d', this.unlockedZones);
    }
  },

  atExit : function() {
    var room = this.rooms[this.heroPos];
    return room.exit && this.hero.pos.equal(room.exit);
  },

  getNearbyPos : function(pos, range) {
    var room = this.getCurrentRoom();
    var res = new Point(0, 0);
    res.x = pos.x + Math.random() * range * 2 - range;
    res.x = Math.floor(Math.max(0, Math.min(room.size.x, res.x)));
    res.y = pos.y + Math.random() * range * 2 - range;
    res.y = Math.floor(Math.max(0, Math.min(room.size.y, res.y)));
    return res;
  },

  zoneTick : function() {
    let room, mons, heroes;

    if (this.waitingUntil) {
      if (gl.time > this.waitingUntil) {
        log.info('Getting new zone');
        this.hero.revive();
        this.newZone(this.nextZone);
        this.waitingUntil = 0;
      }
    } else {
      room = this.ensureRoom();
      _.each(this.liveHeroes(),
             function(h) { h.tryDoStuff(room, this.liveMons()); }, this);

      room = this.ensureRoom();
      _.each(this.liveMons(),
             function(mon) { mon.tryDoStuff(room, this.liveHeroes()); }, this);
    }
    this.attackManager.tick([ this.liveHeroes(), this.liveMons() ]);

    this.checkDone();

    gl.DirtyQueue.mark('zoneTick');
  },

  maxStep : function() {
    var room, mons, min, i;
    room = this.ensureRoom();
    mons = room.monsters;

    min = this.hero.nextAction;
    for (i = mons.length; i--;) {
      ml = Math.min(mons[i].nextAction, min);
    }
    return min - gl.time;
  },

  liveMons : function() {
    return _.filter(this.rooms[this.heroPos].monsters,
                    function(mon) { return mon.isAlive(); });
  },

  liveHeroes : function() {
    return _.filter(this.rooms[this.heroPos].heros,
                    function(h) { return h.isAlive(); });
  },

  getCurrentRoom : function() { return this.rooms[this.heroPos];},

  roomCleared : function() { return this.liveMons().length === 0;},

  done : function() {
    return this.roomCleared() && this.heroPos === this.rooms.length - 1;
  },

  getAttacks : function() { return this.attackManager.getAttacks();},
});

var ZoneMessages = Model.extend({
  initialize : function() {
    this.listenTo(gl.MessageEvents, 'message', this.addMessage);
    this.listenTo(gl.DirtyListener, 'zone:nextRoom', this.flush);
    this.listenTo(gl.DirtyListener, 'zone:new', this.flush);
    this.msgs = [];
  },

  addMessage : function(msgObj) {
    this.msgs.push(msgObj);
    this.prune();
  },

  prune : function() {
    // TODO: when messages die, make them pool on the ground, in iso fmt
    this.msgs = _.filter(this.msgs, function(msg) {
      return msg.expires > gl.time &&
             (!(msg.type === 'dmg') || msg.dmg.getY() > 0)
    });
  },

  flush : function() { this.msgs = []; }
});

/* exports.extend({
 *   ZoneManager : ZoneManager,
 *   MonsterBody : MonsterBody, // extended for use in test.js
 *   HeroBody : HeroBody
 * });*/
