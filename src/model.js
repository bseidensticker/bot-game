import * as Backbone from 'backbone';
import * as _ from 'underscore';

export var Model = function() {
  this.id = _.uniqueId('m');
  this.initialize.apply(this, arguments);
};

_.extend(Model.prototype, Backbone.Events, {initialize : function() {}});

Model.extend = Backbone.Model.extend;
