import * as Backbone from 'backbone';
import $ from 'jquery';
import * as _ from 'underscore';

import {gl} from './globals';
import {ref} from './itemref/itemref';
import log from './log';
import {Model} from './model';
import {Point} from './vectorutils';

/*
  TODO:
  Vis view's el is a div
  Make BackgroundView its own el=canvas view
  Make EntityView its own el=canvas view
  Make canvas full screen, abs positioned
  OnResize adjust global RATIO, REAL_SIZE so the max dimension is
  the min size of the screen
*/

var FOOTER_HEIGHT = 113;

var vvs = {}; // vis vars
var SIZE = 1000 * 10;

export var VisView = Backbone.View.extend({
  tagName : 'div',
  className : 'vis',

  events : {'mouseenter' : 'onMouseenter'},
  onMouseenter : function() {
    gl.UIEvents.trigger('itemSlotMouseleave');
  }, // this is because chrome's mouse leave doesn't work

  // this needs to get all zones, when game model changes, probably should get
  // all of gameModel
  initialize : function(options, game, gameView) {
    gl.vvsRatioMod = 1;
    log.info('visview init');
    this.zone = game.zone;
    this.gameView = gameView;

    this.updateConstants();

    this.bg = new BackgroundView({}, game);
    this.entity = new EntityView({}, game);

    this.listenTo(gl.DirtyListener, 'throttledResize', this.resize);
    this.resize();

    this.$el.append(this.bg.render().el);
    this.$el.append(this.entity.render().el);

    this.listenTo(gl.DirtyListener, 'tick', this.render);
    this.listenTo(gl.DirtyListener, 'hero:move', this.updateConstants);
    this.listenTo(gl.DirtyListener, 'centerChange', this.force);

    window.forceUpdateConstants = this.updateConstants.bind(this);
  },

  updateConstants : function() {
    var ss = new Point(window.innerWidth, window.innerHeight - FOOTER_HEIGHT);
    if (ss.y < 40) {
      ss.y = 40;
    }
    vvs.ss = ss.clone();

    if (ss.x / 2 > ss.y) { // if height is the limiting factor
      vvs.realSize = ss.y;
    } else {
      vvs.realSize = ss.x / 2;
    }
    vvs.ratio = vvs.realSize / SIZE * gl.vvsRatioMod;

    vvs.center = this.gameView.getCenter();
    vvs.cart = this.zone.hero.pos.mult(vvs.ratio);
    vvs.iso = vvs.cart.toIso();
    vvs.iso.y -= SIZE / 2;
    vvs.diff = vvs.center.sub(vvs.iso);
  },

  resize : function() {
    this.updateConstants();
    this.$el.css({width : vvs.ss.x, height : vvs.ss.y});

    this.bg.resize();
    this.entity.resize();
  },

  force : function() {
    this.updateConstants();
    this.bg.force();
  },

  render : function() { return this;},
});

function transpose(modelPoint) {
  var viewPoint = modelPoint.mult(vvs.ratio);
  viewPoint = viewPoint.toIso();
  viewPoint.y -= SIZE / 2;
  return viewPoint.add(vvs.diff);
}

var BackgroundTiler = Model.extend({
  initialize : function() {
    var names = ref.zoneOrder.order;
    this.tiles = {};
    for (var i = 0; i < names.length; i++) {
      this.tiles[names[i]] = new BackgroundTiles(names[i]);
    }
  },

  ready : function(zoneName) {
    this.tiles[zoneName].cache();
    return this.tiles[zoneName].imgLoaded && this.tiles[zoneName].cached;
  },

  get : function(zoneName) {
    var bt = this.tiles[zoneName];
    bt.cache();
    return bt.canvas;
  },
});

var BackgroundTiles = Model.extend({
  initialize : function(name) {
    this.canvas = document.createElement('canvas');
    this.cached = false;

    this.name = name;
    this.filename = 'assets/' + name.replace(/ /g, '_') + '.jpg';
    this.img = new Image();
    this.img.onload = (function() { this.imgLoaded = true; }).bind(this);
    this.img.src = this.filename;

    this.listenTo(gl.DirtyListener, 'throttledResize', this.resize);
  },

  getCanvasSize : function() {
    return new Point(Math.ceil(20000 * vvs.ratio),
                     Math.ceil(20000 * vvs.ratio));
  },

  cache : function() {
    if (!this.imgLoaded) {
      setTimeout(this.cache.bind(this), 50);
      return;
    }
    if (this.cached) {
      return;
    }

    var canvasSize = this.getCanvasSize();
    $(this.canvas).attr({width : canvasSize.x, height : canvasSize.y});

    var scaled = Math.floor(256 * vvs.ratio * 10);
    var ctx = this.canvas.getContext('2d');
    var pos, size;

    ctx.drawImage(this.img, 0, 0, 256, 256, 0, 0, scaled, scaled);

    // this still could be faster, uses too many args, could use a separate
    // tmp scaled canvas and draw with that

    for (var pos = scaled; pos <= canvasSize.x; pos *= 2) {
      ctx.drawImage(this.canvas, 0, 0, pos, pos, pos, 0, pos, pos);
      ctx.drawImage(this.canvas, 0, 0, pos, pos, pos, pos, pos, pos);
      ctx.drawImage(this.canvas, 0, 0, pos, pos, 0, pos, pos, pos);
    }

    this.cached = true;

    gl.DirtyQueue.mark('centerChange');
  },

  resize : function() { this.cached = false; }
});

var BackgroundView = Backbone.View.extend({
  tagName : 'canvas',
  className : 'bg',

  initialize : function(options, game) {
    this.zone = game.zone;

    this.redraw = true;
    this.reposition = true;

    this.tiler = new BackgroundTiler(this.zone.name);

    this.resize();
    this.listenTo(gl.DirtyListener, 'tick', this.render);
    this.listenTo(gl.DirtyListener, 'hero:move',
                  function() { this.redraw = true; });
    this.listenTo(gl.DirtyListener, 'zone:nextRoom',
                  function() { this.redraw = true; });
    this.listenTo(gl.DirtyListener, 'zone:start',
                  function() { this.redraw = true; });

    this.totalFrames = 0;
    this.totalTime = 0;
  },

  resize : function() {
    this.size = new Point(Math.max(window.innerWidth, 40),
                          Math.max(window.innerHeight - FOOTER_HEIGHT, 40));
    this.$el.attr({width : this.size.x, height : this.size.y});
    this.ctx = this.el.getContext('2d');
    this.force();
  },

  clear : function() {
    this.$el.attr('width', this.size.x);
    this.redraw = true;
  },

  force : function() {
    this.redraw = true;
    log.info('force background');
    this.render();
  },

  render : function() {
    if (this.redraw) {
      var t = new Date().getTime();
      // log.error('redrawing');
      this.clear();

      this.ctx.save();
      this.transform();
      this.drawBg();
      this.redraw = false;
      this.reposition = false;

      this.totalFrames += 1;
      this.totalTime += new Date().getTime() - t;

      if (this.totalFrames >= 60) {
        // log.warning('%d frames, %.3f ms per frame', this.totalFrames,
        // this.totalTime / this.totalFrames);
        this.totalFrames = 0;
        this.totalTime = 0;
      }
    }
    if (this.reposition) {
      log.error('repositioning');
      this.retransform();
      this.reposition = false;
    }
    return this;
  },

  transform : function() {
    var a = 1, b = 0.5, c = -1, d = 0.5;
    var coords = transpose(new Point(0, 0));
    this.lastPos = this.zone.hero.pos;
    this.ctx.setTransform(a, b, c, d, coords.x, coords.y);
  },

  retransform : function() {
    var a = 1, b = 0.5, c = -1, d = 0.5;
    var posDiff = this.zone.hero.pos.sub(this.lastPos);
    var coords = transpose(posDiff);
    this.ctx.setTransform(a, b, c, d, coords.x, coords.y);
  },

  drawBg : function() {
    if (!this.tiler.ready(this.zone.name)) {
      return;
    }
    var start = this.zone.heroPos - 4;
    var end = this.zone.heroPos + 4;
    if (start < 0) {
      start = 0;
    }
    if (end >= this.zone.rooms.length) {
      end = this.zone.rooms.length - 1;
    }

    var tiles = this.tiler.get(this.zone.name);

    for (var i = start; i <= end; i++) {
      var room = this.zone.rooms[i];
      var pos = room.pos.sub(this.zone.getCurrentRoom().pos);
      var size;

      pos = pos.mult(vvs.ratio);
      size = room.size.mult(vvs.ratio);
      this.ctx.drawImage(tiles, 0, 0, size.x, size.y, pos.x, pos.y, size.x,
                         size.y);
    }
  },
});

var EntityView = Backbone.View.extend({
  tagName : 'canvas',
  className : 'entity',

  initialize : function(options, game) {
    log.info('visview init');
    this.zone = game.zone;

    this.resize();
    this.listenTo(gl.DirtyListener, 'tick', this.render);
    this.tempCanvas = document.createElement('canvas');
    $(this.tempCanvas).attr({width : 2000, height : 400});
    this.tctx = this.tempCanvas.getContext('2d');
  },

  resize : function() {
    this.size = new Point(Math.max(window.innerWidth, 40),
                          Math.max(window.innerHeight - FOOTER_HEIGHT, 40));
    this.$el.attr({width : this.size.x, height : this.size.y});
  },

  clear : function() {
    this.el.getContext('2d').clearRect(0, 0, this.size.x, this.size.y);
  },

  render : function() {
    this.clear();
    var ctx = this.el.getContext('2d');

    // draw all mons
    var room = this.zone.ensureRoom();
    var mons = this.zone.liveMons();

    var drawables = [];

    _.each(mons, function(mon) { drawables.push(new BodyView(mon)); }, this);

    drawables.push(new BodyView(this.zone.hero));

    _.each(this.zone.getAttacks(),
           function(atk) { drawables.push(new AttackView(atk)); });

    sortDrawables(drawables);

    _.each(drawables,
           function(drawable) { drawable.draw(ctx, this.tempCanvas); }, this);

    this.zone.messages.prune();
    drawMessages(ctx, this.zone.messages.msgs);

    return this;
  },
});

function drawMessages(ctx, msgs) {
  // TODO: fix offset for separating messages about multiple item drops from
  // the same entity
  _.each(msgs, function(msg) {
    ctx.textAlign = 'center';
    ctx.textBaseline = 'bottom';
    ctx.font = Math.floor(vvs.ratio * 300) + 'px Source Code Pro';
    if (msg.type === 'dmg') {
      var dmg = msg.dmg;
      ctx.fillStyle = dmg.color;
      var base = transpose(dmg.getBase());
      base.y -= dmg.getY() * vvs.ratio;
      ctx.fillText(msg.text, base.x, base.y);
    } else {
      ctx.fillStyle = msg.color;
      var pos = transpose(msg.pos);
      if (msg.verticalOffset) {
        pos.y -= msg.verticalOffset * vvs.ratio;
      }
      ctx.fillText(msg.text, pos.x,
                   pos.y - (gl.time - msg.time) / msg.lifespan * 20);
    }
  });
}

function sortDrawables(drawables) {
  for (var i = drawables.length; i--;) {
    drawables[i].updateZ();
  }
  drawables.sort(function(a, b) { return a.z - b.z; });
}

function AttackView(atk) {
  this.atk = atk;
  this.z = 0;
}

AttackView.prototype.updateZ = function() {
  if (this.atk.type === 'cone' || this.atk.type === 'circle') {
    this.z = 0;
  } else {
    this.z = (this.atk.pos.x + this.atk.pos.y) / 2;
  }
};

AttackView.prototype.draw = function(ctx, tempCanvas) {
  if (this.atk.type === 'proj' && gl.time < this.atk.fireTime) {
    return;
  }
  if (this.atk.type === 'cone' && gl.time < this.atk.fireTime) {
    return;
  }
  if (this.atk.type === 'circle' && gl.time < this.atk.fireTime) {
    return;
  }

  var pos = transpose(this.atk.pos);
  pos.y -= this.atk.z * vvs.ratio;
  if (this.atk.type === 'circle') {
    flatCircle(ctx, this.atk);
  } else if (this.atk.type === 'cone') {
    flatArc(ctx, this.atk); // atk.start, atk. pos, this.atk.color,
                            // this.atk.radius * vvs.ratio, true);
  } else {
    circle(ctx, pos, this.atk.color, this.atk.projRadius * vvs.ratio);
  }
};

// Implements drawable interface
function BodyView(body) {
  this.body = body;
  this.z = 0;
  if (this.body.spec.color) {
    this.color = this.body.spec.color;
  } else {
    this.color =
        this.body.isHero() ? 'rgba(0, 150, 240, 1)' : 'rgba(240, 20, 30, 1)';
  }
}

BodyView.prototype.updateZ =
    function() { this.z = (this.body.pos.x + this.body.pos.y) / 2; };

BodyView.prototype.draw = function(ctx, tempCanvas) {
  var coords = transpose(this.body.pos);
  var p;
  var height = this.body.spec.height * vvs.ratio;
  var width = this.body.spec.width * vvs.ratio;
  ctx.lineCap = 'round';
  ctx.lineWidth = this.body.spec.lineWidth * vvs.ratio;

  // height *= 3 / 4

  var headPos = new Point(0, height * 67 / 72);
  var headSize = height * 10 / 72;
  var crotch = new Point(0, height * 23 / 72);
  var legSize = height * 23 / 72;
  var armPos = new Point(0, height * 45 / 72);
  var armSize = height * 28 / 72;
  var bodyPos = [ headPos, new Point(0, legSize) ];

  var opacity = Math.round(100 * this.body.spec.opacity) / 100;
  var actualColor = this.color.slice(0, this.color.length - 2) + opacity + ")";
  // console.log([this.color, actualColor]);
  ctx.strokeStyle = actualColor;
  // head
  circle(ctx, coords.sub(headPos), actualColor, headSize);
  // isoCircle(ctx, coords.sub(headPos), this.color, headSize, headSize,
  // true);

  // draw body, legs
  var legFrame = 0;
  if (this.body.moveStart > -1) {
    // range 0 to 2.
    var secPerWidth = this.body.spec.width / this.body.spec.moveSpeed *
                      8; // the * 8 makes it 8x slower than real
    legFrame =
        ((gl.time - this.body.moveStart) % secPerWidth) / secPerWidth * 2;
  }
  ctx.beginPath();
  lines(ctx, coords.sub(new Point(bodyPos[0].x, bodyPos[0].y - headSize)),
        coords.sub(bodyPos[1]),
        coords.add(new Point(width / 2 * (1 - legFrame), 0)));

  lines(ctx, coords.sub(bodyPos[1]),
        coords.sub(new Point(width / 2 * (1 - legFrame), 0)));

  // arms
  var rArm;
  var lArm;

  if (this.body.busy()) {
    var pct;
    if (this.body.lastDuration > 0) {
      pct = (this.body.nextAction - gl.time) / this.body.lastDuration;
    } else {
      pct = 1;
    }
    var ra = (pct + .1) * 1.3 * Math.PI * 2;
    var mra = Math.PI / 4 * Math.sin(ra);

    var la = pct * Math.PI * 2;
    var mla = Math.PI / 4 * Math.sin(la);

    rArm = new Point(Math.cos(mra) * width / 2, Math.sin(mra) * width / 2);
    lArm = new Point(Math.cos(mla + Math.PI) * width / 2,
                     Math.sin(mla + Math.PI) * width / 2);
  } else {
    rArm = new Point(width / 2, 0);
    lArm = new Point(-width / 2, 0);
  }

  var armBase = coords.sub(armPos);

  lines(ctx, armBase, armBase.add(rArm));

  lines(ctx, armBase, armBase.add(lArm));

  ctx.stroke();

  drawNameHealth(ctx, tempCanvas, this.body.spec.name,
                 coords.sub(new Point(0, height)),
                 this.body.hp / this.body.spec.maxHp);
};

function lines(ctx, p) {
  if (p) {
    ctx.moveTo(p.x, p.y);
  }
  for (var i = 2; i < arguments.length; i++) {
    ctx.lineTo(arguments[i].x, arguments[i].y);
  }
}

function drawNameHealth(ctx, tcanvas, text, pos, hpPct) {
  if (hpPct < 0) {
    hpPct = 0;
  }
  text = text.toUpperCase();

  var fontHeight = Math.floor(vvs.ratio * 300);

  ctx.fillStyle = '#111';
  ctx.textAlign = 'center';
  ctx.textBaseline = 'top';
  ctx.font = fontHeight + 'px Source Code Pro';
  ctx.fillText(text, pos.x, pos.y - fontHeight * 1.75);

  var tctx = tcanvas.getContext('2d');

  var textWidth = tctx.measureText(text).width;

  tctx.clearRect(0, 0, textWidth, fontHeight);

  tctx.fillStyle = '#e12';
  tctx.textAlign = 'left';
  tctx.textBaseline = 'top';
  tctx.font = fontHeight + 'px Source Code Pro';
  tctx.fillText(text, 0, 0);

  tctx.clearRect(textWidth * hpPct, 0, textWidth, fontHeight);
  ctx.drawImage(tcanvas, 0, 0, textWidth, fontHeight, pos.x - textWidth / 2,
                pos.y - fontHeight * 1.75, textWidth, fontHeight);
}

function circle(ctx, pos, color, radius) {
  ctx.beginPath();
  ctx.arc(pos.x, pos.y, radius, 0, 2 * Math.PI, false);
  ctx.closePath();
  ctx.fillStyle = color;
  ctx.fill();
}

function isoCircle(ctx, pos, color, width, height, fill) {
  ctx.strokeStyle = color;
  drawEllipseByCenter(ctx, pos.x, pos.y, width * 2, height * 2);
  ctx.closePath();
  if (fill) {
    ctx.fillStyle = color;
    ctx.fill();
  }
}

function drawEllipseByCenter(ctx, cx, cy, w, h) {
  drawEllipse(ctx, cx - w / 2.0, cy - h / 2.0, w, h);
}

function drawEllipse(ctx, x, y, w, h) {
  var kappa = .5522848,
      ox = (w / 2) * kappa, // control point offset horizontal
      oy = (h / 2) * kappa, // control point offset vertical
      xe = x + w,           // x-end
      ye = y + h,           // y-end
      xm = x + w / 2,       // x-middle
      ym = y + h / 2;       // y-middle

  ctx.beginPath();
  ctx.moveTo(x, ym);
  ctx.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
  ctx.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
  ctx.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
  ctx.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
  // ctx.closePath(); // not used correctly, see comments (use to close off
  // open path)
  ctx.stroke();
}

function flatArc(ctx, atk) {
  var outerRadius = atk.pos.sub(atk.start).len();
  var innerRadius = outerRadius - atk.aoeRadius / 2;
  if (innerRadius < 0) {
    innerRadius = 0;
  }
  outerRadius *= vvs.ratio;
  innerRadius *= vvs.ratio;

  var pos = transpose(atk.start);

  var a1 = atk.vector.rotate(-atk.angle / 2).angle();
  var a2 = atk.vector.rotate(atk.angle / 2).angle();

  var properAngle = function(a) {
    if (a < 0) {
      a += Math.PI * 2;
    }
    return a;
  };

  a1 = properAngle(a1);
  a2 = properAngle(a2);

  ctx.save();
  ctx.beginPath();

  var a = 1, b = 0.5, c = -1, d = 0.5;
  ctx.setTransform(a, b, c, d, pos.x, pos.y);

  ctx.fillStyle = atk.color;

  ctx.arc(0, 0, outerRadius, a1, a2, false);
  ctx.arc(0, 0, innerRadius, a2, a1, true);
  ctx.fill();

  ctx.restore();
}

function flatCircle(ctx, atk) {
  var outerRadius = atk.pos.sub(atk.start).len();
  var innerRadius = outerRadius - 200;
  if (innerRadius < 0) {
    innerRadius = 0;
  }
  outerRadius *= vvs.ratio;
  innerRadius *= vvs.ratio;

  var pos = transpose(atk.start);

  ctx.save();
  ctx.beginPath();

  var a = 1, b = 0.5, c = -1, d = 0.5;
  ctx.setTransform(a, b, c, d, pos.x, pos.y);

  ctx.fillStyle = atk.color;

  ctx.arc(0, 0, outerRadius, 0, Math.PI * 2, false);
  ctx.arc(0, 0, innerRadius, Math.PI * 2, 0, true);
  ctx.fill();

  ctx.restore();
}

// exports.extend({VisView : VisView});
