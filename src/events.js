import * as Backbone from 'backbone';
import * as _ from 'underscore';

import {gl} from './globals';
import log from './log';

export class DirtyQueueClass {
  constructor() {
    this.obj = {};
    this.eventMap = {};
  }

  mark(name) {
    if (this.obj[name]) {
      return;
    }
    this.obj[name] = true;
    var split = name.split(':');
    if (split.length > 1) {
      for (var i = 1; i < split.length; i++) {
        this.obj[split.slice(0, split.length - i).join(':')] = true;
      }
    }
  }

  // Make all event strings map to a single event string. Useful for making
  // multiple different
  //   event strings call a function once instead of once per event string
  mapMark(from, to) {
    if (typeof (from) === 'string') {
      from = [ from ];
    }
    var map = this.eventMap;
    _.each(from, function(f) {
      if (map[f] === undefined) {
        map[f] = [];
      }
      map[f].push(to);
    });
  }

  triggerAll(eventObject) {
    log.debug('Triggering All Events');

    _.each(this.eventMap, function(events, key) {
      if (this.obj[key]) {
        for (var i = 0; i < events.length; i++) {
          this.obj[events[i]] = true;
        }
      }
    }, this);

    _.each(this.obj, function(value, key, list) {
      if (value) {
        eventObject.trigger(key);
        this.obj[key] = false;
      }
    }, this);
  }
}
