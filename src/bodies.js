import * as _ from 'underscore';

import {Damage, DamageDealt} from './damage';
import {MonsterSpec} from './entity';
import {gl} from './globals';
import log from './log';
import {Model} from './model';
import {rand} from './prob';
import {prettifyNum} from './utils';
import {getDistances, Point} from './vectorutils';

var TEAM_HERO = 0;
var TEAM_MONSTER = 1;

var EntityBody = Model.extend({
  initialize : function(spec, zone) {
    this.spec = spec;
    this.zone = zone;
    this.createSkillchain();
    this.revive();
    this.pos = new Point(0, 0);
    this.nextAction = 0;
    this.moveStart = -1;
  },

  createSkillchain : function() {
    this.skills =
        _.map(_.compact(this.spec.skillchain.skills), function(skill) {
          return {spec : skill, coolAt : gl.time + skill.cooldownTime};
        });
  },

  revive : function() {
    this.takeAction(0);
    this.hp = this.spec.maxHp;
    this.mana = this.spec.maxMana;
    this.lastHpFullTime = gl.time;
    this.hpRegened = 0;
    this.lastManaFullTime = gl.time;
    this.manaRegened = 0;
    _.each(
        _.compact(this.skills),
        function(skill) { skill.coolAt = gl.time + skill.spec.cooldownTime; });
  },

  initPos : function(room) {
    if (this.isHero()) {
      this.pos = room.ent.clone();
      gl.DirtyQueue.mark('hero:move');
    } else if (this.isMonster()) {
      this.pos = new Point(rand(0, room.size.x), rand(0, room.size.y));
    }
  },

  isHero : function() { return this.spec.team === TEAM_HERO; },

  isMonster : function() { return this.spec.team === TEAM_MONSTER; },

  teamString : function() {
    if (this.isHero()) {
      return 'Hero';
    } else {
      return 'Monster';
    }
  },

  isAlive : function() { return this.hp > 0; },

  modifyHp : function(added) {
    this.hp += added;
    if (this.hp >= this.spec.maxHp) {
      this.hp = this.spec.maxHp;
      this.lastHpFullTime = gl.time;
      this.hpRegened = 0;
    }
  },

  modifyMana : function(added) {
    this.mana += added;
    if (this.mana >= this.spec.maxMana) {
      this.mana = this.spec.maxMana;
      this.lastManaFullTime = gl.time;
      this.manaRegened = 0;
    }
    if (this.mana < 0) {
      this.mana = 0;
      this.lastManaFullTime = gl.time;
      this.manaRegened = 0;
    }
  },

  regen : function() {
    if (gl.time > this.lastHpFullTime) {
      var total = this.spec.hpRegen * (gl.time - this.lastHpFullTime) / 1000;
      var toAdd = total - this.hpRegened;
      this.hpRegened = total;
      this.modifyHp(toAdd);
    }
    if (gl.time > this.lastManaFullTime) {
      var total =
          this.spec.manaRegen * (gl.time - this.lastManaFullTime) / 1000;
      var toAdd = total - this.manaRegened;
      this.manaRegened = total;
      this.modifyMana(toAdd);
    }
  },

  tryDoStuff : function(room, enemies) {
    this.regen();

    if (this.isHero() && enemies.length) {
      this.setSkillRangeMana(enemies);
    }
    if (!this.isAlive() || this.busy()) {
      return;
    }

    if (enemies.length === 0) {
      if (this.isHero()) {
        this.tryMove(enemies, distances, room);
      }
      _.each(_.compact(this.skills), function(skill) {
        skill.oom = skill.spec.manaCost > this.mana;
        skill.oor = true;
      }, this);
      return;
    }

    var distances = getDistances(this.pos, _.pluck(enemies, 'pos'));

    this.tryAttack(enemies, distances, room);
    this.tryMove(enemies, distances, room);
  },

  tryAttack : function(enemies, distances, room) {
    var minIndex = distances.minIndex();
    var minDist = distances[minIndex];
    var skills = _.compact(this.skills);
    var skill;

    this.setSkillRangeMana(enemies);

    for (var i = 0; i < skills.length; i++) { // use first skill that:
      skill = skills[i];
      if (skill.coolAt <= gl.time && !skill.oom && !skill.oor) {
        this.attackTarget(enemies[minIndex], skill, room);
        return;
      }
    }
  },

  setSkillRangeMana : function(enemies) {
    var mana = this.mana;
    var pos = this.pos;
    var minDist2 = pos.dist2(enemies[0].pos);
    var t;
    for (var i = enemies.length; i--;) {
      t = pos.dist2(enemies[i].pos);
      if (t < minDist2) {
        minDist2 = t;
      }
    }
    _.each(_.compact(this.skills), function(skill) {
      var r = skill.spec.range;
      skill.oom = skill.spec.manaCost > mana;
      skill.oor = (r * r) < minDist2;
    });
  },

  takeAction : function(duration) {
    this.moveStart = -1;
    this.nextAction = gl.time + duration;
    this.lastDuration = duration;
  },

  tryMove : function(enemies, distances, room) {
    if (this.busy()) {
      return;
    }
    if (this.moveStart === -1) {
      this.moveStart = gl.time;
    }

    var dist = this.spec.moveSpeed * gl.lastTimeIncr;
    var newPos;

    if (enemies.length === 0) {
      newPos = this.pos.closer(room.exit, dist, 0);
    } else {
      var target = enemies[distances.minIndex()];
      var range = _.map(_.compact(this.skills),
                        function(skill) { return skill.spec.range * .9; })
                      .min();
      if (!range) {
        range = 1;
      }
      if (isNaN(this.pos.x)) {
        console.log(this.pos);
      }
      newPos = this.pos.closer(target.pos, dist, range);

      var moveVect = newPos.sub(this.pos);
      var moveAngle = this.spec.moveAngle ? this.spec.moveAngle : 0;

      newPos = this.pos.add(moveVect.rotate(moveAngle)).inBounds(room.size);
    }

    if (this.pos.x === newPos.x && this.pos.y === newPos.y) {
      // didn't actually move, make legs not move
      this.moveStart = -1;
    } else {
      this.pos = newPos;
      if (this.isHero()) {
        gl.DirtyQueue.mark('hero:move');
      }
    }
  },

  attackTarget : function(target, skill, room) {
    skill.coolAt = gl.time + skill.spec.speed + skill.spec.cooldownTime;
    this.takeAction(skill.spec.speed);
    this.mana -= skill.spec.manaCost;
    gl.addAttack(skill, this, target);
  },

  handleHit : function(target, atk, dmgResult) {
    this.handleLeech(atk, dmgResult);
    if (!target.isAlive()) {
      this.onKill(target);
      target.onDeath();
    }
  },

  handleLeech : function(atk, dmgResult) {
    var hp = atk.hpOnHit + dmgResult.hpLeeched;
    var mana = atk.manaOnHit + dmgResult.manaLeeched;
    if (hp) {
      this.modifyHp(hp);
    }
    if (mana) {
      this.modifyMana(mana);
    }
  },

  rollHit : function(attack) {
    var hitChance =
        3 * attack.accuracy / (attack.accuracy + this.spec.dodge) *
        (0.5 + attack.attacker.spec.level /
                   (attack.attacker.spec.level + this.spec.level) / 2);

    if (hitChance > 0.99) {
      hitChance = 0.99;
    } else if (hitChance < 0.01) {
      hitChance = 0.01;
    }
    log.info('%s has %d%% chance to be hit by %s', this.spec.name,
             hitChance * 100, attack.attacker.spec.name);

    // log.error('%s has %d%% chance to be hit by %s', this.spec.name,
    // hitChance * 100, attack.attacker.spec.name);

    if (Math.random() < hitChance) {
      return true;
    }
    if ((this.spec.team === 0 && gl.settings.enableHeroDmgMsgs) ||
        (this.spec.team === 1 && gl.settings.enableMonDmgMsgs)) {
      gl.MessageEvents.trigger('message',
                               newZoneMessage('dodged!', 'dodge', this.pos,
                                              'rgba(230, 230, 10, 0.2)', 1000));
    }

    return false;
  },

  takeDamage : function(attack, isThorns) {
    var dealt = new DamageDealt(attack, this.spec);

    if (isNaN(dealt.total)) {
      console.log(dealt.total, attack.name);
      throw (
          'In body.takeDamage by ' + this.spec.name + ', ' +
          attack.attacker.spec.name +
          ' totalDmg has NaN value.  Monster missing card level in itemref?');
    }

    var allowedPct = dealt.total / attack.totalDmg;
    if (this.hp <= 0) {
      allowedPct = 0;
    }

    var result = {
      hpLeeched : allowedPct * attack.hpLeech,
      manaLeeched : allowedPct * attack.manaLeech,
      totalDmg : dealt.total
    };

    if (this.spec.team === TEAM_HERO) {
      log.debug('Team Hero taking %.2f damage', -dealt.total);
      if (dealt.total > this.hp && this.hp > 0) {
        log.reportDeath('hero killed by ' + attack.attacker.spec.name + ' at ' +
                        gl.sessionId + ' ' + gl.time);
        this.spec.lastDeath = attack.attacker.spec.name;
        gl.GameEvents.trigger('hero:death');
        if (gl.settings.pauseOnDeath && this.hp > 0) {
          gl.pause();
        }
      }
    }

    this.modifyHp(-dealt.total);

    log.debug('Team %s taking damage, hit for %s, now has %.2f hp',
              this.teamString(), dealt.total, this.hp);
    // TODO: Add rolling for dodge in here so we can sometimes return 0;

    if ((this.spec.team === 0 && gl.settings.enableHeroDmgMsgs) ||
        (this.spec.team === 1 && gl.settings.enableMonDmgMsgs)) {
      makeDamageMessages(attack, dealt, this.pos);
    }

    if (!isThorns) {
      var thornsAttack = {
        'physDmg' : this.spec.physThorns,
        'fireDmg' : this.spec.fireThorns,
        'coldDmg' : this.spec.coldThorns,
        'lightDmg' : this.spec.lightThorns,
        'poisDmg' : this.spec.poisThorns,
        'attacker' : this,
        'pos' : attack.attacker.pos,
        'start' : attack.attacker.pos,
        'hitHeight' : attack.hitHeight,
        'vector' : attack.vector.rotate(180),
      };
      attack.attacker.takeDamage(thornsAttack, true);
      if (attack.attacker.hp <= 0) {
        this.onKill(attack.attacker);
      }
    }

    return result;
  },

  busy : function() { return this.nextAction >= gl.time; },

  onKill : function() {},
  onDeath : function() {},

  fireHeight : function() { return this.spec.height / 2; },
});

export var HeroBody = EntityBody.extend({
  initialize : function(spec, zone) {
    this.potionCoolAt = gl.time;
    this.listenTo(spec.skillchain, 'skillComputeAttrs', this.updateSkillchain);
    this.listenTo(spec, 'computeAttrs', this.updateSkillchain);
    this.listenTo(spec.equipped, 'change', this.resetCooldowns);
    EntityBody.prototype.initialize.call(this, spec, zone);
  },

  resetCooldowns : function() {
    _.each(
        _.compact(this.skills),
        function(skill) { skill.coolAt = gl.time + skill.spec.cooldownTime; });
  },

  updateSkillchain : function() {
    log.info('HeroBody: updateSkillchain');
    var s = this.spec.skillchain;

    this.lastHpFullTime = gl.time;
    this.hpRegened = 0;
    this.lastManaFullTime = gl.time;
    this.manaRegened = 0;

    var lookup = {};
    for (var i = 0; i < this.skills.length; i++) {
      lookup[this.skills[i].spec.name] = this.skills[i];
    }

    var skills = _.filter(s.skills,
                          function(skill) { return skill && !skill.disabled; });

    this.skills = _.map(skills, function(skill) {
      if (skill.name in lookup) {
        return {spec : skill, coolAt : lookup[skill.name].coolAt};
      } else {
        return {spec : skill, coolAt : gl.time + skill.cooldownTime};
      }
    });
    gl.DirtyQueue.mark('bodySkillchainUpdated');
  },

  onKill : function(target) {
    var xpGained = target.spec.xpOnKill(this.spec.level);
    var levels = this.spec.applyXp(xpGained);
    if (levels > 0) {
      this.revive();
    }
    if (target.spec.deathSpawns !== undefined &&
        target.spec.deathSpawns.length > 0) {
      for (var i = 0; i < target.spec.deathSpawns.length; i++) {
        if (this.zone.rooms[this.zone.heroPos].monsters.length > 10000) {
          continue;
        }
        var newMon = new MonsterBody(target.spec.deathSpawns[i],
                                     target.spec.level, this.zone);
        newMon.pos = this.zone.getNearbyPos(target.pos, 1000);
        ;
        this.zone.rooms[this.zone.heroPos].monsters.push(newMon);
      }
    }
    var allDrops = target.spec.getDrops();
    if (allDrops.any) {
      var invMessages = this.spec.inv.addDrops(allDrops.gearDrops);
      var cardMessages = this.spec.cardInv.addDrops(allDrops.cardDrops);
      var matMessages = this.spec.matInv.addDrops(allDrops.matDrops);
      var messages = invMessages.concat(cardMessages.concat(matMessages));
      if (gl.settings.enableMatMsgs) {
        _.each(messages, function(message, index) {
          var color =
              (message.slice(0, 3) == 'New' || message.slice(0, 7) == 'Leveled')
                  ? 'rgba(255, 140, 0, 0.8)'
                  : 'rgba(255, 100, 0, 0.6)';
          gl.MessageEvents.trigger(
              'message',
              // TODO: Fix the offset here
              newZoneMessage(message, 'drop', target.pos, color, 1000,
                             target.spec.height / 2 + index * 300));
        }, this);
      }
    }
    gl.DirtyQueue.mark('monsters:death');
  },

  onDeath : function() {
    if (this.spec.level >= 100) {
      this.spec.xp =
          Math.max(0, this.spec.xp - this.spec.getNextLevelXp() / 200);
      gl.DirtyQueue.mark('hero:xp');
    }
    log.debug('your hero died');
  },

  modifyHp : function(added) {
    EntityBody.prototype.modifyHp.call(this, added);
    gl.DirtyQueue.mark('hero:hp');
  },

  modifyMana : function(added) {
    EntityBody.prototype.modifyMana.call(this, added);
    gl.DirtyQueue.mark('hero:mana');
  },

  revive : function() {
    this.potionCoolAt = gl.time;
    EntityBody.prototype.revive.call(this);
    gl.DirtyQueue.mark('revive');
  },

  tryUsePotion : function() {
    if (Math.abs(this.spec.maxHp - this.hp) < 0.00001 || this.hp <= 0.00001) {
      return;
    }
    if (this.potionCoolAt <= gl.time) {
      log.reportPotion();
      this.potionCoolAt = gl.time + 10000; // 10 second cooldown
      var addAmount =
          10 + this.spec.level * 20 * Math.pow(1.002, this.spec.level);
      this.modifyHp(addAmount);
      gl.MessageEvents.trigger(
          'message', newZoneMessage('potion worked!', 'potion', this.pos,
                                    'rgba(230, 230, 230, 0.7)', 1000));
    } else {
      gl.MessageEvents.trigger(
          'message', newZoneMessage('potion still cooling down!', 'potion',
                                    this.pos, 'rgba(230, 230, 230, 0.4)', 500));
    }
  }
});

gl.monsterSpecs = {};

export var MonsterBody = EntityBody.extend({
  initialize : function(name, level, zone) {
    var uid = name + '_' + level;
    var spec;
    if (uid in gl.monsterSpecs) {
      spec = gl.monsterSpecs[uid];
    } else {
      spec = new MonsterSpec(name, level);
      gl.monsterSpecs[uid] = spec;
    }
    EntityBody.prototype.initialize.call(this, spec, zone);
  }
});

function newZoneMessage(text, type, pos, color, lifespan, verticalOffset) {
  return {
    text : text,
    type : type,
    pos : pos,
    color : color,
    lifespan : lifespan,
    verticalOffset : verticalOffset,
    time : gl.time,
    expires : gl.time + lifespan
  };
}

function newDamageMessage(attack, pos, text, type) {
  var dmg =
      new Damage(attack.start, pos, attack.vector, attack.hitHeight, type);
  return {
    type : 'dmg',
    text : text,
    dmg : dmg,
    color : dmg.color,
    expires : gl.time + 2000
  };
}

function makeDamageMessages(attack, dealt, pos) {
  _.each([ 'phys', 'light', 'cold', 'fire', 'pois' ], function(type) {
    if (dealt[type] > 0) {
      gl.MessageEvents.trigger(
          'message',
          newDamageMessage(attack, pos, prettifyNum(Math.ceil(dealt[type])),
                           type));
    }
  });
}
