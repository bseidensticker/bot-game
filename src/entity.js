import * as _ from 'underscore';

import * as dropLib from './drops';
import {gl} from './globals';
import * as inventory from './inventory';
import * as itemref from './itemref/itemref';
import log from './log';
import {Model} from './model';
import * as prob from './prob';
import * as utils from './utils';

var TEAM_HERO = 0;
var TEAM_MONSTER = 1;

export var defKeys = [
  'strength', 'wisdom', 'dexterity', 'vitality', 'maxHp', 'maxMana', 'armor',
  'dodge', 'hpRegen', 'manaRegen', 'moveSpeed'
];
export var eleResistKeys =
    [ 'eleResistAll', 'fireResist', 'coldResist', 'lightResist', 'poisResist' ];
export var thornKeys =
    [ 'physThorns', 'fireThorns', 'coldThorns', 'lightThorns', 'poisThorns' ];
var visKeys = [ 'height', 'width', 'lineWidth', 'opacity' ];
export var dmgKeys = [
  'projCount',    'meleeDmg', 'rangeDmg', 'spellDmg',  'speed',
  'cooldownTime', 'physDmg',  'lightDmg', 'coldDmg',   'fireDmg',
  'poisDmg',      'hpOnHit',  'hpLeech',  'manaOnHit', 'manaLeech',
  'speed',        'accuracy', 'range',    'projRange', 'projRadius',
  'aoeRadius',    'manaCost', 'angle',    'projSpeed', 'aoeSpeed'
];

var allKeys = defKeys.concat(eleResistKeys)
                  .concat(thornKeys)
                  .concat(visKeys)
                  .concat(dmgKeys);

export var actualDmgKeys =
    [ 'physDmg', 'lightDmg', 'coldDmg', 'fireDmg', 'poisDmg' ];

export var attackSpecKeys = [//'meleeDmg', 'rangeDmg', 'spellDmg', 
                             'projCount', 'speed', 'physDmg', 'lightDmg', 'coldDmg', 'fireDmg', 'poisDmg',
                             'hpOnHit', 'hpLeech', 'manaOnHit', 'manaLeech',
                             // 'cooldownTime', 'manaCost',
                             'range', 'projRange', 'projRadius', 'aoeRadius', 'accuracy',
                             'angle', 'projSpeed', 'aoeSpeed'];
export var attackSpecDmgKeys = [
  'physDmg', 'lightDmg', 'coldDmg', 'fireDmg', 'poisDmg', 'hpLeech', 'manaLeech'
];

var EntitySpec = Model.extend({
  initialize : function() {
    this.level = 1;
    this.xp = 0;
  },

  computeAttrs : function() {
    if (this.team === TEAM_HERO) {
      this.weaponType = this.equipped.weapon === undefined
                            ? 'melee'
                            : this.equipped.weapon.weaponType;
    }

    var all = utils.newBaseStatsDict(allKeys);
    utils.addAllMods(all, this.getMods());

    if (this.team === TEAM_MONSTER) {
      all.armor.more *= 1 + (this.level * 0.02);
      all.eleResistAll.more *= 1 + (0.02 * this.level);
      all.maxHp.more *= Math.pow(1.01, this.level);
      if (this.level > 400) {
        all.armor.more *= 1 + ((this.level - 400) * 0.02);
        all.eleResistAll.more *= 1 + (0.02 * (this.level - 400));
        all.maxHp.more *= Math.pow(1.005, (this.level - 400));
      }
    } else if (this.team === TEAM_HERO) {
      _.each(
          this.prestige,
          function(val, stat) { all[stat].more *= 1 + (parseInt(val) * 0.01); },
          this);
      // console.log('applying prestige');
    }

    utils.computeStats(this, all, defKeys);

    // Now that def stats have been computed
    // all.eleResistAll.more *= Math.pow(0.998, this.wisdom);
    all.meleeDmg.more *= 1 + (this.strength * 0.001);
    all.rangeDmg.more *= 1 + (this.dexterity * 0.001);
    all.spellDmg.more *= 1 + (this.wisdom * 0.001);

    utils.computeStats(this, all, eleResistKeys);
    utils.computeStats(this, all, thornKeys);
    utils.computeStats(this, all, visKeys);

    this.baseDmg = all;
    this.computeSkillAttrs();

    gl.DirtyQueue.mark('computeAttrs');
  },

  computeSkillAttrs : function() {
    log.info('entity computeSkillAttrs, weaponType: %s', this.weaponType);
    this.skillchain.computeAttrs(this.baseDmg, this.weaponType);
  },

  getMods : function() {
    var mods = [
      'strength added 9',
      'strength added 1 perLevel',
      'dexterity added 9',
      'dexterity added 1 perLevel',
      'wisdom added 9',
      'wisdom added 1 perLevel',
      'vitality added 9',
      'vitality added 1 perLevel',

      'vitality gainedas 100 maxHp',
      'vitality gainedas 25 maxMana',
      'vitality gainedas 100 eleResistAll',

      'wisdom gainedas 100 maxMana',
      'wisdom gainedas 200 eleResistAll',

      'strength gainedas 200 armor',
      'dexterity gainedas 300 dodge',
      'dexterity gainedas 200 accuracy',

      'moveSpeed added 3',

      'height added 1000',
      'width added 300',
      'lineWidth added 30',

      // TODO - add str/dex/wis attacktype bonuses here once impemented
      //'strength gainedas 1 meleeDmg',
      //'dexterity gainedas 1 rangeDmg',
      //'wisdom gainedas 1 spellDmg',

      'meleeDmg added 1',
      'rangeDmg added 1',
      'spellDmg added 1',

      'eleResistAll gainedas 100 lightResist',
      'eleResistAll gainedas 100 coldResist',
      'eleResistAll gainedas 100 fireResist',
      'eleResistAll gainedas 100 poisResist',

      'maxHp added 20 perLevel',
      'maxMana added 5 perLevel',
      'maxHp more 2 perLevel',

      'maxMana gainedas 2 manaRegen',

      'projCount added 1',

      'angle added 15',
      'range gainedas 125 projRange',
      //'rate added 10',
      'projSpeed added 10',
      'aoeSpeed added 3',
      'projRadius added 50',
      'aoeRadius added 3000',
      'opacity added 1',
    ];
    return _.map(mods,
                 function(mod) { return utils.applyPerLevel(mod, this.level); },
                 this);
  },

  getNextLevelXp :
      function() { return Math.floor(100 * Math.pow(1.3, (this.level - 1))); },

  getLastLevelXp :
      function() { return Math.floor(100 * Math.pow(1.3, (this.level - 2))); },
});

var HeroSpec = EntitySpec.extend({
  initialize : function(name, skillchain, inv, equipped, cardInv, matInv) {
    this.name = name;
    this.skillchain = skillchain;
    this.inv = inv;
    this.cardInv = cardInv;
    this.matInv = matInv;
    this.equipped = equipped;
    this.versionCreated = gl.VERSION_NUMBER;
    this.lastDeath = 'Hardcore';
    this.moveAngle = 0;
    this.prestigeTotal = localStorage.getItem('prestigeTotal');
    localStorage.setItem('prestigeTotal', 'valid');

    EntitySpec.prototype.initialize.call(this);
    this.team = TEAM_HERO;

    log.info('HeroSpec initialize');
    this.computeAttrs();

    this.listenTo(this.skillchain, 'change', this.computeSkillAttrs);
    this.listenTo(this.equipped, 'change', this.computeAttrs);
  },

  toJSON : function() {
    return {
      name : this.name,
      level : this.level,
      xp : this.xp,
      versionCreated : this.versionCreated,
      lastDeath : this.lastDeath,
      moveAngle : this.moveAngle,
      prestigeTotal : this.prestigeTotal,
      prestige : this.prestige,
    };
  },

  fromJSON : function(data) { _.extend(this, data); },

  getMods : function() {
    var mods = EntitySpec.prototype.getMods.call(this);
    return mods.concat(this.equipped.getMods());
  },

  applyXp : function(xp) {
    if (localStorage.getItem('prestigeTotal') !== 'valid') {
      log.error('invalid prestige ' + localStorage.getItem('prestigeTotal'));
    }
    // TODO needs to do this to the skillchain as well
    gl.DirtyQueue.mark('hero:xp');
    var levels = 0;
    levels += this.equipped.applyXp(xp);
    levels += this.skillchain.applyXp(xp);
    this.xp += xp;
    while (this.xp >= this.getNextLevelXp()) {
      this.xp -= this.getNextLevelXp();
      this.level += 1;
      gl.DirtyQueue.mark('hero:levelup');
      log.levelUp(this.level);
      levels++;
    }
    if (levels > 0) {
      this.computeAttrs();
    }
    return levels;
  }
});

export var MonsterSpec = EntitySpec.extend({
  // TODO: fn signature needs to be (name, level)
  initialize : function(name, level) {
    // All you need is a name
    EntitySpec.prototype.initialize.call(this);
    this.team = TEAM_MONSTER;
    this.name = name;
    this.level = level;

    _.extend(this, itemref.expand('monster', this.name));

    this.weaponType = 'melee';

    this.mods = _.map(this.items, function(item) {
      var expanded = itemref.expand(item[0], item[1]);
      if (item[0] === 'weapon') {
        this.weaponType = expanded.weaponType;
      }
      return expanded.mods;
    }, this);
    this.mods = _.flatten(this.mods);
    try {
      this.mods = this.mods.concat(utils.expandSourceCards(
          this.sourceCards, Math.floor(this.level / 10)));
    } catch (e) {
      log.error('cannot find reference for some card held by %s', this.name);
    }

    this.droppableCards = _.filter(
        this.sourceCards,
        function(card) { return card[0].slice(0, 5) !== 'proto'; }, this);

    this.skillchain = new inventory.Skillchain();
    _.each(this.skills, function(skill, i) {
      var skill = new inventory.MonsterSkillModel(skill);
      skill.level = this.level;
      this.skillchain.equip(skill, i, true);
    }, this);

    this.computeAttrs();
  },

  getMods : function() {
    return this.mods.concat(EntitySpec.prototype.getMods.call(this));
  },

  getDrops : function() {
    var cardDrops = [];
    var gearDrops = [];
    var matDrops = [];
    var any = false;
    if (Math.random() < 1) { // 0.03 * 10) {
      if (this.droppableCards.length) {
        var card =
            this.droppableCards[prob.pyRand(0, this.droppableCards.length)];
        // Changed so monsters over level 100 drop level reduced cards to slow
        // card qp gain
        var clvl = this.level > 100 ? Math.floor(Math.sqrt(this.level))
                                    : Math.floor(this.level / 10);

        card = [ card[0], card[1] + clvl ];
        cardDrops.push(dropLib.dropFactory('card', card));
        any = true;
      }
    }
    if (Math.random() < 1) { // 0.001 * 50) {
      if (this.materials && this.materials.length >= 1) {
        var matDrop = this.getMatDrop();
        matDrops.push(
            dropLib.dropFactory('material', [ matDrop.name, matDrop.amt ]));
        any = true;
      }
    }
    if (Math.random() < 0.1) { // 0.001 * 50) {
      if (this.items.length) {
        gearDrops.push(dropLib.dropFactory(
            'item', this.items[prob.pyRand(0, this.items.length)]));
        any = true;
      }
    }
    if (Math.random() < 0.5) { // 0.001 * 50) {
      if (this.skills.length) {
        gearDrops.push(dropLib.dropFactory(
            'skill', this.skills[prob.pyRand(0, this.skills.length)]));
        any = true;
      }
    }
    return {
      cardDrops : cardDrops,
      gearDrops : gearDrops,
      matDrops : matDrops,
      any : any
    };
  },

  getMatDrop : function() {
    this.rarity = this.rarity === undefined ? 'normal' : this.rarity;
    var rates = itemref.ref.matDropRates[this.rarity];
    var roll = Math.random();
    var sum = 0;
    var category, catMats, amt;
    for (var i = 0; i < rates.length; i++) {
      sum += rates[i];
      if (roll <= sum) {
        category = Math.abs(i - 4);
        break;
      }
    }
    if (category === 4) {
      catMats = [ 'energy', 'skull', 'heart', 'finger', 'toe', 'handle' ];
    } else {
      catMats = _.filter(this.materials, function(matName) {
        return itemref.ref.materials[matName].category === category;
      }, this);
    }

    var chosenMat = catMats[prob.pyRand(0, catMats.length)];

    if (chosenMat === undefined) {
      log.error('getMatDrop error choosing drops, ended with undefined drop');
    }

    var base = itemref.ref.matCategoryBase[category];
    // monster base slightly less than card levelup cost, to diverge at higher
    // levels.

    base = (base * 2 + 1) / 3;

    amt = Math.ceil(Math.pow(base, Math.max(1, (this.level - 2) / 20)));

    // console.log(chosenMat, amt);
    return {'name' : chosenMat, 'amt' : amt};
  },

  // TODO: memoize this
  xpOnKill : function(playerLevel) {
    var mlevel = playerLevel >= this.level
                     ? this.level
                     : playerLevel + ((this.level - playerLevel) / 20);
    return Math.ceil(20 * Math.pow(1.18, (mlevel - 1)));
  },

  xpPenalty : function(pl, ml) {
    if (pl > ml) {
      return 1;
    }
    var sb = 3 + Math.floor(pl / 16);
    var diff = ml - pl;
    if (diff <= sb) {
      return 1;
    }
    var ed = diff - sb;
    return Math.pow((pl + 5) / (pl + 5 + Math.pow(ed, 2.5)), 1.5);
  }
});

export function newHeroSpec(inv, cardInv, matInv) {
  var heroName = 'some newbie';
  var equipped = new inventory.EquippedGearModel();
  var skillchain = new inventory.Skillchain();

  var hero = new HeroSpec(heroName, skillchain, inv, equipped, cardInv, matInv);

  return hero;
}

/* exports.extend({
 *   newHeroSpec : newHeroSpec,
 *   MonsterSpec : MonsterSpec,
 *   defKeys : defKeys,
 *   eleResistKeys : eleResistKeys,
 *   dmgKeys : dmgKeys,
 *   thornKeys : thornKeys,
 *   actualDmgKeys : actualDmgKeys,
 *   attackSpecKeys : attackSpecKeys,
 *   attackSpecDmgKeys : attackSpecDmgKeys
 * });*/
