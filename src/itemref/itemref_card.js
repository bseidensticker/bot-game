export var card = {
  'proto-skeleton' : {
    'mods' : [
      'fireResist more 20',
    ],
  },
  'proto-grunt' : {
    'mods' : [
      'maxHp more -50',
      'physDmg more -30',
    ],
  },
  'proto-boss' : {
    'mods' : [
      'lineWidth added 30', 'width more 100', 'height more 100',
      'physDmg more 100', 'maxHp more 500'
    ],
  },
  'proto-bat' : {
    'mods' : [
      'height more -80',
    ]
  },
  'proto-rofl' : {
    'mods' : [
      'height more -50',
      'width more 300',
    ]
  },
  'proto-buddha' : {
    'mods' : [
      'maxHp more 1 perLevel',
      'accuracy more 1000 perLevel',
      'lineWidth added 300',
      'height more -30',
      'width more 50',
    ],
  },
  'proto-acheron' : {
    'mods' : [
      'range more 70',
      'cooldownTime added 10',
    ],
  },
  'proto-colossus' : {
    'mods' : [
      'cooldownTime more -99', 'accuracy more 1000000', 'accuracy added 1000000'
    ],
  },
  'proto-tallman' : {
    'mods' : [
      'height more 200',
      'width more -50',
    ],
  },
  'sharpened' : {
    'mods' : [
      'physDmg added 3 perLevel',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'metal', 'blade' ],
  },
  'faster detonation' : {
    'mods' : [
      'aoeRadius more 50',
      'speed more -2 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'gargoyle', 'lichen', 'business card', 'energy' ],
  },
  'flash' : {
    'mods' : [
      'moveSpeed gainedas 3 aoeSpeed',
      'lightDmg more 3 perLevel',
      'lightResist more 3 perLevel',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'spark', 'spark', 'nightmare', 'nightmare' ],
    'credit' : 'WirelessKFC',
  },
  'mobility' : {
    'mods' : [
      'speed converted 80 cooldownTime', 'cooldownTime added 100',
      'cooldownTime more 50', 'cooldownTime more -1 perLevel'
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'wing', 'converter', 'gnome' ],
    'flavor' : 'Less Skill Duration means More Moving'
  },
  'hot sword' : {
    'mods' : [ 'fireDmg added 5 perLevel' ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'ember', 'metal', 'blade' ],
  },
  'cold sword' : {
    'mods' : [ 'coldDmg added 5 perLevel' ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'ice', 'metal', 'blade' ],
  },
  'breadhat' : {
    'mods' : [
      'armor added 10 perLevel',
      'armor more 1 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'metal', 'shield' ],
  },
  'six pack' : {
    'mods' : [
      'lineWidth added 30',
      'armor added 10 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'metal', 'shield' ],
  },
  'steel toed' : {
    'mods' : [
      'armor added 10 perLevel', 'armor more 0.5 perLevel', 'moveSpeed more -30'
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'metal', 'shield' ],
  },
  'quenching blade' : {
    'mods' : [ 'fireResist more 7 perLevel' ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'ember', 'shield' ],
  },
  'enchant weapon' : {
    'mods' : [ 'eleResistAll more 3 perLevel' ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'brain', 'shield' ],
  },
  'possessed weapon' : {
    'mods' : [
      'physDmg gainedas 0.5 hpLeech',
      'wisdom more -50',
      'physDmg more 0.5 perLevel',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'metal', 'needle', 'razor' ],
  },
  'multi-fingered' : {
    'mods' : [
      'accuracy more 20',
      'physDmg more 20',
      'accuracy more 1 perLevel',
      'physDmg more 1 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'metal', 'eye', 'spike' ],
  },
  'derping out' : {
    'mods' : [
      'wisdom more -75',
      'armor more 2 perLevel',
      'dodge more 2 perLevel',
      'eleResistAll more 2 perLevel',
    ],
    'slot' : 'weapon',
    'materials' : [ 'skull', 'shield', 'eye', 'muscle' ],
  },
  'cool shoes' : {
    'mods' : [ 'fireResist more 7 perLevel' ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'ember', 'shield' ],
  },
  'compression shorts' : {
    'mods' : [
      'moveSpeed more 30',
      'moveSpeed more 1 perLevel',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'wing', 'spike' ],
  },
  'asbestos lining' : {
    'mods' : [ 'fireResist more 7 perLevel' ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'ember', 'shield' ],
  },
  'sopping underclothes' : {
    'mods' : [
      'fireResist more 50', 'coldResist more -50', 'fireResist more 3 perLevel'
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'ember', 'shield', 'razor' ],
  },
  'brain juice' : {
    'mods' : [ 'manaRegen added 2 perLevel' ],
    'slot' : 'head',
    'materials' : [ 'skull', 'brain', 'potion' ],
  },
  'heart juice' : {
    'mods' : [
      'hpRegen added 2 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'blood', 'potion' ],
  },
  'head of vigor' : {
    'mods' : [ 'vitality added 20 perLevel' ],
    'slot' : 'head',
    'materials' : [ 'skull', 'blood', 'blade' ],
  },
  'nimble' : {
    'mods' : [ 'dexterity added 5 perLevel', 'dexterity added 20' ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'eye', 'blade' ],
  },
  'bloodsucker' : {
    'mods' : [ 'physDmg gainedas 0.3 hpLeech', 'physDmg added 5 perLevel' ],
    'slot' : 'head',
    'materials' : [ 'skull', 'blood', 'needle', 'spine' ],
    'rarity' : 'rare'
  },
  'strong back' : {
    'mods' : [
      'strength added 10 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'muscle', 'blade' ],
  },
  'holy shield' : {
    'mods' : [
      'armor converted 20 eleResistAll',
      'armor more 4 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'metal', 'spike', 'converter' ],
  },
  'sacred shield' : {
    'mods' : [
      'armor gainedas 20 physThorns',
      'physThorns more 2 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'mirror', 'shield', 'converter' ],
  },
  'ascended' : {
    'mods' : [
      'strength more 0.7 perLevel',
      'dexterity more 0.7 perLevel',
      'wisdom more 0.7 perLevel',
      'vitality more 0.7 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'sigil', 'muscle', 'brain', 'eye' ],
  },
  'aura' : {
    'mods' : [
      'maxHp gainedas 0.5 hpRegen',
      'spellDmg more 2 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'blood', 'brain', 'spike' ],
  },
  'thwomping' : {
    'mods' : [
      'physDmg more 5 perLevel',
      'physDmg more 25',
      'moveSpeed more -50',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'metal', 'feather', 'razor' ],
  },
  'dancing walk' : {
    'mods' : [
      'dodge more 30',
      'dodge more 1 perLevel',
      'moveSpeed more -50',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'feather', 'razor' ],
  },
  'dexterous hands' : {
    'mods' : [
      'dexterity added 5 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'feather', 'blade' ],
  },
  'dummy' : {
    'mods' : [
      'moveSpeed added -300',
    ],
    'slot' : 'head',
  },
  'fear' : {
    'mods' : [
      'moveSpeed added -400',
    ],
    'slot' : 'legs',
  },
  'more projectiles' : {
    'mods' : [
      'projCount added 2', 'angle more 20', 'speed more -0.5 perLevel',
      'manaCost added 2'
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'eye', 'feather', 'imp head' ]
  },
  'stinging' : {
    'mods' : [
      'physDmg added 3 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'metal', 'blade' ],
  },
  'ignited' : {
    'mods' : [
      'physDmg converted 20 fireDmg',
      'fireDmg more 3 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'ember', 'spike', 'converter' ],
  },
  'frosted' : {
    'mods' : [
      'physDmg converted 20 coldDmg',
      'coldDmg more 3 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'ice', 'spike', 'converter' ],
  },
  'charged' : {
    'mods' : [
      'physDmg converted 20 lightDmg',
      'lightDmg more 3 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'spark', 'spike', 'converter' ],
  },
  'putrefied' : {
    'mods' : [
      'physDmg converted 20 poisDmg',
      'poisDmg more 3 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'spore', 'spike', 'converter' ],
  },
  'heart of granite' : {
    'mods' : [
      'armor added 5 perLevel',
      'armor more 3 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'metal', 'blade', 'spike' ],
  },
  'small stature' : {
    'mods' : [
      'height more -30',
      'width more -30',
      'moveSpeed more 1 perLevel',
      'dodge added 50',
      'dodge more 3 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'feather', 'spike', 'imp head' ],
  },
  'keen wit' : {
    'mods' : [
      'wisdom added 10 perLevel',

    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'brain', 'blade' ],
  },
  'textbook' : {
    'mods' : [
      'wisdom added 10 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'brain', 'blade' ],
  },
  'electrified' : {
    'mods' : [
      'lightDmg more 4 perLevel',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'spark', 'spike' ],
  },
  'festering' : {
    'mods' : [
      'poisDmg more 4 perLevel',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'spore', 'spike' ],
  },
  'blazing' : {
    'mods' : [
      'fireDmg more 4 perLevel',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'ember', 'spike' ],
  },
  'iced' : {
    'mods' : [
      'coldDmg more 4 perLevel',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'ice', 'spike' ],
  },
  'flying' : {
    'mods' : [
      'armor more -20', 'dodge added 95', 'dodge added 5 perLevel',
      // TODO - flying vis stuff
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'feather', 'razor' ],
  },
  'clawed' : {
    'mods' : [
      'physDmg added 3 perLevel',
      'physDmg more 10',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'metal', 'blade', 'spike' ],
  },
  'riveted' : {
    'mods' : [
      'lineWidth added 30',
      'armor more 5 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'metal', 'shield' ],
  },
  'clockwork' : {
    'mods' : [
      'physDmg more 5 perLevel',
      'speed more -15',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'metal', 'spike', 'gnome' ],
  },
  'mecha heart' : {
    'mods' : [
      'lineWidth added 30',
      'vitality added 5 perLevel',
      'hpRegen added 5 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'blood', 'blade', 'potion' ],
  },
  'steam powered' : {
    'mods' : [
      'manaRegen added 10 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'brain', 'potion', 'gnome' ],
    'flavor' : 'all hail gaben',
  },
  'goblin toe' : {
    'mods' : [
      'armor added 10 perLevel',
      'physDmg more 2 perLevel',
      'physDmg more 25',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'metal', 'spike', 'crag shard' ],
  },
  'berserking' : {
    'mods' : [
      'physDmg more 20',
      'physDmg more 1 perLevel',
      'speed more -0.3 perLevel',
      'maxHp more -50',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'metal', 'blood', 'razor' ],
  },
  'simple minded' : {
    'mods' : [
      'spellDmg more -30',
      'strength more 2 perLevel',
      'meleeDmg more 2 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'muscle', 'spike', 'razor' ],
  },
  'explosive bolts' : {
    'mods' : [
      'rangeDmg more 0.5 perLevel',
      'physDmg converted 25 fireDmg',
    ],
    'slot' : 'skill',
    'types' : [ 'range' ],
    'materials' : [ 'energy', 'eye', 'ember', 'converter' ],
  },
  'shambling' : {
    'mods' : [
      'moveSpeed more -20',
      'physDmg more 10',
      'physDmg more 3 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'metal', 'razor' ],
  },
  'unwashed hands' : {
    'mods' : [
      'physDmg converted 25 poisDmg',
      'poisDmg more 3 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'spore', 'converter' ]
  },
  'icy hands' : {
    'mods' : [
      'coldDmg more 3 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'ice', 'spike' ]
  },
  'charged hands' : {
    'mods' : [
      'lightDmg more 3 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'spark', 'spike' ]
  },
  'hot hands' : {
    'mods' : [
      'fireDmg more 3 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'ember', 'spike' ]
  },
  'indigenous toxins' : {
    'mods' : [
      'poisDmg more 10',
      'poisDmg added 3 perLevel',
      'poisDmg more 1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'spore', 'spike', 'blade' ],
  },
  'swamp armor' : {
    'mods' : [
      'poisResist more 5 perLevel',
      'poisResist more 10',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'spore', 'shield' ],
  },
  'big' : {
    'mods' : [
      'height more 30',
      'width more 30',
      'vitality added 10 perLevel',
      'maxHp more 2 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'blood', 'muscle', 'spike' ],
  },
  'colossus' : {
    'mods' : [
      'height more 200', 'width more 200', 'strength more -100',
      'dexterity more -100', 'wisdom more -100', 'vitality more -50',
      'cooldownTime added 999999', 'armor more 3 perLevel',
      'armor gainedas 100 physThorns', 'fireResist gainedas 100 fireThorns',
      'coldResist gainedas 100 coldThorns',
      'lightResist gainedas 100 lightThorns',
      'poisResist gainedas 100 poisThorns'
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'blood', 'razor', 'nightmare' ]
  },
  'proto-bigger' : {
    'mods' : [
      'height more 50',
      'width more 50',
      'lineWidth more 50',
    ],
  },
  'buff' : {
    'mods' : [
      'width more 30',
      'lineWidth added 30',
      'strength added 5 perLevel',
      'meleeDmg more 3 perLevel',
      'rangeDmg more 3 perLevel',
      'vitality added 3 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'muscle', 'eye', 'spike' ],
  },
  'vampyric touch' : {
    'mods' : [
      'physDmg gainedas 0.3 hpLeech',
      'physDmg more 2 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'metal', 'needle', 'gargoyle' ]
  },
  'vampyric embrace' : {
    'mods' : [
      'physDmg gainedas 0.3 hpLeech',
      'physDmg more 3 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'blood', 'needle', 'gargoyle' ]
  },
  'soulsucker' : {
    'mods' : [
      'physDmg gainedas 0.5 manaLeech',
      'physDmg more 2 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'metal', 'brain', 'needle' ],
  },
  'alabaster' : {
    'mods' : [
      'armor more 10 perLevel',
      'armor added 100',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'metal', 'shield', 'gargoyle' ],
  },
  'vest pockets' : {
    'mods' : [
      'speed more -15',
      'speed more -0.5 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'wing', 'blade' ],
  },
  'precise' : {
    'mods' : [
      'speed more 20',
      'physDmg more 10',
      'physDmg more 3 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'metal', 'razor' ],
  },
  'fleece lining' : {
    'mods' : [
      'coldResist more 20',
      'coldResist more 5 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'ice', 'shield' ],
  },
  'fur hat' : {
    'mods' : [
      'coldResist more 20',
      'coldResist more 5 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'ice', 'shield' ],
  },
  'chinchilla lining' : {
    'mods' : [
      'coldResist more 20',
      'coldResist more 5 perLevel',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'ice', 'shield' ],
  },
  'yeti fur' : {
    'mods' : [
      'coldResist more 20',
      'coldResist more 7 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'ice', 'shield' ],
  },
  'ice plating' : {
    'mods' : [
      'armor more 3 perLevel',
      'fireResist more 3 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'ember', 'metal', 'shield' ],
  },
  'blue ice' : {
    'mods' : [ 'coldDmg gainedas 30 poisDmg', 'coldDmg more 3 perLevel' ],
    'slot' : 'skill',
    'rarity' : 'rare',
    'flavor' : 'I am not in danger, I am the danger.',
    'materials' : [ 'energy', 'spore', 'converter', 'wight snow' ]
  },
  'refridgerator' : {
    'mods' : [
      'lightDmg converted 100 coldDmg',
      'lightDmg more -30',
      'lightDmg more 3 perLevel',
    ],
    'slot' : 'chest',
    'rarity' : 'rare',
    'materials' : [ 'heart', 'spark', 'converter', 'gnome' ]
  },
  'cycled attack' : {
    'mods' : [
      'speed gainedas 250 cooldownTime',
      'speed more -0.3 perLevel',
    ],
    'slot' : 'hands',
    'rarity' : 'rare',
    'flavor' : 'Variety is the spice of life.',
    'materials' : [ 'finger', 'wing', 'razor', 'wight snow' ]
  },
  'chilly powder' : {
    'mods' : [ 'coldDmg gainedas 30 fireDmg', 'coldDmg more 5 perLevel' ],
    'slot' : 'skill',
    'rarity' : 'rare',
    'flavor' : 'Chilly P is my signature!',
    'materials' : [ 'energy', 'ember', 'converter', 'wight snow' ],
  },
  'chemistry' : {
    'mods' : [
      'fireDmg converted 100 poisDmg', 'fireDmg more 3 perLevel',
      'fireResist more -50'
    ],
    'slot' : 'hands',
    'rarity' : 'rare',
    'flavor' : 'Science, bitch',
    'materials' : [ 'finger', 'spore', 'converter', 'crag shard' ],
  },
  'shadow walker' : {
    'mods' : [
      'opacity more -20',
      'dodge added 20 perLevel',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'feather', 'shield' ],
  },
  'somnambulate' : {
    'mods' : [
      'dodge more 3 perLevel',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'feather', 'shield' ],
  },
  'full plating' : {
    'mods' : [
      'armor added 30 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'metal', 'shield' ],
  },
  'hateful blade' : {
    'mods' : [
      'physDmg gainedas -0.6 hpLeech',
      'physDmg more 20',
      'physDmg more 5 perLevel',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'metal', 'razor', 'lichen' ],
  },
  'ethereal' : {
    'mods' : [
      'dodge more 4 perLevel',
      'dodge added 100',
      'opacity more -60',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'feather', 'shield', 'business card' ],
  },
  'invisibility' : {
    'mods' : [
      'dodge more 3 perLevel',
      'dodge added 100',
      'opacity more -100',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'feather', 'shield', 'nightmare' ],
  },
  'liquefied body' : {
    'mods' : [
      'height more -100',
      'moveSpeed more -30',
      'dodge more 100',
      'dodge more 2 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'feather', 'shield', 'slime' ]
  },
  'spiked' : {
    'mods' : [
      'physThorns added 10 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'metal', 'razor', 'slime' ]
  },
  'pyromania' : {
    'mods' : [
      'fireDmg more 8',
      'fireDmg more 5 perLevel',
      'fireDmg gainedas 0.5 hpLeech',
      'fireResist more -50',
    ],
    'slot' : 'head',
    'rarity' : 'rare',
    'materials' : [ 'skull', 'ember', 'needle', 'spine' ],
  },
  'life on hit' : {
    'mods' : [
      'hpOnHit added 1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'blood', 'potion' ]
  },
  'gratifying blow' : {
    'mods' : [
      'hpOnHit added 3 perLevel',
      'manaCost more 100',
    ],
    'slot' : 'skill',
    'rarity' : 'rare',
    'materials' : [ 'energy', 'blood', 'potion', 'razor' ]
  },
  'mana on hit' : {
    'mods' : [
      'manaOnHit added 2 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'brain', 'potion' ],
  },
  'mana drinker' : {
    'mods' : [
      'manaOnHit added 5 perLevel',
    ],
    'slot' : 'skill',
    'rarity' : 'rare',
    'materials' : [ 'energy', 'blood', 'potion', 'gnome' ]
  },
  'potion guzzler' : {
    'mods' : [
      'hpRegen more 2 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'slime', 'slime', 'blood' ]
  },
  'earth commune' : {
    'mods' : [
      'hpRegen more 2 perLevel',
    ],
    'slot' : 'legs',
    'materials' : [ 'slime', 'slime', 'blood' ],
  },
  'blessed' : {
    'mods' : [
      'hpRegen more 2 perLevel',
    ],
    'slot' : 'weapon',
    'materials' : [
      'handle',
      'blood',
      'potion',
    ]
  },
  'hydra blood' : {
    'mods' : [
      'maxHp more -30',
      'maxHp gainedas 1 hpRegen',
      'poisDmg more 3 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [
      'heart',
      'blood',
      'spore',
      'imp head',
    ]
  },
  'faster attacks' : {
    'mods' : [ 'speed more -25', 'speed more -0.3 perLevel' ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'wing', 'blade' ],
  },
  'more physical damage' : {
    'mods' : [
      'physDmg more 20',
      'physDmg more 1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'metal', 'blade' ],
  },
  'longer cooldown' : {
    'mods' : [
      'cooldownTime added 100',
      'cooldownTime more 3 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'wing', 'razor' ],
  },
  'shorter cooldown' : {
    'mods' : [
      'cooldownTime more -1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'wing', 'spike' ],
  },
  'faster propogation' : {
    'mods' : [
      'aoeSpeed more 2 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'energy', 'energy', 'spike' ],
  },
  'telescoping handle' : {
    'mods' : [
      'range more 1 perLevel',
      'range more 50',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'eye', 'spike' ],
  },
  'shorter range' : {
    'mods' : [
      'range more -5 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'eye', 'razor' ],
  },
  'ab shocker belt' : {
    'mods' : [
      'vitality added 20 perLevel', 'strength more 1 perLevel',
      'lightResist more -20'
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'blood', 'muscle', 'razor' ],
  },
  'bloodfingers' : {
    'mods' : [
      'vitality added 10 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'blood', 'blade' ],
  },
  'stimpack' : {
    'mods' : [
      'hpRegen more 2 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'blood', 'spike', 'nightmare' ],
  },
  'tinfoil hat' : {
    'mods' : [
      'lightDmg gainedas 0.5 hpLeech',
      'manaRegen more 5 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'blood', 'spike', 'nightmare' ],
  },
  'manafingers' : {
    'mods' : [
      'maxMana added 10 perLevel',
      'spellDmg more 0.5 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'brain', 'blade' ],
  },
  'bloodbath' : {
    'mods' : [
      'maxHp more 3 perLevel',
      'vitality added 2 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'blood', 'spike' ],
  },
  'side arm' : {
    'mods' : [
      'cooldownTime added 250', 'speed more -50', 'speed more -0.3 perLevel'
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'wing', 'spike', 'razor' ],
  },
  'practiced' : {
    'mods' :
        [ 'physDmg more 15', 'speed more -20', 'speed more -0.1 perLevel' ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'metal', 'wing', 'spike' ],
  },
  'honed' : {
    'mods' : [
      'physDmg more 1 perLevel',
      'speed more -0.2 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'metal', 'spike' ],
  },
  'fatal blow' : {
    'mods' : [
      'meleeDmg more 1 perLevel',
      'meleeDmg more 100',
      'cooldownTime added 1000',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'muscle', 'razor', 'gargoyle' ],
  },
  'finishing move' : {
    'mods' : [
      'meleeDmg more 200',
      'cooldownTime added 500',
      'speed more 50',
      'speed more -1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'muscle', 'razor' ]
  },
  'long reach' : {
    'mods' : [
      'range more 40',
      'range more 0.5 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'eye', 'spike' ],
  },
  'frugal' : {
    'mods' : [
      'manaCost added -1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'brain', 'shield' ],
  },
  'stingy' : {
    'mods' : [ 'manaCost added -3 perLevel', 'speed more 50' ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'brain', 'razor' ],
  },
  'short sighted' : {
    'mods' : [ 'manaCost more -5 perLevel', 'range more -60' ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'brain', 'razor' ],
  },
  'divine assistance' : {
    'mods' : [
      'manaCost more -100', 'cooldownTime added 1000', 'speed more -2 perLevel'
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'wing', 'razor' ],
  },
  'micronaps' : {
    'mods' : [
      'cooldownTime more -20',
      'cooldownTime more -0.5 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'wing', 'blade' ],
  },
  'healing charm' : {
    'mods' : [
      'manaRegen added -5 perLevel',
      'hpRegen added 10 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'blood', 'potion', 'razor' ],
  },
  'blood pact' : {
    'mods' : [
      'manaRegen added 10 perLevel',
      'hpRegen added -5 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'brain', 'potion', 'razor' ],
  },
  'vengeful' : {
    'mods' : [
      'maxHp gainedas -2 hpRegen',
      'meleeDmg more 30',
      'meleeDmg more 1 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'muscle', 'potion', 'razor' ],
  },
  'painful phylactery' : {
    'mods' : [
      'maxHp gainedas -6 hpRegen',
      'maxHp gainedas 2 hpOnHit',
      'hpRegen more -2 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'blood', 'razor', 'lichen' ],
    'flavor' : 'Lich, Please!'
  },
  'deadly focus' : {
    'mods' : [
      'projCount converted 100 spellDmg',
      'spellDmg more -30',
      'spellDmg more 1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'brain', 'converter', 'razor' ],
  },
  'undeath' : {
    'mods' : [
      'speed more 50',
      'hpOnHit added 50',
      'spellDmg more 1 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'brain', 'potion', 'razor' ],
  },
  'soul channeling' : {
    'mods' : [
      'maxHp more -50',
      'spellDmg more 30',
      'spellDmg more 1 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'brain', 'potion', 'razor' ],
  },
  'arcane thirst' : {
    'mods' : [
      'maxHp more -25',
      'maxMana gainedas 5 hpOnHit',
      'maxMana more 1 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'brain', 'potion', 'razor' ],
  },
  'jet pack' : {
    'mods' : [
      'moveSpeed more 50',
      'moveSpeed more 4 perLevel',
      'dodge more -50',
      'armor more -50',
    ],
    'slot' : 'chest',
    'flavor' : 'Burning out his fuse up here alone',
    'materials' : [ 'heart', 'wing', 'razor' ],
  },
  'cold blooded' : {
    'mods' : [
      'coldDmg more 30',
      'fireDmg more -30',
      'coldResist more 20',
      'fireResist more -20',
      'coldResist more 2 perLevel',
      'coldDmg more 3 perLevel',
    ],
    'slot' : 'chest',
    'flavor' : 'Ice-water in his veins...',
    'materials' : [ 'heart', 'ice', 'spike', 'razor' ],
  },
  'hot blooded' : {
    'mods' : [
      'coldDmg more -30',
      'fireDmg more 30',
      'coldResist more -20',
      'fireResist more 20',
      'fireResist more 2 perLevel',
      'fireDmg more 3 perLevel',
    ],
    'slot' : 'chest',
    'flavor' : 'Magma in his veins...',
    'materials' : [ 'heart', 'ember', 'spike', 'razor' ],
  },
  'semi automatic' : {
    'mods' : [
      'cooldownTime more -20',
      'cooldownTime more -1 perLevel',
    ],
    'slot' : 'weapon',
    'flavor' : 'NOW I HAVE A MACHINE GUN HO-HO-HO',
    'materials' : [ 'handle', 'wing', 'spike' ],
  },
  'careful aim' : {
    'mods' : [
      'physDmg more 50',
      'cooldownTime added 3000',
      'physDmg more 2 perLevel',
      'accuracy more 100',
      'manaCost more 100',
    ],
    'slot' : 'skill',
    'flavor' : 'Ready... Aim... FIRE!',
    'materials' : [ 'energy', 'metal', 'razor' ],
  },
  'conductive suit' : {
    'mods' : [
      'lightDmg more 20',
      'lightResist more 5 perLevel',
      'lightDmg more 2 perLevel',
    ],
    'slot' : 'chest',
    'flavor' :
        'Fortunately the path of least resistance is no longer through your heart.',
    'materials' : [ 'heart', 'spark', 'spike', 'shield' ],
  },
  'mageheart' : {
    'mods' : [ 'spellDmg more 30', 'spellDmg more 3 perLevel' ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'brain', 'spike' ],
  },
  'antibiotics' : {
    'mods' : [
      'poisResist more 20',
      'poisResist more 5 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'spore', 'shield' ],
  },
  'forest spirit' : {
    'mods' : [ 'maxHp gainedas 0.5 hpRegen', 'maxHp more 1 perLevel' ],
    'slot' : 'head',
    'materials' : [ 'skull', 'blood', 'potion', 'spike' ],
  },
  'armor plating' : {
    'mods' : [ 'armor added 10 perLevel', 'armor more 2 perLevel' ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'metal', 'shield' ],
  },
  'mind expansion' : {
    'mods' : [ 'wisdom more 1 perLevel', 'wisdom gainedas 100 maxMana' ],
    'slot' : 'head',
    'materials' : [ 'skull', 'brain', 'brain', 'spike' ],

  },
  'concentration' : {
    'mods' : [
      'angle more -50',
      'speed more 35',
      'wisdom added 30',
      'wisdom added 3 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'brain', 'blade', 'gnome' ],
  },
  'unwavering' : {
    'mods' : [
      'dexterity converted 100 accuracy',
      'accuracy more 1000',
      'accuracy more 5 perLevel',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'eye', 'razor', 'elf ear' ],
  },
  'clarity' : {
    'mods' : [
      'wisdom converted 30 accuracy',
      'wisdom more 1 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'brain', 'converter', 'gnome' ],
  },
  'deathwish' : {
    'mods' : [
      'eleResistAll more -90',
      'eleResistAll more -2 perLevel',
      'spellDmg more 10 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'brain', 'razor', 'lichen' ],
  },
  'capacitor' : {
    'mods' : [
      'cooldownTime gainedas 100 lightDmg',
      'lightDmg more 1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'wing', 'converter', 'spark' ],
  },
  'ice breath' : {
    'mods' : [
      'wisdom converted 30 coldDmg',
      'wisdom more 1 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'brain', 'converter', 'ice' ],
  },
  'well grounded' : {
    'mods' : [
      'wisdom gainedas 30 lightDmg',
      'wisdom more 1 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'brain', 'converter', 'spark' ],
  },
  'roller skates' : {
    'mods' : [
      'moveSpeed more 2 perLevel',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'wing', 'spike' ],
  },
  'prismatic toe ring' : {
    'mods' : [ 'eleResistAll added 100', 'eleResistAll added 5 perLevel' ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'brain', 'shield' ],
  },
  'hobbit foot' : {
    'mods' : [ 'maxHp more 10', 'maxHp more 1 perLevel' ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'blood', 'spike' ],
  },
  'clown shoes' : {
    'mods' : [
      'vitality added 10 perLevel', 'maxHp more 1 perLevel',
      'moveSpeed more -30'
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'blood', 'razor' ],
  },
  'happy feet' : {
    'mods' : [
      'vitality added 10 perLevel',
      'maxHp more 20',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'blood', 'spike', 'blade' ],
  },
  'ice spikes' : {
    'mods' : [
      'coldResist more 10',
      'coldResist more 5 perLevel',
      'coldDmg added 3 perLevel',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'spike', 'ice', 'shield' ],
  },
  'extreme odor' : {
    'mods' : [
      'poisDmg more 30',
      'poisDmg more 3 perLevel',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'spore', 'spike' ]
  },
  'firewalker' : {
    'mods' : [
      'fireDmg more 30',
      'fireDmg more 3 perLevel',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'ember', 'spike' ]
  },
  'static socks' : {
    'mods' : [
      'lightDmg more 30',
      'lightDmg more 3 perLevel',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'spark', 'spike' ]
  },
  'knee pads' : {
    'mods' : [
      'armor more 10',
      'armor more 0.5 perLevel',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'metal', 'shield' ],
  },
  'hazmat boots' : {
    'mods' : [ 'eleResistAll more 5 perLevel' ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'brain', 'shield' ],
  },
  'rubber boots' : {
    'mods' : [ 'lightResist more 10', 'lightResist more 5 perLevel' ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'spark', 'shield' ],
  },
  'good circulation' : {
    'mods' : [ 'maxHp more 20', 'vitality added 5 perLevel' ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'blood', 'spike', 'blade' ],
  },
  'reduced radius' : {
    'mods' : [
      'aoeRadius more -50',
      'speed more -10',
      'speed more -0.1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'brain', 'wing', 'razor' ],
  },
  'increased radius' : {
    'mods' : [
      'aoeRadius more 50',
      'aoeRadius more 1 perLevel',
      'manaCost more 100',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'brain', 'wing', 'razor' ],
  },
  'potion holster' : {
    'mods' : [
      'vitality more 2 perLevel',
      'vitality more 20',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'blood', 'blade' ],
  },
  'liquefied brain' : {
    'mods' : [
      'maxMana more -100',
      'wisdom gainedas 60 vitality',
      'vitality more 2 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'slime', 'slime', 'slime', 'slime', 'slime' ],
  },
  'flame ritual' : {
    'mods' : [
      'fireDmg gainedas 0.75 hpLeech',
      'fireDmg more 1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'ember', 'needle' ],
  },
  'frost ritual' : {
    'mods' : [
      'coldDmg gainedas 0.75 hpLeech',
      'coldDmg more 1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'ice', 'needle' ],
  },
  'shock ritual' : {
    'mods' : [
      'lightDmg gainedas 0.75 hpLeech',
      'lightDmg more 1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'spark', 'needle' ],
  },
  'plague ritual' : {
    'mods' : [
      'poisDmg gainedas 0.75 hpLeech',
      'poisDmg more 1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'spore', 'needle' ],
  },
  'pyropotency' : {
    'mods' : [
      'fireDmg more 25',
      'fireDmg more 1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'ember', 'spike' ],
  },
  'frigopotency' : {
    'mods' : [
      'coldDmg more 25',
      'coldDmg more 1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'ice', 'spike' ],
  },
  'electropotency' : {
    'mods' : [
      'lightDmg more 25',
      'lightDmg more 1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'spark', 'spike' ],
  },
  'toxopotency' : {
    'mods' : [
      'poisDmg more 25',
      'poisDmg more 1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'spore', 'spike' ],
  },
  'sure footing' : {
    'mods' : [
      'accuracy more 3 perLevel',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'feather', 'spike' ],
  },
  'steady hands' : {
    'mods' : [
      'accuracy more 3 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'feather', 'spike' ],
  },
  'imaginary' : {
    'mods' : [
      'dodge more 3 perLevel',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'feather', 'spike', 'nightmare' ],
  },
  'planet buster' : {
    'mods' : [
      'projRadius more 200',
      'spellDmg more 100',
      'spellDmg more 5 perLevel',
      'cooldownTime added 3000',
      'manaCost more 100',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'brain', 'spike', 'wight snow' ],
  },
  'nanotube reinforcement' : {
    'mods' : [
      'armor more 5 perLevel',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'metal', 'shield' ],
  },
  'pinpoint precision' : {
    'mods' : [
      'accuracy more 1 perLevel',
      'rangeDmg more 20',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'feather', 'eye', 'elf ear' ],
  },
  'fletching' : {
    'mods' : [
      'rangeDmg more 3 perLevel',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'feather', 'eye', 'sigil' ],
  },
  'strafing' : {
    'mods' : [
      'projCount added 2', 'accuracy more -30', 'speed more -30',
      'rangeDmg more -30', 'rangeDmg more 3 perLevel'
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'feather', 'eye', 'spike' ],
  },
  'swift hands' : {
    'mods' : [ 'speed more -14.6', 'speed more -0.4 perLevel' ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'feather', 'eye', 'spike' ],
  },
  'war horse' : {
    'mods' : [
      'moveSpeed more 0.2 perLevel',
      'meleeDmg more 2 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'muscle', 'spike', 'sigil' ],
  },
  'swashbuckling' : {
    'mods' : [ 'strength more 1 perLevel', 'dexterity more 1 perLevel' ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'muscle', 'eye', 'spike' ],
  },
  'soft weapons' : {
    'mods' : [ 'dexterity more 2 perLevel' ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'eye', 'spike', 'sigil' ],
  },
  'demonic split' : {
    'mods' : [
      'speed more 666',
      'spellDmg more -66',
      'projCount added 6',
      'cooldownTime more -0.6 perLevel',

    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'brain', 'feather', 'sigil' ],
  },

  'minimum tolerances' : {
    'mods' : [
      'accuracy more 1.5 perLevel',
      'accuracy more 20',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'feather', 'spike' ],
  },
  'hazmat suit' : {
    'mods' : [
      'eleResistAll more 5 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'brain', 'shield' ],
  },
  'hazmat gloves' : {
    'mods' : [
      'eleResistAll more 5 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'brain', 'shield' ],
  },
  'hazmat mask' : {
    'mods' : [
      'eleResistAll more 5 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'brain', 'shield' ],
  },
  'face training' : {
    'mods' : [
      'strength more 5 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'muscle', 'spike' ],
  },
  'cosmic channeling' : {
    'mods' : [
      'spellDmg more 50',
      'rangeDmg more 50',
      'meleeDmg more 50',
      'cooldownTime added 2000',
      'aoeSpeed more -50',
      'aoeRadius more 5 perLevel',
      'aoeSpeed more 1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'muscle', 'eye', 'brain', 'razor' ],
  },
  'electricians gloves' : {
    'mods' : [
      'lightResist more 6 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'spark', 'shield' ],
  },
  'basket hilt' : {
    'mods' : [
      'armor more 5 perLevel',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'metal', 'shield' ],
  },
  'accurate' : {
    'mods' : [
      'accuracy more 50',
      'accuracy more 1 perLevel',
    ],
    'slot' : 'skill',
    'materials' : [ 'energy', 'feather', 'spike' ],
  },
  'balanced' : {
    'mods' : [
      'dexterity more 1.5 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'eye', 'spike', 'feather' ],
  },
  'grabby arm' : {
    'mods' : [
      'range more 0.5 perLevel',
      'range more 30',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'eye', 'spike' ],
  },
  'bushido' : {
    'mods' : [
      'meleeDmg more 0.5 perLevel',
      'armor more 0.5 perLevel',
      'dodge more 0.5 perLevel',
      'eleResistAll more 1 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'muscle', 'brain', 'eye', 'shield' ],
  },
  'jovial' : {
    'mods' : [
      'lineWidth added 60',
      'maxHp more 30',
      'maxHp more 0.75 perLevel',
      'moveSpeed more -30',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'blood', 'spike', 'razor' ],
  },
  'proto-slime' : {
    'mods' : [
      'manaCost more -100',
      'lineWidth added 500',
      'height more -50',
      'maxHp gainedas 20 hpLeech',
    ],
  },
  'proto-corpse' : {
    'mods' : [
      'moveSpeed more -100',
    ],
  },
  'impenetrable' : {
    'mods' : [
      'dodge more -50',
      'armor more 50',
      'armor more 1 perLevel',
    ],
    'slot' : 'chest',
    'materials' : [ 'heart', 'metal', 'shield', 'razor' ],
  },
  'meditation' : {
    'mods' : [
      'eleResistAll more 10',
      'eleResistAll more 1 perLevel',
      'maxHp gainedas 0.5 hpRegen',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'brain', 'blood', 'potion' ],
  },
  'more balls' : {
    'mods' : [
      'projCount added 2',
      'projRadius more 100',
      'projRadius more 2.5 perLevel',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'eye', 'blade', 'business card' ],
  },
  'ninja stance' : {
    'mods' : [
      'dexterity gainedas 0.2 moveSpeed',
      'dexterity more 1 perLevel',
      'range more -35',
      'cooldownTime added 25',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'eye', 'wing', 'razor' ],
  },
  'smart footing' : {
    'mods' : [
      'wisdom gainedas 0.1 moveSpeed',
      'wisdom more 1 perLevel',
      'speed more 25',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'brain', 'wing', 'razor' ],
  },
  'momentum' : {
    'mods' : [
      'moveSpeed gainedas 100 projSpeed',
      'projSpeed more 1 perLevel',
    ],
    'slot' : 'legs',
    'materials' : [ 'toe', 'eye', 'wing', 'spike' ],
  },
  'overdraw' : {
    'mods' : [
      'projSpeed more 1 perLevel',
    ],
    'slot' : 'weapon',
    'materials' : [ 'handle', 'wing', 'spike' ],
  },
  'eagle eye' : {
    'mods' : [
      'accuracy more 30',
      'accuracy more 1 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'feather', 'spike' ],
  },
  'quick reaction' : {
    'mods' : [
      'dodge more 30',
      'dodge more 1 perLevel',
    ],
    'slot' : 'head',
    'materials' : [ 'skull', 'feather', 'shield' ],
  },
  'titan\'s grip' : {
    'mods' : [
      'strength added 10 perLevel',
    ],
    'slot' : 'hands',
    'materials' : [ 'finger', 'muscle', 'blade', 'slime' ],
    'designCredit' : 'Virakai',
  },
};
