export var armor = {
  ////////////////////
  ///// HEAD /////////
  ////////////////////
  'balsa helmet' : {
    'mods' : [ 'armor added 5', 'armor more 1 perLevel' ],
    'slot' : 'head',
  },
  'collander' : {
    'mods' : [ 'armor added 7', 'armor more 1.2 perLevel' ],
    'slot' : 'head',
  },
  'conquistador helm' : {
    'mods' : [ 'armor added 9', 'armor more 1.4 perLevel' ],
    'slot' : 'head',
  },
  'crusader helm' : {
    'mods' : [ 'armor more 1.6 perLevel' ],
    'slot' : 'head',
  },
  'gladiator helm' : {
    'mods' : [ 'armor added 22', 'armor more 1.3 perLevel' ],
    'slot' : 'head',
  },
  'apollo helmet' : {
    'mods' : [ 'armor added 30', 'armor more 1.5 perLevel' ],
    'slot' : 'head',
  },
  'plague doctor' : {
    'mods' : [
      'poisResist more 10',
      'poisDmg gainedas 0.5 hpLeech',
      'poisResist more 2 perLevel',
      'poisDmg more 0.5 perLevel',
    ],
    'slot' : 'head',
  },
  'visor' : {
    'mods' : [
      'armor added 9', 'armor more 1.2 perLevel', 'rangeDmg more 0.8 perLevel'
    ],
    'slot' : 'head',
  },
  'mage hat' : {
    'mods' : [ 'manaRegen added 10', 'eleResistAll more 2 perLevel' ],
    'slot' : 'head',
  },
  'lichgaze' : {
    'mods' : [
      'maxMana more 1 perLevel', 'wisdom added 1 perLevel',
      'eleResistAll more 1 perLevel'
    ],
    'slot' : 'head',
  },
  'dodgers cap' : {
    'mods' : [
      'dodge added 30',
      'dodge more 1 perLevel',
    ],
    'slot' : 'head',
  },
  'kabuto' : {
    'mods' : [ 'armor more 0.5 perLevel', 'eleResistAll more 1 perLevel' ],
    'slot' : 'head',
  },
  'gooey gibus' : {
    'mods' : [
      'vitality more 0.5 perLevel',
      'eleResistAll more 1 perLevel',
    ],
    'slot' : 'head',
  },
  'champion helm' : {
    'mods' : [
      'armor more 0.8 perLevel',
      'eleResistAll more 0.8 perLevel',
      'strength more 0.3 perLevel',
      'dexterity more 0.3 perLevel',
    ],
    'slot' : 'head',
  },
  'zealot hood' : {
    'mods' : [
      'eleResistAll more -40', 'cooldownTime more -40',
      'spellDmg more 1.5 perLevel'
    ],
    'slot' : 'head',
  },
  'halo' : {
    'mods' : [
      'aoeSpeed more 0.8 perLevel',
      'aoeRadius more 0.8 perLevel',
      'dodge more 0.8 perLevel',
    ],
    'slot' : 'head',
  },
  ////////////////////
  ///// CHEST ////////
  ////////////////////
  't-shirt' : {
    'mods' : [ 'armor added 5', 'armor more 1 perLevel' ],
    'slot' : 'chest',
  },
  'leather armor' : {
    'mods' : [ 'armor added 8', 'armor more 1.2 perLevel' ],
    'slot' : 'chest',
  },
  'goblin leather' : {
    'mods' : [
      'armor added 5', 'armor more 1.3 perLevel', 'fireResist more 2 perLevel'
    ],
    'slot' : 'chest',
  },
  'leatherscale armor' : {
    'mods' : [
      'dodge more 1 perLevel',
      'armor more 1 perLevel',
      'eleResistAll more 1 perLevel',
    ],
    'slot' : 'chest',
  },
  'leatherplate armor' : {
    'mods' : [
      'armor added 20', 'armor more 1.5 perLevel',
      'eleResistAll more 0.6 perLevel'
    ],
    'slot' : 'chest',
  },
  'hammered chestplate' : {
    'mods' : [ 'armor more 50', 'armor more 1 perLevel' ],
    'slot' : 'chest',
  },
  'iron chestplate' : {
    'mods' : [ 'armor added 40', 'armor more 1.5 perLevel' ],
    'slot' : 'chest',
  },
  'dragonscale' : {
    'mods' : [
      'fireDmg gainedas 0.5 hpLeech',
      'fireDmg more 1 perLevel',
      'eleResistAll more 1 perLevel',
      'dodge more 0.5 perLevel',
    ],
    'slot' : 'chest',
  },
  'muscle plate' : {
    'mods' : [
      'armor added 100',
      'armor more 1.6 perLevel',
    ],
    'slot' : 'chest',
  },
  'chainmail' : {
    'mods' : [
      'armor more 1 perLevel',
      'dexterity more 1 perLevel',
    ],
    'slot' : 'chest',
  },
  'elegant plate' : {
    'mods' : [
      'armor added 200',
      'dodge more -50',
      'armor more 1.9 perLevel',
      'eleResistAll more 2 perLevel',
    ],
    'slot' : 'chest',
  },
  'paladin armor' : {
    'mods' : [
      'moveSpeed more -50',
      'maxHp gainedas 2.2 hpRegen',
      'armor more 2.2 perLevel',
      'eleResistAll more 2.2 perLevel',
    ],
    'slot' : 'chest',
  },
  'raider armor' : {
    'mods' : [
      'physDmg more 2 perLevel',
      'armor more -20',
    ],
    'slot' : 'chest',
  },
  'shadow armor' : {
    'mods' : [
      'dodge added 50',
      'dodge more 1.5 perLevel',
      'eleResistAll more 0.5 perLevel',
    ],
    'slot' : 'chest',
  },
  'scout leather' : {
    'mods' : [
      'dodge added 20',
      'dodge more 1 perLevel',
      'eleResistAll more 1 perLevel',
    ],
    'slot' : 'chest',
  },
  'studded leather' : {
    'mods' : [
      'dodge more 1.4 perLevel',
      'armor more 0.5 perLevel',
      'eleResistAll more 1 perLevel',
    ],
    'slot' : 'chest',
  },
  'velvet tunic' : {
    'mods' : [
      'manaRegen added 5',
      'eleResistAll more 0.5 perLevel',
      'eleResistAll added 5 perLevel',
    ],
    'slot' : 'chest',
  },
  'war robe' : {
    'mods' : [
      'manaRegen added 10',
      'armor more 1.2 perLevel',
      'eleResistAll more 2 perLevel',
    ],
    'slot' : 'chest',
  },
  'winged leather' : {
    'mods' : [
      'armor added 100', 'armor more 1.2 perLevel',
      'eleResistAll more 1 perLevel'
    ],
    'slot' : 'chest',
  },
  'cultist robe' : {
    'mods' : [
      'spellDmg more 30', 'dodge more 0.5 perLevel',
      'eleResistAll more 1 perLevel'
    ],
    'slot' : 'chest',
  },
  'embroidered silks' : {
    'mods' : [
      'manaRegen added 20',
      'spellDmg more 1 perLevel',
      'eleResistAll more 1 perLevel',
      'eleResistAll added 1 perLevel',
    ],
    'slot' : 'chest',
  },
  'champion mail' : {
    'mods' : [
      'armor more 1.2 perLevel',
      'eleResistAll more 1.2 perLevel',
      'strength more 0.5 perLevel',
      'dexterity more 0.5 perLevel',
    ],
    'slot' : 'chest',
  },
  'batsuit' : {
    'mods' : [
      'speed more -20',
      'physDmg more 1 perLevel',
    ],
    'slot' : 'chest',
  },
  'gooey gaberdine' : {
    'mods' : [
      'vitality more 1 perLevel',
      'eleResistAll more 1 perLevel',
    ],
    'slot' : 'chest',
  },
  ////////////////////
  ///// LEGS /////////
  ////////////////////
  'jeans' : {
    'mods' : [ 'armor added 5', 'armor more 1 perLevel' ],
    'slot' : 'legs',
  },
  'leather boots' : {
    'mods' : [ 'armor added 10', 'armor more 1.2 perLevel' ],
    'slot' : 'legs',
  },
  'elf boots' : {
    'mods' : [ 'dodge added 5', 'dodge more 1 perLevel' ],
    'slot' : 'legs',
  },
  'mage boots' : {
    'mods' : [
      'manaRegen added 5',
      'manaRegen more 2 perLevel',
      'eleResistAll more 1 perLevel',
    ],
    'slot' : 'legs',
  },
  'arcane boots' : {
    'mods' : [
      'spellDmg more 20', 'manaRegen more 2 perLevel',
      'eleResistAll more 1 perLevel'
    ],
    'slot' : 'legs',
  },
  'buckaneer boots' : {
    'mods' : [
      'armor added 25', 'armor more 1 perLevel',
      'eleResistAll more 0.6 perLevel'
    ],
    'slot' : 'legs',
  },
  'foot plate' : {
    'mods' : [
      'armor added 40',
      'armor more 1.5 perLevel',
      'dodge more -30',
      'eleResistAll more 1 perLevel',
    ],
    'slot' : 'legs',
  },
  'ninja tabi' : {
    'mods' : [ 'dodge added 100', 'dodge more 1.5 perLevel' ],
    'slot' : 'legs',
  },
  'suess boots' : {
    'mods' : [ 'armor added 500', 'armor added 20 perLevel' ],
    'slot' : 'legs',
  },
  'rear claws' : {
    'mods' : [
      'cooldownTime more -30', 'strength added 3 perLevel',
      'lightResist more 3 perLevel'
    ],
    'slot' : 'legs',
  },
  'shiny greaves' : {
    'mods' : [
      'vitality more 0.3 perLevel',
      'eleResistAll more 0.8 perLevel',
      'armor more 0.8 perLevel',
    ],
    'slot' : 'legs',
  },
  'cavalry boots' : {
    'mods' : [
      'strength more 0.8 perLevel',
      'armor more 0.8 perLevel',
    ],
    'slot' : 'legs',
  },
  'magesteel greaves' : {
    'mods' : [
      'cooldownTime more -30', 'armor more 0.8 perLevel',
      'eleResistAll more 0.8 perLevel'
    ],
    'slot' : 'legs',
  },
  'champion boots' : {
    'mods' : [
      'armor more 0.8 perLevel',
      'eleResistAll more 0.8 perLevel',
      'strength more 0.3 perLevel',
      'dexterity more 0.3 perLevel',
    ],
    'slot' : 'legs',
  },
  'winged sandals' : {
    'mods' : [ 'dexterity more 1 perLevel', 'moveSpeed more 1 perLevel' ],
    'slot' : 'legs',
  },
  ////////////////////
  ///// GLOVES ///////
  ////////////////////
  'latex gloves' : {
    'mods' : [ 'armor added 5', 'armor more 1 perLevel' ],
    'slot' : 'hands',
  },
  'gardening gloves' : {
    'mods' : [ 'armor added 7', 'armor more 1.2 perLevel' ],
    'slot' : 'hands',
  },
  'leather gloves' : {
    'mods' : [ 'dodge added 10', 'dodge more 1.2 perLevel' ],
    'slot' : 'hands',
  },
  'silk feather gloves' : {
    'mods' : [
      'speed more -25', 'dexterity more 1 perLevel',
      'eleResistAll more 1 perLevel'
    ],
    'slot' : 'hands',
  },
  'velvet gloves' : {
    'mods' : [
      'manaRegen added 2',
      'manaRegen more 2 perLevel',
      'spellDmg more 0.5 perLevel',
      'eleResistAll more 1 perLevel',
    ],
    'slot' : 'hands',
  },
  'front claws' : {
    'mods' : [
      'speed more -30', 'dexterity added 3 perLevel', 'lightDmg more 1 perLevel'
    ],
    'slot' : 'hands',
  },
  'handmail' : {
    'mods' : [ 'armor added 15', 'armor more 1.3 perLevel' ],
    'slot' : 'hands',
  },
  'fancy gauntlets' : {
    'mods' : [ 'armor added 25', 'armor more 1.5 perLevel' ],
    'slot' : 'hands',
  },
  'polished gauntlets' : {
    'mods' : [ 'armor added 30', 'dodge more -30', 'armor more 1.8 perLevel' ],
    'slot' : 'hands',
  },
  'goldenscale gauntlets' : {
    'mods' : [
      'dodge added 50', 'dodge more 1.1 perLevel',
      'eleResistAll more 0.6 perLevel'
    ],
    'slot' : 'hands',
  },
  'mage gloves' : {
    'mods' : [
      'manaRegen added 5', 'wisdom added 1 perLevel',
      'eleResistAll more 1 perLevel'
    ],
    'slot' : 'hands',
  },
  'gooey gauntlets' : {
    'mods' : [
      'vitality more 0.5 perLevel',
      'eleResistAll more 1 perLevel',
    ],
    'slot' : 'hands',
  },
  'magesteel gauntlets' : {
    'mods' : [
      'spellDmg more 30', 'armor more 0.8 perLevel',
      'eleResistAll more 0.8 perLevel'
    ],
    'slot' : 'hands',
  },
  'champion gloves' : {
    'mods' : [
      'armor more 0.8 perLevel',
      'eleResistAll more 0.8 perLevel',
      'strength more 0.3 perLevel',
      'dexterity more 0.3 perLevel',
    ],
    'slot' : 'hands',
  },
};
