import * as color from './colors';

export var BASE_MELEE_RANGE = 300;
export var BASE_RANGE_RANGE = 4000;
export var BASE_SPELL_RANGE = 4000;

export var skill = {
  'basic' : {},
  'basic melee' : {
    'prototype' : [ 'basic' ],
    'skillType' : 'melee',
    'types' : [ 'melee' ],
    'specs' : [ {
      type : 'melee',
      color : color.PHYS_COLOR,
      quals : [],
      onHit : [],
      onKill : [],
      onRemove : []
    } ],
    'baseMods' : [
      'speed added 500',
      'range added ' + BASE_MELEE_RANGE,
      'physDmg added 2 perLevel',
    ]
  },
  'basic range' : {
    'prototype' : [ 'basic' ],
    'skillType' : 'range',
    'types' : [ 'proj' ],
    'specs' : [ {
      type : 'proj',
      color : color.PHYS_COLOR,
      quals : [],
      onHit : [],
      onKill : [],
      onRemove : []
    } ],
    'baseMods' : [
      'speed added 500',
      'range added ' + BASE_RANGE_RANGE,
      'physDmg added 2 perLevel',
    ]
  },
  'basic spell' : {
    'prototype' : [ 'basic' ],
    'skillType' : 'spell',
    'types' : [ 'proj' ],
    'specs' : [ {
      type : 'proj',
      color : color.PHYS_COLOR,
      quals : [],
      onHit : [],
      onKill : [],
      onRemove : []
    } ],
    'baseMods' : [
      'speed added 500',
      'range added ' + BASE_SPELL_RANGE,
      'physDmg added 2 perLevel',
    ]
  },
  'pacifism' : {
    'prototype' : [ 'basic melee' ],
    'types' : [ 'melee' ],
    'specs' : [ {type : 'melee', quals : [ 'dmg more -100' ], color : '#fff'} ],
    'baseMods' : [
      'speed added 1000',
      'projRadius more -100',
      'physDmg more -100',
      'range added ' + BASE_MELEE_RANGE * 3,
    ],
    'flavor' : 'Why isn\'t this working??',
  },
  'super smash' : {
    'prototype' : [ 'basic melee' ],
    'baseMods' : [
      'manaCost added 3', 'speed added 800', 'range added ' + BASE_MELEE_RANGE,
      'physDmg more 10', 'physDmg more 1 perLevel', 'physDmg added 2 perLevel'
    ]
  },
  'masterful strike' : {
    'prototype' : [ 'basic melee' ],
    'baseMods' : [
      'manaCost added 15', 'speed added 200', 'cooldownTime added 1000',
      'range added ' + BASE_MELEE_RANGE, 'physDmg more 20',
      'physDmg more 1 perLevel', 'physDmg added 5 perLevel'
    ]
  },
  'quick hit' : {
    'prototype' : [ 'basic melee' ],
    'baseMods' : [
      'manaCost added 3',
      'speed added 250',
      'physDmg more 2 perLevel',
      'range added ' + BASE_MELEE_RANGE,
    ]
  },
  'charge' : {
    'prototype' : [ 'basic melee' ],
    'baseMods' : [
      'manaCost added 15',
      'speed added 800',
      'cooldownTime added 800',
      'physDmg more 3 perLevel',
      'range added ' + BASE_MELEE_RANGE * 3,
    ]
  },
  'sweep' : {
    'prototype' : [ 'basic melee' ],
    'specs' : [ {type : 'cone', quals : [ 'projRadius more 10000' ]} ],
    'baseMods' : [
      'manaCost added 8',
      'speed added 400',
      'angle added 165',
      'accuracy more 100',
      'aoeSpeed more 200',
      'aoeRadius more -50',
      'physDmg more 2 perLevel',
      'range added ' + BASE_MELEE_RANGE,
    ]
  },
  'throw weapon' : {
    'prototype' : [ 'basic melee' ],
    'baseMods' : [
      'manaCost added 25',
      'speed added 250',
      'cooldownTime added 2000',
      'physDmg more 2.5 perLevel',
      'range added ' + BASE_RANGE_RANGE,
    ]
  },
  'fire slash' : {
    'prototype' : [ 'basic melee' ],
    'types' : [ 'melee', 'fire' ],
    'specs' : [ {type : 'melee', color : color.FIRE_COLOR} ],
    'baseMods' : [
      'manaCost added 3', 'speed added 300', 'range added ' + BASE_MELEE_RANGE,
      'fireDmg more 2 perLevel', 'fireDmg added 1 perLevel',
      'physDmg added 1 perLevel', 'physDmg converted 60 fireDmg'
    ]
  },
  'flaming debris' : {
    'prototype' : [ 'basic melee' ],
    'types' : [ 'melee', 'fire' ],
    'specs' : [ {
      type : 'melee',
      quals : [],
      onHit : [ {
        type : 'proj',
        color : color.FIRE_COLOR,
        quals : [ 'projCount added 2', 'dmg more -20' ],
        onKill : [],
        onRemove : []
      } ],
      onKill : [],
      onRemove : []
    } ],
    'baseMods' : [
      'manaCost added 3',
      'speed added 350',
      'range added ' + BASE_MELEE_RANGE,
      'projRange added ' + BASE_SPELL_RANGE,
      'fireDmg more 1.8 perLevel',
      'fireDmg added 1 perLevel',
      'physDmg added 1 perLevel',
      'physDmg converted 60 fireDmg',
      'projSpeed more -80',
    ]
  },
  'exploding strike' : {
    'prototype' : [ 'basic melee' ],
    'types' : [ 'melee', 'fire' ],
    'specs' : [ {
      type : 'melee',
      quals : [],
      color : color.FIRE_COLOR,
      onHit : [],
      onKill : [ {
        type : 'circle',
        color : color.FIRE_COLOR,
        quals : [ 'dmg more 100' ],
        onHit : [
          {type : 'cone', color : color.FIRE_COLOR, quals : [ 'dmg more -50' ]}
        ],
        onKill : [],
        onRemove : []
      } ],
      onRemove : []
    } ],
    'baseMods' : [
      'manaCost added 7',
      'speed added 300',
      'range added ' + BASE_MELEE_RANGE,
      'fireDmg added 1 perLevel',
      'physDmg added 1 perLevel',
      'physDmg more 1.7 perLevel',
      'physDmg converted 60 fireDmg',
      'aoeRadius more -40',
      'angle added 60',
      'cooldownTime gainedas 100 physDmg',
    ],
    'flavor' :
        'Creates fiery AoE explosions on kill dealing double damage and triggering fiery cones.',
  },
  'chain lightning' : {
    'prototype' : [ 'basic melee' ],
    'types' : [ 'melee', 'lightning' ],
    'specs' : [ {
      type : 'melee',
      quals : [],
      color : color.LIGHT_COLOR,
      onHit : [],
      onKill : [ {
        type : 'circle',
        color : color.LIGHT_COLOR,
        quals : [ 'dmg more 100' ],
        onHit : [ {
          type : 'circle',
          color : color.LIGHT_COLOR,
          quals : [ 'dmg more -50' ],
          onHit : [],
          onKill : [],
          onRemove : []
        } ],
        onKill : [],
        onRemove : []
      } ],
      onRemove : []
    } ],
    'baseMods' : [
      'manaCost added 4',
      'speed added 300',
      'range added ' + BASE_MELEE_RANGE,
      'lightDmg added 1 perLevel',
      'physDmg added 1 perLevel',
      'physDmg more 1.7 perLevel',
      'physDmg converted 60 lightDmg',
      'aoeRadius more -40',
      'cooldownTime gainedas 100 physDmg',
    ],
    'flavor' : 'Creates chained electical AoE explosions on kill',
  },
  'splashing hit' : {
    'prototype' : [ 'basic melee' ],
    'types' : [ 'melee' ],
    'specs' : [ {
      type : 'melee',
      quals : [],
      color : color.PHYS_COLOR,
      onHit : [ {
        type : 'circle',
        color : color.PHYS_COLOR,
        quals : [ 'dmg more -20' ],
        onHit : [],
        onKill : [],
        onRemove : []
      } ],
      onKill : [],
      onRemove : []
    } ],
    'baseMods' : [
      'manaCost added 10',
      'speed added 500',
      'range added ' + BASE_MELEE_RANGE,
      'physDmg more 2 perLevel',
      'physDmg added 1 perLevel',
      'aoeRadius more -60',
    ],
    'flavor' : 'Creates small AoE explosions on hit',
  },
  'blast arrow' : {
    'prototype' : [ 'basic range' ],
    'types' : [ 'range' ],
    'specs' : [ {
      type : 'proj',
      quals : [],
      color : color.PHYS_COLOR,
      onHit : [ {
        type : 'circle',
        color : color.PHYS_COLOR,
        quals : [ 'dmg more -20' ],
        onHit : [],
        onKill : [],
        onRemove : []
      } ],
      onKill : [],
      onRemove : []
    } ],
    'baseMods' : [
      'manaCost added 12',
      'speed added 500',
      'range added ' + BASE_RANGE_RANGE,
      'physDmg more 1.3 perLevel',
      'physDmg added 1 perLevel',
      'physDmg more -40',
      'aoeRadius more -60',
    ],
    'flavor' : 'Creates small AoE explosions on hit',
  },
  'piercing shot' : {
    'prototype' : [ 'basic range' ],
    'types' : [ 'range' ],
    'specs' : [ {
      type : 'proj',
      quals : [],
      color : color.PHYS_COLOR,
      onHit : [ {
        type : 'proj',
        color : color.PHYS_COLOR,
        quals : [ 'projCount added -100' ],
        onHit : [ {
          type : 'proj',
          color : color.PHYS_COLOR,
          quals : [ 'projCount added -100' ]
        } ],
        onKill : [],
        onRemove : []
      } ],
      onKill : [],
      onRemove : []
    } ],
    'baseMods' : [
      'manaCost added 12',
      'speed added 600',
      'range added ' + BASE_RANGE_RANGE,
      'physDmg more 1.3 perLevel',
      'physDmg added 1 perLevel',
    ],
    'flavor' : 'Pierces up to two enemies'
  },
  'explonential shot' : {
    'prototype' : [ 'basic range' ],
    'types' : [ 'range' ],
    'specs' : [ {
      type : 'proj',
      quals : [],
      color : color.PHYS_COLOR,
      onHit : [ {
        type : 'proj',
        color : color.PHYS_COLOR,
        quals : [ 'projCount added 8', 'dmg more -50' ],
        onHit : [ {
          type : 'proj',
          quals : [ 'projCount added 8', 'dmg more -90' ],
          color : color.PHYS_COLOR,
          onHit : [ {
            type : 'proj',
            color : color.PHYS_COLOR,
            quals : [ 'projCount added 8', 'dmg more -95' ]
          } ]
        } ],
        onKill : [],
        onRemove : []
      } ],
      onKill : [],
      onRemove : []
    } ],
    'baseMods' : [
      'manaCost added 12', 'speed added 1000', 'cooldownTime added 8000',
      'projCount more -100', 'range added ' + BASE_RANGE_RANGE,
      'physDmg more 1.3 perLevel', 'physDmg added 1 perLevel',
      'physDmg more -80', 'angle added 20'
    ],
    'flavor' : 'Spawns additional projectiles on hit'
  },
  'shiverstorm' : {
    'prototype' : [ 'basic spell' ],
    'types' : [ 'spell' ],
    'specs' : [ {
      type : 'circle',
      color : color.COLD_COLOR,
      onHit : [
        {type : 'circle', color : color.COLD_COLOR, quals : [ 'dmg more -50' ]}
      ]
    } ],
    'baseMods' : [
      'manaCost added 30', 'speed added 100', 'cooldownTime added 5000',
      'range added ' + BASE_SPELL_RANGE / 2, 'coldDmg added 3 perLevel',
      'coldDmg more 2 perLevel'
    ],
  },
  'plague field' : {
    'prototype' : [ 'basic spell' ],
    'types' : [ 'spell' ],
    'specs' : [ {
      type : 'circle',
      color : color.POIS_COLOR,
      onHit : [
        {type : 'circle', color : color.POIS_COLOR, quals : [ 'dmg more -50' ]}
      ]
    } ],
    'baseMods' : [
      'manaCost added 30', 'speed added 100', 'cooldownTime added 5000',
      'range added ' + BASE_SPELL_RANGE / 2, 'poisDmg added 3 perLevel',
      'poisDmg more 2 perLevel'
    ],
  },
  'thunderstorm' : {
    'prototype' : [ 'basic spell' ],
    'types' : [ 'spell' ],
    'specs' : [ {
      type : 'circle',
      color : color.LIGHT_COLOR,
      onHit : [
        {type : 'circle', color : color.LIGHT_COLOR, quals : [ 'dmg more -50' ]}
      ]
    } ],
    'baseMods' : [
      'manaCost added 30', 'speed added 100', 'cooldownTime added 5000',
      'range added ' + BASE_SPELL_RANGE / 2, 'lightDmg added 3 perLevel',
      'lightDmg more 2 perLevel'
    ],
  },
  'blazing inferno' : {
    'prototype' : [ 'basic spell' ],
    'types' : [ 'spell' ],
    'specs' : [ {
      type : 'circle',
      color : color.FIRE_COLOR,
      onHit : [
        {type : 'circle', color : color.FIRE_COLOR, quals : [ 'dmg more -50' ]}
      ]
    } ],
    'baseMods' : [
      'manaCost added 30', 'speed added 100', 'cooldownTime added 5000',
      'range added ' + BASE_SPELL_RANGE / 2, 'fireDmg added 3 perLevel',
      'fireDmg more 2 perLevel'
    ],
  },
  'sacrifice' : {
    'prototype' : [ 'basic spell' ],
    'types' : [ 'spell' ],
    'specs' : [ {
      type : 'circle',
      color : '#400',
      onHit : [ {type : 'proj', color : '#900'} ]
    } ],
    'baseMods' : [
      'manaCost added 0',
      'cooldownTime added 30000',
      'speed added 100',
      'accuracy more 1000',
      'aoeSpeed more -80',
      'range added ' + BASE_SPELL_RANGE,
      'physDmg added 100 perLevel',
      'physDmg more 3 perLevel',
      'physDmg gainedas -200 hpOnHit',
    ],
  },
  'ground smash' : {
    'prototype' : [ 'basic melee' ],
    'types' : [ 'melee', 'fire' ],
    'specs' : [ {
      type : 'cone',
      color : color.FIRE_COLOR,
      quals : [],
      onHit : [ {type : 'proj', color : color.FIRE_COLOR} ],
      onKill : [],
      onRemove : []
    } ],

    'baseMods' : [
      'manaCost added 3',
      'speed added 400',
      'range added ' + BASE_MELEE_RANGE,
      'fireDmg more 1.5 perLevel',
      'fireDmg added 1 perLevel',
      'physDmg added 1 perLevel',
      'physDmg converted 60 fireDmg',
      'angle more 200',
      'projRange more 500',
    ]
  },
  'ice slash' : {
    'prototype' : [ 'basic melee' ],
    'types' : [ 'melee', 'cold' ],
    'specs' : [ {type : 'melee', color : color.COLD_COLOR} ],
    'baseMods' : [
      'manaCost added 5', 'speed added 300', 'range added ' + BASE_MELEE_RANGE,
      'coldDmg more 3 perLevel', 'coldDmg added 1 perLevel',
      'physDmg added 1 perLevel', 'physDmg converted 60 coldDmg'
    ]
  },
  'lightning slash' : {
    'prototype' : [ 'basic melee' ],
    'types' : [ 'melee', 'lightning' ],
    'specs' : [ {type : 'melee', color : color.LIGHT_COLOR} ],
    'baseMods' : [
      'manaCost added 5', 'speed added 250', 'range added ' + BASE_MELEE_RANGE,
      'lightDmg more 2 perLevel', 'lightDmg added 1 perLevel',
      'physDmg added 1 perLevel', 'physDmg converted 60 lightDmg'
    ]
  },
  'poison slash' : {
    'prototype' : [ 'basic melee' ],
    'types' : [ 'melee', 'poison' ],
    'specs' : [ {type : 'melee', color : color.POIS_COLOR} ],
    'baseMods' : [
      'manaCost added 5', 'speed added 400', 'range added ' + BASE_MELEE_RANGE,
      'poisDmg more 2 perLevel', 'poisDmg added 1 perLevel',
      'physDmg added 1 perLevel', 'physDmg converted 60 poisDmg'
    ]
  },
  'speed shot' : {
    'prototype' : [ 'basic range' ],
    'skillType' : 'range',
    'types' : [ 'proj' ],
    'baseMods' : [
      'manaCost added 3',
      'physDmg more -35',
      'physDmg added 1 perLevel',
      'physDmg more 1.4 perLevel',
      'speed added 250',
      'range added ' + BASE_RANGE_RANGE,
    ]
  },
  'fire arrow' : {
    'prototype' : [ 'basic range' ],
    'skillType' : 'range',
    'types' : [ 'proj', 'fire' ],
    'specs' : [ {type : 'proj', color : color.FIRE_COLOR} ],
    'baseMods' : [
      'manaCost added 6', 'speed added 300', 'range added ' + BASE_RANGE_RANGE,
      'physDmg added 3 perLevel', 'fireDmg more 1.5 perLevel',
      'physDmg converted 50 fireDmg'
    ]
  },
  'cold arrow' : {
    'prototype' : [ 'basic range' ],
    'skillType' : 'range',
    'types' : [ 'proj', 'cold' ],
    'specs' : [ {type : 'proj', color : color.COLD_COLOR} ],
    'baseMods' : [
      'manaCost added 6', 'speed added 250', 'range added ' + BASE_RANGE_RANGE,
      'physDmg added 1.2 perLevel', 'coldDmg more 3 perLevel',
      'physDmg converted 50 coldDmg'
    ]
  },
  'lightning arrow' : {
    'prototype' : [ 'basic range' ],
    'skillType' : 'range',
    'types' : [ 'proj', 'lightning' ],
    'specs' : [ {type : 'proj', color : color.LIGHT_COLOR} ],
    'baseMods' : [
      'manaCost added 6', 'speed added 200', 'range added ' + BASE_RANGE_RANGE,
      'physDmg added 1 perLevel', 'lightDmg more 3 perLevel',
      'physDmg converted 50 lightDmg'
    ]
  },
  'poison arrow' : {
    'prototype' : [ 'basic range' ],
    'skillType' : 'range',
    'types' : [ 'proj', 'poison' ],
    'specs' : [ {type : 'proj', color : color.POIS_COLOR} ],
    'baseMods' : [
      'manaCost added 6', 'speed added 300', 'range added ' + BASE_RANGE_RANGE,
      'physDmg added 1 perLevel', 'poisDmg more 3 perLevel',
      'physDmg converted 50 poisDmg'
    ]
  },
  'headshot' : {
    'prototype' : [ 'basic' ],
    'skillType' : 'range',
    'types' : [ 'proj' ],
    'specs' : [ {
      type : 'proj',
      color : '#FFF',
      quals : [],
      onHit : [],
      onKill : [],
      onRemove : []
    } ],
    'baseMods' : [
      'manaCost added 13',
      'speed added 500',
      'range added ' + BASE_RANGE_RANGE,
      'physDmg more 5 perLevel',
      'projSpeed more 200',
      'cooldownTime added 3000',
    ]
  },
  'incinerate' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'proj', 'fire', 'spell' ],
    'specs' : [ {type : 'cone', color : color.FIRE_COLOR} ],
    'baseMods' : [
      'manaCost added 7',
      'speed added 300',
      'range added ' + BASE_SPELL_RANGE / 4,
      'fireDmg added 2',
      'fireDmg added 1 perLevel',
      'fireDmg more 0.7 perLevel',
      'physDmg converted 100 fireDmg',
      'angle added 30',
    ]
  },
  'poison spray' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'cone', 'pois', 'spell' ],
    'specs' : [ {type : 'cone', color : color.POIS_COLOR} ],
    'baseMods' : [
      'manaCost added 9',
      'speed added 350',
      'range added ' + BASE_RANGE_RANGE * 0.2,
      'poisDmg added 4',
      'poisDmg added 1 perLevel',
      'poisDmg more 0.9 perLevel',
      'physDmg converted 100 poisDmg',
      'angle added 30',
    ]
  },
  'fire ball' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'proj', 'fire', 'spell' ],
    'specs' : [ {
      type : 'proj',
      color : color.FIRE_COLOR,
      onHit : [ {type : 'circle', color : color.FIRE_COLOR} ]
    } ],
    'baseMods' : [
      'manaCost added 9',
      'speed added 600',
      'range added ' + BASE_SPELL_RANGE,
      'fireDmg added 3 perLevel',
      'fireDmg added 3',
      'fireDmg more 0.5 perLevel',
      'projRadius more 200',
      'aoeRadius more -70',
    ],
    'flavor' : 'Goodness gracious, these balls are great!'
  },
  'ice ball' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'proj', 'cold', 'spell' ],
    'specs' : [ {
      type : 'proj',
      color : color.COLD_COLOR,
      onHit : [ {type : 'circle', color : color.COLD_COLOR} ]
    } ],
    'baseMods' : [
      'manaCost added 9',
      'speed added 600',
      'range added ' + BASE_SPELL_RANGE,
      'coldDmg added 3 perLevel',
      'coldDmg added 3',
      'coldDmg more 0.5 perLevel',
      'projRadius more 200',
      'aoeRadius more -70',
    ]
  },
  'lightning ball' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'proj', 'lightning', 'spell' ],
    'specs' : [ {
      type : 'proj',
      color : color.LIGHT_COLOR,
      onHit : [ {type : 'circle', color : color.LIGHT_COLOR} ]
    } ],
    'baseMods' : [
      'manaCost added 9',
      'speed added 600',
      'range added ' + BASE_SPELL_RANGE,
      'lightDmg added 3 perLevel',
      'lightDmg added 3',
      'lightDmg more 0.5 perLevel',
      'projRadius more 200',
      'aoeRadius more -50',
      'projSpeed more 50',
    ]
  },
  'poison ball' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'proj', 'poison', 'spell' ],
    'specs' : [ {
      type : 'proj',
      color : color.POIS_COLOR,
      onHit : [ {type : 'circle', color : color.POIS_COLOR} ]
    } ],
    'baseMods' : [
      'manaCost added 16',
      'speed added 1000',
      'range added ' + BASE_SPELL_RANGE * 0.7,
      'poisDmg added 5 perLevel',
      'poisDmg added 5',
      'poisDmg more 0.5 perLevel',
      'projRadius more 200',
      'aoeRadius more -70',
    ]
  },
  'ice blast' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'cone', 'cold', 'spell' ],
    'specs' : [ {
      type : 'cone',
      color : color.COLD_COLOR,
      quals : [],
      onHit : [],
      onKill : [],
      onRemove : []
    } ],
    'baseMods' : [
      'manaCost added 5',
      'speed added 400',
      'range added ' + BASE_SPELL_RANGE / 4,
      'coldDmg added 6',
      'coldDmg added 3 perLevel',
      'coldDmg more 0.7 perLevel',
      'angle added 30',
    ]
  },
  'lightning spray' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'cone', 'lightning', 'spell' ],
    'specs' : [ {
      type : 'cone',
      color : color.LIGHT_COLOR,
      quals : [],
      onHit : [],
      onKill : [],
      onRemove : []
    } ],
    'baseMods' : [
      'manaCost added 5',
      'speed added 400',
      'range added ' + BASE_SPELL_RANGE / 4,
      'lightDmg added 6',
      'lightDmg added 3 perLevel',
      'lightDmg more 0.7 perLevel',
      'angle added 30',
    ]
  },
  'pressure wave' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'cone', 'spell' ],
    'specs' : [ {
      type : 'cone',
      color : color.PHYS_COLOR,
      quals : [],
      onHit : [],
      onKill : [],
      onRemove : []
    } ],
    'baseMods' : [
      'manaCost added 10',
      'cooldownTime added 300',
      'speed added 500',
      'range added ' + BASE_SPELL_RANGE / 3,
      'physDmg added 5 perLevel',
      'aoeSpeed more 300',
      'angle more 300',
    ]
  },
  'shadow dagger' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'proj', 'spell' ],
    'specs' : [ {type : 'proj', color : '#000'} ],
    'baseMods' : [
      'manaCost added 10',
      'cooldownTime added 5000',
      'speed added 200',
      'range added ' + BASE_SPELL_RANGE,
      'physDmg added 10 perLevel',
    ]
  },
  'oblivion ray' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'proj', 'spell' ],
    'specs' : [ {type : 'proj', color : '#000'} ],
    'baseMods' : [
      'manaCost added 20',
      'projRadius more 300',
      'projSpeed more -90',
      'speed added 200',
      'range added ' + BASE_SPELL_RANGE * 2,
      'coldDmg added 5 perLevel',
      'lightDmg added 5 perLevel',
    ]
  },
  'telekinesis' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'proj', 'spell' ],
    'specs' : [ {type : 'proj', color : '#778'} ],
    'baseMods' : [
      //'maxMana gainedas 5 manaCost',
      'cooldownTime added 200',
      'speed added 200',
      'range added ' + BASE_SPELL_RANGE,
      'physDmg added 6 perLevel',
    ]
  },
  'health suck' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'proj', 'spell' ],
    'baseMods' : [
      'manaCost added 25', 'speed added 700', 'range added ' + BASE_SPELL_RANGE,
      'physDmg added 9', 'physDmg added 1 perLevel',
      'physDmg more 0.5 perLevel', 'physDmg gainedas 1 hpLeech'
    ]
  },
  'consume' : {
    'prototype' : [ 'basic melee' ],
    'skillType' : 'melee',
    'types' : [ 'proj', 'spell' ],
    'baseMods' : [
      'manaCost added 25',
      //'cooldownTime added 3000',
      'speed added 3000', 'range added ' + BASE_MELEE_RANGE * 5,
      'physDmg added 1 perLevel', 'physDmg more 0.5 perLevel',
      'physDmg gainedas 500 hpLeech'
    ]
  },
  'nova' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'proj', 'circle', 'spell' ],
    'specs' : [ {
      type : 'circle',
      color : color.LIGHT_COLOR,
      quals : [],
      onHit : [],
      onKill : [],
      onRemove : []
    } ],
    'baseMods' : [
      'manaCost added 12', 'speed added 200', 'cooldownTime added 200',
      'range added ' + BASE_SPELL_RANGE / 5, 'aoeRadius more -50',
      'lightDmg added 3 perLevel', 'lightDmg more 1.2 perLevel'
    ]
  },
  'fire nova' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'proj', 'aoecircle', 'spell' ],
    'specs' : [ {type : 'circle', color : color.FIRE_COLOR} ],
    'baseMods' : [
      'manaCost added 12',
      'speed added 200',
      'cooldownTime added 200',
      'range added ' + BASE_SPELL_RANGE / 5,
      'fireDmg added 3 perLevel',
      'fireDmg more 1.2 perLevel',
      'aoeRadius more -50',
    ]
  },
  'ice nova' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'proj', 'aoecircle', 'spell' ],
    'specs' : [ {type : 'circle', color : color.COLD_COLOR} ],
    'baseMods' : [
      'manaCost added 12',
      'speed added 200',
      'cooldownTime added 200',
      'range added ' + BASE_SPELL_RANGE / 5,
      'coldDmg added 3 perLevel',
      'coldDmg more 1.2 perLevel',
      'aoeRadius more -50',
    ]
  },
  'poison nova' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'proj', 'aoecircle', 'spell' ],
    'specs' : [ {type : 'circle', color : color.POIS_COLOR} ],
    'baseMods' : [
      'manaCost added 12',
      'speed added 200',
      'cooldownTime added 200',
      'range added ' + BASE_SPELL_RANGE / 5,
      'poisDmg added 3 perLevel',
      'poisDmg more 1.2 perLevel',
      'aoeRadius more -50',
    ]
  },
  'holy light' : {
    'prototype' : [ 'basic spell' ],
    'skillType' : 'spell',
    'types' : [ 'proj', 'aoecircle', 'spell' ],
    'specs' : [ {type : 'circle', color : color.cbone} ],
    'baseMods' : [
      'manaCost added 20', 'speed added 200', 'cooldownTime added 200',
      'range added ' + BASE_SPELL_RANGE / 5, 'physDmg added 3 perLevel',
      'physDmg more 1.2 perLevel', 'aoeRadius more -50',
      //                    'physDmg gainedas 5 hpLeech'
    ]
  },
  'flame cone' : {
    'prototype' : [ 'basic' ],
    'skillType' : 'melee',
    'types' : [ 'cone', 'melee' ],
    'specs' : [ {
      type : 'cone',
      color : color.FIRE_COLOR,
      quals : [],
      onHit : [],
      onKill : [],
      onRemove : []
    } ],
    'baseMods' : [
      'manaCost added 5',
      'speed added 200',
      'range added ' + BASE_MELEE_RANGE,
      'aoeRadius more 50',
      'fireDmg added 3 perLevel',
    ]
  },
  'lethal strike' : {
    'prototype' : [ 'basic melee' ],
    'skillType' : 'melee',
    'types' : [ 'melee' ],
    'baseMods' : [
      'manaCost added 20',
      'speed added 200',
      'range added ' + BASE_MELEE_RANGE,
      'physDmg added 1 perLevel',
      'physDmg more 2 perLevel',
      'physDmg more 100',
      'cooldownTime added 3000',
    ]
  },
  'deadly volley' : {
    'prototype' : [ 'basic' ],
    'skillType' : 'range',
    'types' : [ 'proj' ],
    'specs' : [ {
      type : 'proj',
      color : '#FFF',
      quals : [],
      onHit : [],
      onKill : [],
      onRemove : []
    } ],
    'baseMods' : [
      'speed added 200', 'range added ' + BASE_RANGE_RANGE, 'manaCost added 20',
      'physDmg added 1 perLevel', 'physDmg more 0.5 perLevel',
      'projCount added 16', 'physDmg more -50', 'angle more -70',
      'cooldownTime added 3000'
    ]
  },
};
