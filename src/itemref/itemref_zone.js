export var zone = {
  'spooky dungeon' : {
    'choices' : [
      'skeleton', 'skeleton archer', 'skeleton mage', 'fire skeleton',
      'skeleton embermage', 'skeleton warmage', 'skeleton champion',
      'skeleton highlord', 'skeleton pyromage'
    ],
    'weights' : [ 50, 20, 10, 10, 5, 10, 10, 10, 10 ],
    'boss' : 'skeleton king',
    'roomCount' : 20,
    'quantity' : [ 1, 1, 3 ],
    'materials' : [
      'metal', 'ember', 'blood', 'blade', 'spike', 'shield', 'potion', 'spine'
    ],
  },
  'dark forest' : {
    'choices' : [
      'wood nymph', 'bat', 'elf', 'elf marksman', 'ent', 'dahd djinn', 'umuri',
      'elf sharpshooter'
    ],
    'weights' : [ 2000, 1500, 1500, 200, 500, 20, 5, 5 ],
    'boss' : 'elf king',
    'roomCount' : 20,
    'quantity' : [ 1, 2, 3 ],
    'materials' : [
      'metal', 'feather', 'eye', 'blade', 'shield', 'spike', 'needle', 'wing',
      'elf ear'
    ],
  },
  'clockwork ruins' : {
    'choices' : [
      'gnome', 'gnome electrician', 'roflcopter', 'harpy', 'shock golem',
      'mechcinerator', 'mechfridgerator', 'mecha watt', 'ser djinn',
      'mecha tank', 'gnome chuck testa'
    ],
    'weights' : [ 200, 100, 100, 100, 50, 50, 50, 50, 1, 50, 5 ],
    'boss' : 'sir mechs-a-lot',
    'roomCount' : 20,
    'quantity' : [ 1, 2, 3 ],
    'materials' : [
      'spark', 'brain', 'blade', 'razor', 'spike', 'potion', 'converter',
      'gnome'
    ],
  },
  'aggro crag' : {
    'choices' : [
      'goblin', 'goblin priest', 'goblin artillery', 'goblin barbarian',
      'fire skeleton', 'fire golem', 'kei djinn', 'goblin bombardier'
    ],
    'weights' : [ 200, 100, 100, 100, 100, 50, 1, 10 ],
    'boss' : 'the inhuman torch',
    'roomCount' : 20,
    'quantity' : [ 1, 2, 3 ],
    'materials' : [
      'ember', 'muscle', 'blood', 'brain', 'shield', 'blade', 'razor',
      'crag shard'
    ]
  },
  'hostile marsh' : {
    'choices' : [
      'zombie', 'angry imp', 'dart imp', 'imp shaman', 'marshwalker', 'mad ape',
      'al-err djinn', 'scalp collector', 'toxic golem', 'imp chieftain', 'hydra'
    ],
    'weights' : [ 200, 100, 100, 100, 80, 80, 1, 50, 20, 10, 20 ],
    'boss' : 'swamp thing',
    'roomCount' : 20,
    'quantity' : [ 1, 2, 6 ],
    'materials' : [
      'spore', 'feather', 'brain', 'shield', 'spike', 'needle', 'potion',
      'imp head'
    ],
  },
  'icy tunnel' : {
    'choices' : [
      'frost skeleton', 'ice golem', 'frost mage', 'frozen warrior', 'yeti',
      'wight', 'frow djinn', 'shiver spirit', 'jesse blueman', 'frost goliath'
    ],
    'weights' : [ 200, 100, 50, 80, 70, 100, 1, 20, 1, 50 ],
    'boss' : 'walter wight',
    'roomCount' : 20,
    'quantity' : [ 1, 2, 4 ],
    'materials' : [
      'ice', 'brain', 'blade', 'razor', 'spike', 'converter', 'wight snow'
    ],
  },
  'gothic castle' : {
    'choices' : [
      'shadow knight', 'ghoul', 'vampire', 'living statue', 'gargoyle',
      'minotaur', 'wraith', 'death knight'
    ],
    'weights' : [ 20, 10, 10, 10, 20, 20, 10, 10 ],
    'boss' : 'acheron',
    'roomCount' : 15,
    'quantity' : [ 1, 3, 6 ],
    'materials' : [
      'metal', 'blood', 'feather', 'muscle', 'brain', 'eye', 'spike', 'blade',
      'shield', 'needle', 'gargoyle'
    ],
  },
  'anthropomorphic savanah' : {
    'choices' : [
      'buzzard', 'hyena', 'lion', 'hippo', 'honey badger', 'cheetah', 'bee'
    ],
    'weights' : [ 1, 1 ],
    'boss' : 'bat',
    'roomCount' : 20,
    'quantity' : [ 20, 500, 5000 ],
  },
  'decaying temple' : {
    'choices' : [
      'buddha', 'ninja', 'ninja assassin', 'samurai', 'fallen samurai',
      'tanuki', 'CFTS', 'the z'
    ],
    'weights' : [ 150, 200, 100, 200, 200, 100, 1, 50 ],
    'boss' : 'treasure hunter',
    'roomCount' : 15,
    'quantity' : [ 2, 2, 6 ],
    'materials' : [
      'metal', 'blood', 'brain', 'eye', 'feather', 'spike', 'blade',
      'business card'
    ],
  },
  'lich tower' : {
    'choices' : [
      'walking meat', 'lich', 'withering goliath', 'abomination', 'slagathor',
      'ice lich', 'tormented colossus'
    ],
    'weights' : [ 400, 200, 50, 100, 1, 10, 10 ],
    'boss' : 'lichie lich',
    'quantity' : [ 2, 2, 7 ],
    'materials' : [
      'metal', 'brain', 'potion', 'razor', 'blood', 'spike', 'blade', 'lichen'
    ],
  },
  'beginners field' : {
    'choices' : [
      'baby slime', 'slime', 'big slime', 'huge slime', 'gigantic slime',
      'adventurer\'s corpse', 'adventuring slime', 'angry puddle'
    ],
    'weights' : [ 100, 500, 1000, 500, 250, 300, 0, 500 ],
    'boss' : 'king slime',
    'quantity' : [ 1, 0, 0 ],
    'materials' : [
      'slime', 'blood', 'brain', 'muscle', 'shield', 'blade', 'spike', 'needle'
    ],
  },
  'wicked dream' : {
    'choices' : [
      'baku', 'incubus', 'succubus', 'lucid dreamer', 'tall man',
      'freddy kooler', 'it', 'your biggest fear'
    ],
    'weights' : [ 1000, 300, 200, 100, 50, 50, 50, 10 ],
    'boss' : 'cthulhu',
    'roomCount' : 20,
    'quantity' : [ 2, 2, 5 ],
    'credit' : 'Nemek',
    'materials' :
        [ 'brain', 'blood', 'razor', 'mirror', 'nightmare', 'spark', 'ice' ],
  },
  'imperial barracks' : {
    'choices' : [
      'pikeman', 'crossbowman', 'griffon', 'crusader', 'cavalier', 'templar',
      'champion', 'zealot', 'paladin', 'seraph'
    ],
    'weights' : [ 1000, 500, 500, 300, 50, 50, 50, 5, 10, 5 ],
    'boss' : 'lost seraph',
    'roomCount' : 20,
    'quantity' : [ 2, 2, 5 ],
    'credit' : 'WirelessKFC',
    'materials' :
        [ 'brain', 'blood', 'razor', 'spike', 'sigil', 'spark', 'metal' ],
  },
  'forgotten tomb' : {
    'choices' : [ 'mummy', 'scarab', 'pharaoh', 'anubis', 'snake' ],
    'weights' : [ 1, 1 ],
    'boss' : 'bat',
    'roomCount' : 20,
    'quantity' : [ 20, 500, 5000 ],
  },
  'shipwreck cove' : {
    'choices' : [
      'swashbucker', 'cannoneer', 'drowned corpse', 'parrot', 'monkey',
      'mermaid', 'first mate'
    ],
    'weights' : [ 1, 1 ],
    'boss' : 'bat',
    'roomCount' : 20,
    'quantity' : [ 20, 500, 5000 ],
  },
  'demonic laboroatory' : {
    'choices' :
        [ 'stitchling', 'mad scientist', 'evil grad student', 'blood golem' ],
    'weights' : [ 20, 10, 10 ],
    'boss' : 'pigbearman',
    'roomCount' : 20,
    'quantity' : [ 2, 3, 4 ],
  },
  'scarred plains' : {
    'choices' :
        [ 'troll', 'cyclops', 'harpy', 'bandit', 'giant', 'frost giant' ],
    'weights' : [ 20, 10, 10 ],
    'boss' : 'pigbearman',
    'roomCount' : 20,
    'quantity' : [ 3, 3, 6 ],
  },
  'hordecave' : {
    'choices' : [ 'vampire', 'shadow knight' ],
    'weights' : [ 1, 1 ],
    'boss' : 'bat',
    'roomCount' : 20,
    'quantity' : [ 20, 500, 5000 ],
  },
  'halls of pain' : {
    'choices' : [
      'vampire', 'shadow knight', 'skeleton king', 'elf king',
      'sir mechs-a-lot', 'flame dragon', 'scalp collector', 'walter wight'
    ],
    'weights' : [ 1, 1, 1, 1, 1, 1, 1, 1 ],
    'boss' : 'vampire',
    'roomCount' : 20,
    'quantity' : [ 20, 500, 5000 ],
  },
  'dojo' : {
    'choices' : [ 'skeleton king' ],
    'weights' : [ 1 ],
    'boss' : 'dummy',
    'roomCount' : 10,
    'quantity' : [ 10, 0, 0 ],
  },
  'empty dojo' : {
    'choices' : [],
    'weights' : [],
    'boss' : 'dummy',
    'roomCount' : 10,
    'quantity' : [ 0, 0, 0 ],
  },
};
