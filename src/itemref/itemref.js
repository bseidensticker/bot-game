import $ from 'jquery';

import {armor} from './itemref_armor';
import {card} from './itemref_card';
import {monster} from './itemref_monster';
import {skill} from './itemref_skill';
import {weapon} from './itemref_weapon';
import {zone} from './itemref_zone';

import log from '../log';
import * as prob from '../prob';

export var ref = {
  'weapon' : weapon,
  'armor' : armor,
  'skill' : skill,
  'card' : card,
  'monster' : monster,
  'zoneOrder' : {
    'order' : [
      'spooky dungeon', 'dark forest', 'aggro crag', 'icy tunnel',
      'hostile marsh', 'clockwork ruins', 'beginners field', 'gothic castle',
      'decaying temple', 'lich tower', 'wicked dream', 'imperial barracks'
    ],
  },
  'zone' : zone,
  'test' : {
    'hngg' : {'a' : 10},
    'fwah' : {'b' : 10},
    'buh' : {'a' : 12},
    'hi' : {'prototype' : [ 'hngg', 'fwah' ], 'b' : 12},
    'foo' : {'prototype' : [ 'hngg', 'buh' ], 'a' : 15},
    'harf' : {'prototype' : [ 'hi', 'foo' ], 'c' : 10}
  },
  // it goes: [hngg, fwah, hi, hngg, buh, foo, harf]: c: 10, b: 12, a: 15
  'materials' : {
    'skull' : {'printed' : 'Skulls', 'category' : 4},
    'heart' : {'printed' : 'Hearts', 'category' : 4},
    'finger' : {'printed' : 'Fingers', 'category' : 4},
    'toe' : {'printed' : 'Toes', 'category' : 4},
    'handle' : {'printed' : 'Handles', 'category' : 4},
    'energy' : {'printed' : 'Energy', 'category' : 4},
    'metal' : {'printed' : 'Metal', 'category' : 3},
    'ember' : {'printed' : 'Embers', 'category' : 3},
    'ice' : {'printed' : 'Ice', 'category' : 3},
    'spark' : {'printed' : 'Sparks', 'category' : 3},
    'spore' : {'printed' : 'Spores', 'category' : 3},
    'feather' : {'printed' : 'Feathers', 'category' : 3},
    'muscle' : {'printed' : 'Muscles', 'category' : 3},
    'eye' : {'printed' : 'Eyes', 'category' : 3},
    'brain' : {'printed' : 'Brains', 'category' : 3},
    'blood' : {'printed' : 'Blood', 'category' : 3},
    'wing' : {'printed' : 'Wings', 'category' : 3},
    'mirror' : {'printed' : 'Mirrors', 'category' : 3},
    'shield' : {'printed' : 'Shields', 'category' : 2},
    'blade' : {'printed' : 'Blades', 'category' : 2},
    'spike' : {'printed' : 'Spikes', 'category' : 2},
    'needle' : {'printed' : 'Needles', 'category' : 2},
    'razor' : {'printed' : 'Razors', 'category' : 2},
    'potion' : {'printed' : 'Potions', 'category' : 2},
    'converter' : {'printed' : 'Converters', 'category' : 2},
    'spine' : {'printed' : 'Spines', 'category' : 1},
    'elf ear' : {'printed' : 'Elf Ears', 'category' : 1},
    'crag shard' : {'printed' : 'Crag Shards', 'category' : 1},
    'wight snow' : {'printed' : 'Wight Snow', 'category' : 1},
    'imp head' : {'printed' : 'Imp Heads', 'category' : 1},
    'gnome' : {'printed' : 'Gnomes', 'category' : 1},
    'gargoyle' : {'printed' : 'Gargoyles', 'category' : 1},
    'business card' : {'printed' : 'Business Cards', 'category' : 1},
    'lichen' : {'printed' : 'Lichen', 'category' : 1},
    'slime' : {'printed' : 'Slime', 'category' : 1},
    'nightmare' : {'printed' : 'Nightmares', 'category' : 1},
    'sigil' : {'printed' : 'Sigils', 'category' : 1},
  },
  'matCategoryBase' : [ '1 indexed for simplicity', 1, 2.3, 2.7, 3.9 ],
  'matDropRates' : {
    'normal' : [ 0.7, 0.2, 0.1, 0 ],
    'rare' : [ 0.2, 0.4, 0.35, 0.05 ],
    'boss' : [ 0, 0, 0, 1 ],
    'slime' : [ 0, 0, 0, 1 ],
  },
  'statnames' : {
    'strength' : 'Strength',
    'dexterity' : 'Dexterity',
    'wisdom' : 'Wisdom',
    'vitality' : 'Vitality',
    'maxHp' : 'Maximum Health',
    'maxMana' : 'Maximum Mana',
    'armor' : 'Armor',
    'dodge' : 'Dodge',
    'accuracy' : 'Accuracy',
    'eleResistAll' : 'Elemental Resistance',
    'hpRegen' : 'Health Regenerated per Second',
    'manaRegen' : 'Mana Regenerated per Second',
    'moveSpeed' : 'Movement Speed',
    'fireResist' : 'Fire Resistance',
    'coldResist' : 'Cold Resistance',
    'lightResist' : 'Lightning Resistance',
    'poisResist' : 'Poison Resistance',
    'physThorns' : 'Physical Thorns',
    'fireThorns' : 'Fire Thorns',
    'coldThorns' : 'Cold Thorns',
    'lightThorns' : 'Lightning Thorns',
    'poisThorns' : 'Poison Thorns',
    'meleeDmg' : 'Melee Damage',
    'rangeDmg' : 'Ranged Damage',
    'spellDmg' : 'Spell Damage',
    'physDmg' : 'Physical Damage',
    'fireDmg' : 'Fire Damage',
    'coldDmg' : 'Cold Damage',
    'lightDmg' : 'Lightning Damage',
    'poisDmg' : 'Poison Damage',
    'hpOnHit' : 'Health Gained on Hit',
    'manaOnHit' : 'Mana Gained on Hit',
    'cooldownTime' : 'Cooldown Length',
    'range' : 'Skill Range',
    'speed' : 'Skill Duration',
    'manaCost' : 'Mana Cost',
    'hpLeech' : 'Leeched Health',
    'manaLeech' : 'Leeched Mana',
    'lineWidth' : 'Line Width',
    'opacity' : 'Opacity',
    'height' : 'Character Height',
    'width' : 'Character Width',
    'aoeRadius' : 'AOE Radius',
    'aoeSpeed' : 'AOE Speed',
    'angle' : 'Angle',
    'projRange' : 'Projectile Range',
    'projRadius' : 'Projectile Radius',
    'projSpeed' : 'Projectile Speed',
    'projCount' : 'Additional Projectiles'
  }
};

/*
  fudge: a = 1
  fwah: a = 3
  sherbet: a = 2

  asdf: [fudge(2), sherbet(3)]

  fdsa: [fwah(5)]

  buh: [asdf(1), fdsa(4)]

  [asdf, fudge, sherbet, fdsa, fwah]
*/

function recExtend(name, r, names) {
  if ('prototype' in r[name] && r[name]['prototype'].length > 0) {
    for (var i = 0; i < r[name]['prototype'].length; i++) {
      names = recExtend(r[name]['prototype'][i], r, names);
    }
  }
  names[names.length] = name;
  // log.debug("recExtend, name %s, names now %s", name,
  // JSON.stringify(names));
  return names;
}

export function expand(type, name) {
  if (!(type in ref) || !(name in ref[type])) {
    log.error('Could not find reference for a %s named %s', type, name);
    throw ('fudge');
    return;
  }

  var names = recExtend(name, ref[type], []);
  var item = Object.assign({}, ref[type][name]);
  for (var i = 0; i < names.length; i++) {
    item = Object.assign({}, item, ref[type][names[i]]);
  }
  if ('itemType' in item) {
    throw (sprintf(
        'Found a "itemType" key in item %s. No item is allowed to have "itemType" as it it set in expand',
        JSON.stringify(item)));
  }
  item['itemType'] = type;
  if ('name' in item) {
    throw (sprintf(
        'Found a "name" key in item %s. No item is allowed to have "name" as it it set in expand',
        JSON.stringify(item)));
  }
  item['name'] = name;

  log.debug('itemref.expand(%s, %s), Final item: %s', type, name,
            JSON.stringify(item));

  return item;
}


/*
   "hot sword": {
   "slot": "weapon",
   "levels": 10,
   "modType": "added",
   "stat": "fireDmg",
   "perLevel": 2
   },
   "surprisingly hot sword": {
   "slot": "weapon",
   "levels": 10,
   "modType": "more",
   "stat": "fireDmg",
   "perLevel": 1
   },
   {base: [], perLevel: 'fireDmg more 1'}

   added
   converted increased % of other
   converted decreased % of max
   more

   hatred 50% phys as cold
   phys to light 50% physical converted to light
   cold to fire 50%  cold converted to fire

   100 phys (after added and more), 0 of else

   phys to light:
   100 (-50) phys
   50 light


   hatred:
   phys is 50
   cold 25

   cold to fire:
   25 - 12.5 cold
   12.5 fire

   50 phys
   50 light
   12.5 cold
   12.5 fire
   0 pois

   "hot sword": {
   "slot": "weapon",
   "levels": 10,
   "mods": [
   'fireDmg added 2 perLevel',
   'fireDmg more 1 perLevel'
   ]
   },

   itemref has this format:
   mods: [
   ['fireDmg more 100', 'dmg'],
   ['physDmg converted 50 fireDmg', 'dmg'],
   ['fireDmg more 1 perLevel', 'dmg']
   ]

   [
   ['physDmg more 100', 'dmg'],
   ['physDmg added 5 perLevel', 'dmg']
   ]

   compileCards converts to this:

   primary verb amt special(perLevel / element inc ase of converted and
   gainedas)



   hatred:

   {base: ['physDmg gainedas coldDmg 50'], perLevel: 'physDmg gainedas 2
   coldDmg'}

 */
