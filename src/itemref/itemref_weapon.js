export var weapon = {
  ////////////////////
  /// MELEE //////////
  ////////////////////
  'cardboard sword' : {
    'mods' : [ 'physDmg added 6', 'physDmg more 0.5 perLevel' ],
    'weaponType' : 'melee',
  },
  'hand axe' : {
    'mods' :
        [ 'speed more -20', 'physDmg added 10', 'physDmg more 0.55 perLevel' ],
    'weaponType' : 'melee',
  },
  'stone hammer' : {
    'mods' :
        [ 'speed more 20', 'physDmg added 10', 'physDmg more 1.1 perLevel' ],
    'weaponType' : 'melee',
  },
  'falchion' : {
    'mods' :
        [ 'speed more -25', 'physDmg added 12', 'physDmg more 0.7 perLevel' ],
    'weaponType' : 'melee',
  },
  'morning star' : {
    'mods' :
        [ 'speed more 100', 'physDmg added 14', 'physDmg more 2 perLevel' ],
    'weaponType' : 'melee',
  },
  'long sword' : {
    'mods' : [
      'speed more -20', 'speed more -0.1 perLevel', 'physDmg added 16',
      'physDmg more 0.6 perLevel'
    ],
    'weaponType' : 'melee',
  },
  'spikey mace' : {
    'mods' :
        [ 'speed more 50', 'physDmg added 16', 'physDmg more 1.5 perLevel' ],
    'weaponType' : 'melee',
  },
  'spiked battle axe' : {
    'mods' : [
      'speed more -30',
      'physDmg added 20',
      'physDmg more 0.8 perLevel',
      'accuracy more 2 perLevel',

    ],
    'weaponType' : 'melee',
  },
  'winged axe' : {
    'mods' : [
      'range more 50', 'range more 0.1 perLevel', 'physDmg added 25',
      'physDmg more 0.8 perLevel'
    ],
    'weaponType' : 'melee',
  },
  'dream eater' : {
    'mods' : [
      'physDmg gainedas 0.5 hpLeech', 'physDmg gainedas 0.5 manaLeech',
      'physDmg added 50', 'physDmg more 0.75 perLevel'
    ],
    'weaponType' : 'melee',
  },
  'tanto' : {
    'mods' : [
      'range more -60',
      'speed more -30',
      'physDmg added 1 perLevel',
      'physDmg more 0.75 perLevel',
    ],
    'weaponType' : 'melee',
  },
  'katana' : {
    'mods' : [
      'speed more -30',
      'physDmg added 1 perLevel',
      'physDmg more 0.7 perLevel',
    ],
    'weaponType' : 'melee',
  },
  'wakizashi' : {
    'mods' : [
      'range more -90',
      'accuracy more 100',
      'physDmg added 1 perLevel',
      'physDmg more 0.8 perLevel',
    ],
    'weaponType' : 'melee',
  },
  'barbarian blade' : {
    'mods' : [
      'strength gainedas 0.2 moveSpeed',
      'strength more 0.2 perLevel',
      'physDmg added 23',
      'physDmg more 0.6 perLevel',

    ],
    'weaponType' : 'melee',
  },
  'grounding rod' : {
    'mods' : [
      'physDmg converted 100 lightDmg',
      'physDmg added 21',
      'physDmg more 0.55 perLevel',

    ],
    'weaponType' : 'melee',
  },
  'pike' : {
    'mods' : [
      'physDmg added 30', 'range added 1000', 'speed more 80',
      'physDmg more 1.8 perLevel'
    ],
    'weaponType' : 'melee',
  },
  'angelic blade' : {
    'mods' : [
      'physDmg added 2 perLevel', 'fireDmg added 2 perLevel',
      'coldDmg added 2 perLevel', 'lightDmg added 2 perLevel',
      'meleeDmg more 1 perLevel'
    ],
    'weaponType' : 'melee',
  },
  ////////////////////
  ///// RANGED ///////
  ////////////////////
  'wooden bow' : {
    'mods' : [ 'physDmg added 5', 'physDmg more 0.5 perLevel' ],
    'weaponType' : 'range',
  },
  'elf bow' : {
    'mods' :
        [ 'projSpeed more 50', 'physDmg added 7', 'physDmg more 0.6 perLevel' ],
    'weaponType' : 'range',
  },
  'hand crossbow' : {
    'mods' :
        [ 'physDmg added 7', 'speed more -30', 'physDmg more 0.6 perLevel' ],
    'weaponType' : 'range',
  },
  'crossbow' : {
    'mods' :
        [ 'speed more 30', 'physDmg added 9', 'physDmg more 0.85 perLevel' ],
    'weaponType' : 'range',
  },
  'composite bow' : {
    'mods' :
        [ 'range more 20', 'physDmg added 20', 'physDmg more 0.75 perLevel' ],
    'weaponType' : 'range',
  },
  'shuriken' : {
    'mods' : [
      'projCount added 2', 'range more -40', 'physDmg added 15',
      'physDmg more 0.7 perLevel'
    ],
    'weaponType' : 'range',
  },
  'yumi' : {
    'mods' : [ 'physDmg added 20', 'physDmg more 0.8 perLevel' ],
    'weaponType' : 'range',
  },
  'compound bow' : {
    'mods' : [
      'physDmg added 20',
      'physDmg more 0.6 perLevel',
      'projSpeed more 0.5 perLevel',
    ],
    'weaponType' : 'range',
  },
  ////////////////////
  ////// SPELL ///////
  ////////////////////
  'simple wand' : {
    'mods' : [ 'spellDmg more 15', 'spellDmg more 0.5 perLevel' ],
    'weaponType' : 'spell',
  },
  'knobby wand' : {
    'mods' : [
      'spellDmg more 15',
      'spellDmg more 0.7 perLevel',
      'speed more -0.1 perLevel',
    ],
    'weaponType' : 'spell',
  },
  'pewter wand' : {
    'mods' : [
      'spellDmg more 50', 'spellDmg more 1.5 perLevel', 'eleResistAll more -30'
    ],
    'weaponType' : 'spell',
  },
  'delicate wand' : {
    'mods' :
        [ 'speed more -20', 'spellDmg more 25', 'spellDmg more 1 perLevel' ],
    'weaponType' : 'spell',
  },
  'dragonstone wand' : {
    'mods' : [ 'spellDmg more 0.8 perLevel', 'fireDmg more 1 perLevel' ],
    'weaponType' : 'spell',
  },
  'fairy wand' : {
    'mods' : [ 'wisdom added 3 perLevel', 'spellDmg more 1.2 perLevel' ],
    'weaponType' : 'spell',
  },
  'star wand' : {
    'mods' : [ 'spellDmg more 50', 'spellDmg more 1 perLevel' ],
    'weaponType' : 'spell',
  },
  'demon wand' : {
    'mods' : [ 'hpRegen added -50', 'spellDmg more 1.4 perLevel' ],
    'weaponType' : 'spell',
  },
  'succubus skull' : {
    'mods' : [ 'maxHp converted 70 maxMana', 'spellDmg more 2 perLevel' ],
    'weaponType' : 'spell',
  },
  'magic wand' : {
    'mods' : [ 'speed more -20', 'spellDmg more 1.2 perLevel' ],
    'weaponType' : 'spell',
    'flavor' : 'BZZZZZZZZZZZZZZ'
  },
};
