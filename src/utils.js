import $ from 'jquery';
import * as _ from 'underscore';

import {ref} from './itemref/itemref';
import log from './log';
import {Model} from './model';

export function newBaseStatsDict() {
  var a, i, j, l;
  var comp = {};
  for (i = 0; i < arguments.length; i++) {
    a = arguments[i];
    for (j = 0; j < a.length; j++) {
      comp[a[j]] = {added : 0, more : 1, converted : {}, gainedas : {}};
    }
  }
  return comp;
}

export function computeStats(entity, bsd, keys) {
  _.each(keys, function(stat) { entity[stat] = computeStat(bsd, stat); });
}

export function cloneStat(from) {
  return {
    added : from.added,
    more : from.more,
    converted : $.extend({}, from.converted),
    gainedas : $.extend({}, from.gainedas)
  };
}

export function cloneStats(from, keys) {
  var all = {};
  _.each(keys, function(stat) { all[stat] = cloneStat(from[stat]); });
  return all;
}

// [primary] [verb] [amt] [special]   Note: special is either perLevel or
// dmgType in case of converted and gainedas mod restrictions: can only have
// 4, cannot have a converted or gainedas as perlevel
export function addMod(dict, mod) {
  // console.log(mod);
  if (mod === undefined) {
    log.error('addMod called with undefined mod');
  }
  var s = mod.split(' ');
  var amt = parseFloat(s[2]);
  if (s[1] === 'added') {
    dict[s[0]]['added'] += amt;
  } else if (s[1] === 'more') {
    dict[s[0]]['more'] *= 1 + (amt / 100);
  } else if (s[1] === 'gainedas' || s[1] === 'converted') {
    if (dict[s[0]][s[1]][s[3]]) {
      dict[s[0]][s[1]][s[3]] += amt;
    } else {
      dict[s[0]][s[1]][s[3]] = amt;
    }
  } else {
    console.log('addMod about to barf with state ', dict, mod);
    throw ('shoot');
  }
}

export function applyPerLevel(mod, level) {
  var s = mod.split(' ');
  if (s.length === 4 && s[3] === 'perLevel') {
    if (s[1] === 'more') {
      if (s[2] > 0) {
        s[2] = s[2] * level;
      } else {
        s[2] = Math.pow(1 + parseFloat(s[2]) / 100, level) * 100 - 100;
      }
    } else {
      s[2] = parseFloat(s[2]) * level;
    }
    s.pop();
    return s.join(' ');
  } else {
    return mod;
  }
}

export function applyPerLevels(mods, level) {
  var ret = [];
  for (var i = mods.length; i--;) {
    ret.push(applyPerLevel(mods[i], level));
  }
  return ret;
}

export function prettifyMods(mods, level) {
  var res = [];
  _.each(mods, function(mod, i) { res[i] = applyPerLevel(mod, level); });

  var flatModDefs = flattenSameMods(res);
  res = [];

  _.each(flatModDefs, function(flatmod) {
    var finalmod = '';
    var spl = flatmod.split(' ');
    var val = prettifyNum(parseFloat(spl[2]));
    if (spl.length === 3) {
      if (spl[1] === 'added') {
        if (spl[2] >= 0) {
          finalmod = '+' + val + ' ' + ref.statnames[spl[0]];
        } else {
          finalmod = val + ' ' + ref.statnames[spl[0]];
        }
      } else if (spl[1] === 'more') {
        if (spl[2] >= 0) {
          finalmod = val + '% More ' + ref.statnames[spl[0]];
        } else {
          finalmod = Math.abs(val) + '% Less ' + ref.statnames[spl[0]];
        }
      }
    } else {
      if (spl[1] === 'gainedas') {
        finalmod = val + '% of ' + ref.statnames[spl[0]] + ' Gained As ' +
                   ref.statnames[spl[3]];
      } else if (spl[1] === 'converted') {
        finalmod = val + '% of ' + ref.statnames[spl[0]] + ' Converted To ' +
                   ref.statnames[spl[3]];
      } else {
        log.error('infobox display not configured for : ' + flatmod);
        finalmod = flatmod + ' unimplemented';
      }
    }
    res.push(finalmod);
  });
  return res;
}

export function prettifyPerLvlMods(mods) {
  mods = _.filter(mods, function(mod) {
    var spl = mod.split(' ');
    return spl[spl.length - 1] === 'perLevel';
  });
  return prettifyMods(mods, 1);
}

export function flattenSameMods(mods) {
  var fin = [];
  var lookup = {};

  _.each(mods, function(mod) {
    var spl = mod.split(' ');
    if (spl.length === 4) {
      fin.push(mod);
    } else if (spl.length === 3) {
      var stat = spl[0];
      var type = spl[1];
      if (lookup[type] === undefined) {
        lookup[type] = {};
      }
      if (lookup[type][stat] === undefined) {
        lookup[type][stat] = [];
      }
      lookup[type][stat].push(parseFloat(spl[2]));
    } else {
      throw ('weird mods in utils.flattenSameMods');
    }
  });
  _.each([ 'added', 'more', 'converted', 'gainedas' ], function(typeKey) {
    var typeObj = lookup[typeKey];
    _.each(typeObj, function(statArr, statKey) {
      var total;
      if (typeKey === 'more') {
        total = 1;
        for (var i = statArr.length; i--;) {
          total *= statArr[i] / 100 + 1;
        }
        total = (total - 1) * 100;
        fin.push(statKey + ' ' + typeKey + ' ' + total.toFixed(2));
      } else {
        total = 0;
        for (var i = statArr.length; i--;) {
          total += statArr[i];
        }
        fin.push(statKey + ' ' + typeKey + ' ' + total);
      }
    });
  });
  return fin;
}

export function computeStat(section, stat) {
  let obj = section[stat];
  let convPct = 100;
  let res = obj.added * obj.more;

  _.each(obj.converted, function(value, key) {
    let convAmt = obj.converted[key];
    if (convAmt > convPct) {
      convAmt = convPct;
    }
    section[key].added += convAmt / 100 * res;
    convPct -= convAmt;
  });

  res *= (convPct / 100);

  _.each(obj.gainedas, function(value, key) {
    let gainedAmt = obj.gainedas[key];
    section[key].added += gainedAmt / 100 * res;
  });

  return res;
}

export function firstCap(str) {
  if (str === undefined) {
    log.error('utils.firstCap called with undefined string');
    return;
  }
  var words = str.split(' ');
  _.each(
      words,
      function(word, i) { words[i] = word[0].toUpperCase() + word.slice(1); });
  return words.join(' ');
}

var numberSuffixes = [
  '',    'k',   'M',   'B',   'T',   'Qa',  'Qi',  'Sx',
  'Sp',  'Oc',  'No',  'De',  'UnD', 'DuD', 'TrD', 'QaD',
  'QiD', 'SeD', 'SpD', 'OcD', 'NoD', 'Vi',  'UnV'
];

export function prettifyNum(n) {
  if (typeof (n) === "string") {
    n = parseFloat(n);
  }
  if (n === Infinity) {
    return 'Infinity';
  }
  if (n < 0.001 && n > 0) {
    return n.toExponential(3);
  }
  if (n < 1000) {
    if (n >= 100) {
      return Math.floor(n).toString();
    }
    if (n === Math.floor(n)) {
      return Math.floor(n).toString();
    }
    var prec = 1;
    if (n < 1) {
      prec = 3;
    } else if (n < 10) {
      prec = 2;
    }
    return n.toFixed(prec);
  }
  var l, q, r;
  l = Math.floor(Math.log10(n));
  q = Math.floor(l / 3);
  if (q >= numberSuffixes.length) {
    return n.toPrecision(2);
  }
  r = l % 3;
  n /= Math.pow(1000, q);
  if (r === 2) {
    return Math.round(n).toString() + numberSuffixes[q];
  }
  return n.toFixed(2 - r) + numberSuffixes[q];
}

var presentableSlotDict = {
  'weapon' : 'Weapon',
  'head' : 'Head',
  'chest' : 'Chest',
  'hands' : 'Hand',
  'legs' : 'Leg',
  'skill' : 'Skill'
};

export function presentableSlot(slotStr) {
  var res = presentableSlotDict[slotStr];
  if (!res) {
    res = slotStr;
  }
  return res;
}

export function spaceToUnderscore(str) {
  var arr = str.split(' ');
  return arr.join('_');
}

export function addAllMods(bsd, mods) {
  _.each(mods, function(mod) { addMod(bsd, mod); });
}

// turns shorthand from monster definitions into usable cards
// [['hot sword', 1], ['hard head', 1]] => [{mods: [(hot sword mods)], level:
// 1}, {mods: [(hard head mods)], level: 1}]
export function expandSourceCards(sourceCards, level) {
  return _.flatten(_.map(sourceCards, function(card) {
    if (card[0] === undefined) {
      throw ('crap! did you forget a comma after card line in itemref?');
    }
    return applyPerLevels(ref.card[card[0]].mods, card[1] + level);
  }, this));
}

export var Throttler = Model.extend({
  initialize : function(fns, minTime) {
    this.fns = typeof (fns) === 'function' ? [ fns ] : fns;
    this.minTime = minTime;
    this.lastCall = 0;
    this.timeout = null;
    this.throttled = this._throttled.bind(this);
    this.clear = this._clear.bind(this);
  },

  _callFns : function() {
    this._clear();
    this.lastCall = new Date().getTime();
    _.each(this.fns, function(fn) { fn(); });
  },

  _clear : function() {
    clearTimeout(this.timeout);
    this.timeout = null;
  },

  _throttled : function() {
    var now = new Date().getTime();
    var sinceLast = now - this.lastCall;

    if (sinceLast > this.minTime) {
      this._callFns();
    } else if (this.timeout === null) {
      this.timeout =
          setTimeout(this._callFns.bind(this), this.minTime - sinceLast);
    }
  },
});

// Takes an existing moving average value and returns the next iteration given
// the [avg], the [newSample], the lookback window, [n], and how many samples
// this sample should count for, [ns].
export function computeMovingAvg(avg, newSample, n, ns) {
    if (ns > n) {
        return newSample;
    }
    let oldWeight = (n - ns) / n;
    let newWeight = 1 - oldWeight;
    return avg * oldWeight + newSample * newWeight;
}

// Takes an existing moving average value and returns the next iteration given
// the [avg], the [newSample], the lookback window, [n], and how many samples
// this sample should count for, [ns].
export function computeMovingPowma(avg, newSample, n, ns) {
    if (ns > n) {
        return newSample;
    }
    let oldLinearWeight = (n - ns) / n;
    let oldPowWeight = Math.pow(oldLinearWeight, 8);

    return avg * oldPowWeight + newSample * (1 - oldPowWeight);
}

// Takes an existing moving average value and returns the next iteration given
// the [avg], the [newSample], the lookback window, [n], and how many samples
// this sample should count for, [ns].
export function computeMovingEwma(avg, newSample, n, ns) {
    if (ns > n) {
        return newSample;
    }
    let newWeight = Math.log2(ns / n + 1);
    let oldWeight = 1 - newWeight;
    return avg * oldWeight + newSample * newWeight;
}



/* exports.extend({
 *   applyPerLevel : applyPerLevel,
 *   applyPerLevels : applyPerLevels,
 *   expandSourceCards : expandSourceCards,
 *   newBaseStatsDict : newBaseStatsDict,
 *   cloneStats : cloneStats,
 *   prettifyMods : prettifyMods,
 *   prettifyPerLvlMods : prettifyPerLvlMods,
 *   addAllMods : addAllMods,
 *   addMod : addMod,
 *   computeStats : computeStats,
 *   computeStat : computeStat,
 *   firstCap : firstCap,
 *   spaceToUnderscore : spaceToUnderscore,
 *   presentableSlot : presentableSlot,
 *   prettifyNum : prettifyNum,
 *   Throttler : Throttler
 * });*/
