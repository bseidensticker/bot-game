import $ from 'jquery';
import * as _ from 'underscore';

import * as attacks from './attacks';
import {dropFactory} from './drops';
import * as entity from './entity';
import {gl} from './globals';
import * as itemref from './itemref/itemref';
import log from './log';
import {Model} from './model';
import * as prob from './prob';
import * as utils from './utils';

export var GearModel = Model.extend({
  initialize : function() {
    this.xp = 0;
    this.level = 1;
    this.baseMods = [];
    this.cards = [ undefined, undefined, undefined, undefined, undefined ];
    this.equipped = false;
    this.isNew = false;
    this.hasNewCards = false;
    this.inRecycle = false;
    this.isRecycled = false;
  },

  toJSON : function() {
    return {
      xp : this.xp,
      level : this.level,
      cardNames : _.pluck(_.compact(this.cards), 'name'),
      isNew : this.isNew,
      itemType : this.itemType,
      name : this.name,
      inRecycle : this.inRecycle,
      isRecycled : this.isRecycled
    };
  },

  fromJSON : function(data, cardInv) {
    _.extend(this, data);
    this.loadCards(this.cardNames, cardInv);
  },

  loadCards : function(cardNames, cardInv) {
    _.each(cardNames, function(name, i) {
      this.equipCard(_.findWhere(cardInv.getModels(), {name : name}), i);
    }, this);
  },

  applyXp : function(xp) {
    var levels = 0;
    this.xp += xp;
    while (this.canLevel()) {
      this.xp -= this.getNextLevelXp();
      this.level++;
      levels++;
    }
    return levels;
  },

  canLevel : function() { return this.xp >= this.getNextLevelXp();},

  getNextLevelXp :
      function() { return Math.floor(100 * Math.pow(1.3, (this.level - 1)));},

  pctLeveled : function() { return this.xp / this.getNextLevelXp();},

  getMods : function() {
    var cards = _.compact(this.cards);
    var mods =
        _.flatten(_.map(cards, function(card) { return card.getMods(); }));
    return mods.concat(utils.applyPerLevels(this.baseMods, this.level));
  },

  equipCard : function(card, slot) {
    if (slot >= this.cards.length || (card && card.itemType !== 'card')) {
      return false;
    }

    if (card === undefined) {
      if (this.cards[slot]) {
        this.actuallyUnequipCard(slot);
      }
      gl.EquipEvents.trigger('change');
      return false;
    }

    if (card.equipped) { // card already equipped
      var curSlot = this.getCardSlot(card);
      if (slot === curSlot) {
        return false;
      }
      if (this.cards[slot]) {
        this.actuallyEquipCard(this.cards[slot], curSlot);
      } else {
        this.actuallyUnequipCard(curSlot);
      }
      this.actuallyEquipCard(card, slot);
    } else {
      if (this.cards[slot]) {
        this.actuallyUnequipCard(slot);
      }
      this.actuallyEquipCard(card, slot);
    }

    // this trigger is exclusively for communication between gear and
    // equipped gear model so egm doesn't have to listenTo and stopListening
    // on every single gear change
    gl.EquipEvents.trigger('change');
    return true;
  },

  actuallyEquipCard : function(card, slot) {
    card.equipped = true;
    card.gearModel = this;
    this.cards[slot] = card;
  },

  actuallyUnequipCard : function(slot) {
    this.cards[slot].equipped = false;
    this.cards[slot].gearModel = this;
    this.cards[slot] = undefined;
  },

  swapCards : function(other) {
    if (other === undefined) {
      return;
    }
    var card;
    for (var i = 0; i < this.cards.length; i++) {
      if (other.cards[i]) {
        card = other.cards[i];
        other.actuallyUnequipCard(i);
        this.actuallyEquipCard(card, i);
      }
    }
  },

  getCardSlot : function(card) {
    if (!card) {
      return undefined;
    }
    for (var i = this.cards.length; i--;) {
      if (this.cards[i] && this.cards[i].name === card.name) {
        return i;
      }
    }
    return undefined;
  },

  unequipCards : function() {
    for (var i = 0; i < this.cards.length; i++) {
      if (this.cards[i]) {
        this.cards[i].equipped = false;
        this.cards[i] = undefined;
      }
    }
  },

  recycle : function() {
    log.warning('Recycling gear %s', this.name);
    this.isRecycled = true;
    return this.slot;
  }
});

function getSortKey(model) {
  var num = 0;
  if (model.itemType === 'weapon') {
    return 'a' + {melee : 'a', range : 'b', spell : 'c'}[model.weaponType] +
           model.name;
  } else if (model.itemType === 'armor') {
    return 'b' +
           {head : 'a', chest : 'c', legs : 'd', hands : 'b'}[model.slot] +
           model.name;
  } else if (model.itemType === 'skill') {
    return 'c' + model.name.toLowerCase();
  }
}

// itemType -> slot
// weapon  type -> weaponType
// skill class -> skillType

export var ArmorModel = GearModel.extend({
  initialize : function(name) {
    GearModel.prototype.initialize.call(this);
    _.extend(this, itemref.expand('armor', name));

    this.name = name;
    this.itemType = 'armor';
    this.baseMods = this.mods;
    this.key = getSortKey(this);
  }
});

export var WeaponModel = GearModel.extend({
  initialize : function(name) {
    GearModel.prototype.initialize.call(this);
    _.extend(this, itemref.expand('weapon', name));

    this.name = name;
    this.itemType = 'weapon';
    this.slot = 'weapon';
    this.baseMods = this.mods;
    this.key = getSortKey(this);
  }
});

export var SkillModel = GearModel.extend({
  initialize : function(name) {
    GearModel.prototype.initialize.call(this);
    _.extend(this, itemref.expand('skill', name));

    this.name = name;
    this.itemType = 'skill';
    this.slot = 'skill';
    this.key = getSortKey(this);
  },

  computeAttrs : function(baseDmgStats, weaponType) {
    if (this.skillType !== 'spell' && this.skillType !== weaponType) {
      this.disabled = true;
      return;
    }
    this.disabled = false;

    var dmgKeys = entity.dmgKeys;
    var attackSpecDmgKeys = entity.attackSpecDmgKeys;

    var all = utils.cloneStats(baseDmgStats, dmgKeys);

    // COMMENTED OUT! - remove added baseDmg amounts from spells (they can
    // only be modified by cards on skill or by more increases)
    /*if (this.skillType === 'spell') {
        _.each(actualDmgKeys, function(dmgType) {
            all[dmgType].added = 0;
        });
    }*/

    utils.addAllMods(all, this.getMods());

    var metaType = this.skillType + 'Dmg';
    utils.computeStats(
        this, all,
        [ 'projCount' ]); // Hack fix for deadly focus convert order
    utils.computeStats(this, all, [ metaType ]);
    var extraDmgMod = this[metaType];

    if (this.monster) { // Set in MonsterSkillSpec
      extraDmgMod *= 0.5;
    }

    utils.computeStats(this, all, dmgKeys);

    _.each(attackSpecDmgKeys, function(key) { this[key] *= extraDmgMod; },
           this);

    this.speed = Math.max(10, this.speed);

    this.calcAttacks();
  },

  calcAttack : function(spec) {
    var keys = entity.attackSpecKeys;
    var dmgKeys = entity.attackSpecDmgKeys;

    // Ensure spec has necessary keys from skill
    _.each(keys, function(key) { spec[key] = this[key]; }, this);

    // ensure angle and projCount, log errors
    if (spec.angle === undefined) {
      log.error('In calcAttack, Skill angle is undefined');
      spec.angle = 30;
    }
    if (spec.projCount === undefined) {
      log.error('In calcAttack, Skill projCount is undefined');
      spec.projCount = 1;
    }

    // apply qualifiers, lots of special case code
    if (spec.quals && spec.quals.length) {
      _.each(spec.quals, function(qual) {
        var split = qual.split(' ');
        if (split[0] === 'dmg') {
          if (split[1] === 'more') {
            var dmgMod = 1 + (parseFloat(split[2]) / 100);
            _.each(dmgKeys, function(key) { spec[key] *= dmgMod; });
          } else {
            log.error('Trying to apply an invalid damage qualifier %s', qual);
          }
        } else if (split[0].indexOf('Dmg') > -1) {
          log.error('Trying to apply an invalid damage qualifier %s', qual);
        } else if (split[0] === 'projCount') {
          spec.projCount += parseFloat(split[2]);
        } else if (split[0] === 'angle') {
          spec.angle += parseFloat(split[2]);
        }
      }, this);
    }

    // Total damage summed here for convenience.  Used in takeDamage to
    // quickly figure out
    //   how much damage was mitigated to adjsut the hpLeech and manaLeech
    spec.totalDmg = spec.physDmg + spec.lightDmg + spec.coldDmg + spec.fireDmg +
                    spec.poisDmg;

    // Ensure projCount and angle have sane values
    spec.projCount = Math.floor(spec.projCount);
    spec.angle = Math.floor(Math.abs(spec.angle));
    if (spec.projCount < 1) {
      spec.projCount = 1;
    }
    if (spec.projCount > 1 && spec.angle === 0) {
      spec.angle = 5;
    }

    var arrs = [ 'onHit', 'onKill', 'onRemove' ];
    _.each(arrs, function(arr) {
      if (spec[arr] && spec[arr].length) {
        _.each(spec[arr], this.calcAttack, this);
      }
    }, this);
  },

  calcAttacks : function() {
    this.specs = $.extend({}, this.specs);
    _.each(this.specs, this.calcAttack, this);
  },

  getTotalDmg : function() {
    return (this.physDmg + this.fireDmg + this.coldDmg + this.lightDmg +
            this.poisDmg)
        .toFixed(2);
  },

  getDps : function() {
    var totalDur = this.speed + this.cooldownTime;
    var dps = this.getTotalDmg() / totalDur * 1000;
    return dps.toFixed(2);
  }
});

export var MonsterSkillModel = SkillModel.extend({monster : true});

export var Skillchain = Model.extend({
  initialize : function() {
    this.skills = [ undefined, undefined, undefined, undefined, undefined ];
  },

  toJSON : function() { return _.pluck(_.compact(this.skills), 'name');},

  fromJSON : function(data, inv) {
    _.each(data, function(skillName, slot) {
      this.equip(_.findWhere(inv.getModels(), {name : skillName}), slot, false);
    }, this);
  },

  equip : function(skill, slot, isMonster) {
    log.info('skillchain equip');

    var change = false;

    if (skill === undefined) {
      change = this.unequip(slot);
    } else if (skill.itemType === 'skill') {
      if (skill.equipped) { // if skill we are trying to equip is already
                            // equipped
        var curSlot = this.getSkillSlot(skill); // get the slot it's equipped in
        if (curSlot !== slot) {    // if it was dropped in a different slot
          if (this.skills[slot]) { // if something is already in that slot
            this.skills[curSlot] = this.skills[slot];
            this.skills[slot] = skill;
          } else { // slot dropping into is currently empty
            this.skills[slot] = this.skills[curSlot];
            this.skills[curSlot] = undefined;
          }
          change = true;
        }
      } else {
        skill.swapCards(this.skills[slot]);
        this.unequip(slot);
        skill.equipped = true;
        this.skills[slot] = skill;
        change = true;
        if (!isMonster) {
          log.warning('Skillchain equipped %s into slot %d', skill.name, slot);
        }
      }
    }
    if (change) {
      this.trigger('change');
    }
    return change;
  },

  unequip : function(slot) {
    var skill = this.skills[slot];
    if (skill !== undefined) {
      skill.equipped = false;
      skill.disabled = false;
      skill.unequipCards(); // TODO save equipped cards
      log.warning('Skillchain unequipping %s from slot %d', skill.name, slot);
      this.skills[slot] = undefined;
      return true;
    }
    return false;
  },

  getSkillSlot : function(skill) {
    if (!skill) {
      return undefined;
    }
    for (var i = this.skills.length; i--;) {
      if (this.skills[i] && this.skills[i].name === skill.name) {
        return i;
      }
    }
    return undefined;
  },

  ranges : function() {
    if (this.skills.length === 0) {
      this.furthest = undefined;
      this.shortest = undefined;
      return;
    }

    var i, l;
    var ranges = _.pluck(_.compact(this.skills), 'range');

    this.shortest = ranges.min();
    this.furthest = ranges.max();
  },

  computeAttrs : function(dmgStats, weaponType) {
    this.lastDmgStats = dmgStats;
    _.each(_.compact(this.skills),
           function(skill) { skill.computeAttrs(dmgStats, weaponType); });

    this.trigger('skillComputeAttrs');
    gl.DirtyQueue.mark('skillComputeAttrs');
  },

  applyXp : function(xp) {
    var levels = 0;
    _.each(_.compact(this.skills),
           function(skill) { levels += skill.applyXp(xp); });
    return levels;
  },
});

export var EquippedGearModel = Model.extend({

  slots : [ 'weapon', 'head', 'hands', 'chest', 'legs' ],

  // weapon slots: weapon
  // armor slots: head, chest, hands, legs

  initialize : function() {
    // this line and event object is used exclusively for equipment card
    // changes to propogate through here and up to the hero so the egm doesn't
    // have to listen and stop listening every equip
    this.listenTo(gl.EquipEvents, 'change', this.propChange);
  },

  toJSON : function() {
    var slot;
    var obj = {};
    for (var i = 0; i < this.slots.length; i++) {
      slot = this.slots[i];
      if (this[slot]) {
        obj[slot] = this[slot].name;
      }
    }
    return obj;
  },

  fromJSON : function(data, inv) {
    _.each(data, function(itemName, slot) {
      this.equip(_.findWhere(inv.getModels(), {name : itemName}), slot);
    }, this);
  },

  propChange : function() { this.trigger('change');},

  equip : function(item, slot) {
    log.info('equipped gear model equip');

    var change = false;

    if (item === undefined) {
      change = this.unequip(slot);
    } else if (this[slot] && this[slot].name === item.name) {
      change = false;
    } else if (item.slot === slot) {
      item.swapCards(this[slot]);
      this.unequip(slot);
      this[slot] = item;
      item.equipped = true;
      change = true;
    }

    if (change) {
      this.trigger('change');
    }
    return change;
  },

  unequip : function(slot) {
    var item = this[slot];
    if (item !== undefined) {
      item.equipped = false;
      item.unequipCards(); // TODO save equipped cards
      this[slot] = undefined;
      return true;
    }
    return false;
  },

  getMods : function() {
    var items = _.compact(
        _.map(this.slots, function(slot) { return this[slot]; }, this));
    return _.flatten(_.map(items, function(item) { return item.getMods(); }));
  },

  toDict : function() {
    return _.object(
        this.slots,
        _.map(this.slots, function(slot) { return this[slot]; }, this));
  },

  applyXp : function(xp) {
    var levels = 0;
    _.each(this.slots, function(slot) {
      if (this[slot] !== undefined) {
        levels += this[slot].applyXp(xp);
      }
    }, this);
    return levels;
  },
});

// Mixed into ItemCollection and CardCollection
// CardModel and GearModel both have inRecycle and isRecycled variables
// ItemCollection and CardCollection both have "newEventStr" attribute
var RecycleCollectionMixin = Model.extend({
  getModels : function() {
    return _.filter(
        this.models,
        function(model) { return !model.inRecycle && !model.isRecycled; },
        this);
  },

  addDrops : function(drops) {
    var messages = [];
    _.each(drops, function(drop) {
      var existingModel = _.findWhere(this.models, {name : drop.name});
      if (existingModel) {
        if (existingModel.isRecycled) {
          log.warning('%s "%s" dropped again.', this.newEventStr, drop.name);
          existingModel.isRecycled = false;
          gl.DirtyQueue.mark(this.newEventStr);
        }
        return;
      }
      this.models.push(drop.make());
      this.hasNew = true;
      this.sort();
      gl.DirtyQueue.mark(this.newEventStr);
      messages.push(drop.message());
      log.warning('%s: %s', this.newEventStr, drop.name);
    }, this);
    return messages;
  }
});

export var ItemCollection = RecycleCollectionMixin.extend({
  newEventStr : 'item:new',

  initialize : function() {
    this.models = [];
    this.sort();
    this.hasNew = false;
  },

  noobGear : function() {
    this.models = [
      new WeaponModel('cardboard sword'),
      new WeaponModel('wooden bow'),
      new WeaponModel('simple wand'),
      new SkillModel('basic melee'),
      new SkillModel('basic range'),
      new SkillModel('basic spell'),
      new ArmorModel('balsa helmet'),
      new ArmorModel('latex gloves'),
      new ArmorModel('t-shirt'),
      new ArmorModel('jeans'),
    ];
    this.sort();
    _.each(this.models, function(m) { m.isNew = true; });
  },

  toJSON : function() {
    return _.map(this.models, function(model) { return model.toJSON(); });
  },

  fromJSON : function(data, cardInv) {
    this.models = _.map(data, function(model) {
      var item;
      if (model.itemType === 'skill') {
        item = new SkillModel(model.name);
      } else if (model.itemType === 'weapon') {
        item = new WeaponModel(model.name);
      } else if (model.itemType === 'armor') {
        item = new ArmorModel(model.name);
      }
      item.fromJSON(model, cardInv);
      return item;
    }, this);
    this.sort();
  },

  byId : function(id) { return _.findWhere(this.models, {id : id});},

  sort : function() {
    this.models.sort(function(a, b) {
      if (a.key < b.key) {
        return -1;
      }
      if (a.key > b.key) {
        return 1;
      }
      return 0;
    });
  }
});

export var MaterialManager = Model.extend({
  initialize : function(cardInv) {
    var materials = itemref.ref.materials;
    _.each(materials, function(obj, mat) { this[mat] = 0; }, this);
    this.cardInv = cardInv;
  },

  toJSON : function() {
    var result = {};
    _.each(_.keys(itemref.ref.materials),
           function(mat) { result[mat] = this[mat]; }, this);
    return result;
  },

  fromJSON : function(data) { _.extend(this, data);},

  addDrops : function(drops) {
    let messages = [];
    let matKeys = _.keys(itemref.ref.materials);
    if (drops.length > 0) {
      _.each(drops, function(drop) {
        if (matKeys.indexOf(drop.name) === -1) {
          log.error('weird drop: %s %d', drop.name, drop.quantity);
        }
        this[drop.name] += drop.quantity;
        messages.push(drop.message());
      }, this);
      if (gl.settings.autoCraft) {
        this.tryLevelEquippedCards();
      }
      gl.DirtyQueue.mark('material:new');
    }
    return messages;
  },

  canLevelCard : function(card) {
    // returns whether card can level at least once
    if (card === undefined) {
      console.log('attempting to level undefined card');
    }
    var cost = card.getLevelCost();
    var canLevel = true;

    _.each(cost, function(amt, mat) {
      if (this[mat] < amt) {
        canLevel = false;
      }
    }, this);
    return canLevel;
  },

  tryLevelCard : function(card, levels) {
    // tries to level up a card 'levels' number of times.
    // returns whether it successfully leveled up that many times
    if (levels === undefined) {
      levels = 1;
    }

    for (var i = 0; i < levels; i++) {
      var cost = card.getLevelCost();
      var canLevel = this.canLevelCard(card);
      if (canLevel) {
        _.each(cost, function(amt, mat) { this[mat] -= amt; }, this);
        card.levelUp();
      } else {
        return false;
      }
    }
    return true;
  },

  tryLevelEquippedCards : function() {
    var anyLeveled = false;
    _.each(this.cardInv.models.reverse(), function(card) {
      if (card.equipped) {
        anyLeveled = anyLeveled || this.tryLevelCard(card, 50);
      }
    }, this);
    this.cardInv.models.reverse();
    if (anyLeveled) {
      gl.DirtyQueue.mark('card:new');
    }
  },

});

export var CardModel = Model.extend({
  initialize : function(name) {
    _.extend(this, itemref.expand('card', name));
    this.itemType = 'card';
    this.name = name;
    this.equipped = false;
    this.qp = 0;
    this.level = 1;
    this.isNew = false;
    this.inRecycle = false;
    this.isRecycled = false;
  },

  toJSON : function() {
    return {
      name : this.name,
      qp : this.qp,
      level : this.level,
      isNew : this.isNew,
      inRecycle : this.inRecycle,
      isRecycled : this.isRecycled
    };
  },

  fromJSON : function(data) { _.extend(this, data);},

  getMods : function() { return utils.applyPerLevels(this.mods, this.level);},

  getLevelCost : function() {
    var cost = {};
    _.each(this.materials, function(mat) {
      var matref = itemref.ref.materials[mat];
      if (cost[mat] === undefined) {
        cost[mat] = 0;
      }
      if (matref === undefined) {
        log.error('cannot find reference to mat named %s', mat);
      }
      var category = itemref.ref.materials[mat].category;
      var base = itemref.ref.matCategoryBase[category];
      cost[mat] += Math.max(1, Math.ceil(Math.pow(base, this.level)));
      if (category === 1) {
        cost[mat] = Math.ceil(cost[mat] * Math.sqrt(this.level));
      }
    }, this);
    return cost;
  },

  levelUp : function() {
    this.level++;
    gl.DirtyQueue.mark('card:level');
    this.trigger('change');
  },

  /*applyQp: function(qp) {
      var levels = 0;
      this.qp += qp;
      while (this.canLevel()) {
          this.qp -= this.getNextLevelQp();
          this.level++;
          levels++;
      }
      if (levels && this.equipped) {
          this.trigger('change');
      }
      return levels;
  },*/

  canLevel : function() { return gl.canLevelCard(this);},

  /*getNextLevelQp: function() {
      return Math.pow(3, this.level);
  },*/

  pctLeveled : function() { return 0;},

  recycle : function() {
    log.warning('Recycling card %s', this.name);
    this.isRecycled = true;
    return this.slot;
  }
});

export var CardCollection = RecycleCollectionMixin.extend({
  newEventStr : 'card:new',

  initialize : function() {
    this.models = [];
    this.hasNew = false;
  },

  noobGear : function() {
    this.models = [ new CardModel('sharpened'), new CardModel('stinging') ];
    this.sort();
    _.each(this.models, function(m) { m.isNew = true; });
  },

  toJSON : function() {
    return _.map(this.models, function(model) { return model.toJSON(); });
  },

  fromJSON : function(data) {
    this.models = _.map(data, function(cardData) {
      var c = new CardModel(cardData.name);
      c.fromJSON(cardData);
      return c;
    });
    this.sort();
  },

  sort : function(sortStyle) {
    this.models.sort(function(a, b) {
      var ascore = a.level;
      var bscore = b.level;
      if (sortStyle === 'craft') {
        ascore += a.canLevel() ? 1000 : 0;
        bscore += b.canLevel() ? 1000 : 0;
      }
      // console.log(a.name, a.level, a.canLevel(), ascore, b.name, bscore);
      if (ascore > bscore) {
        return -1;
      }
      if (ascore < bscore) {
        return 1;
      }
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
      return 0;
    });
  }
});

export var RecycleManager = Model.extend({
  initialize : function(inv, cardInv) {
    this.inv = inv;
    this.cardInv = cardInv;
  },

  ff : function(item) { return item.inRecycle && !item.isRecycled;},

  getModels : function() {
    return _.filter(this.inv.models, this.ff)
        .concat(_.filter(this.cardInv.models, this.ff));
  },

  getRecycleValue : function(slot) {
    var slotTranslate = {
      'skill' : 'energy',
      'weapon' : 'handle',
      'head' : 'skull',
      'chest' : 'heart',
      'hands' : 'finger',
      'legs' : 'toe'
    };
    var zLvl = (this.zone.hero.spec.level / 2.35);
    var name = slotTranslate[slot];
    var amt = Math.ceil(Math.pow(2.5, zLvl));
    return dropFactory('material', [ name, amt ]);
  },

  getAllValueStr : function() {
    var valids = this.getModels();
    if (valids.length == 0) {
      return 'nothing.';
    }
    var mats = {};
    _.each(valids, function(valid) {
      var drop = this.getRecycleValue(valid.slot);
      if (mats[drop.nameStr] === undefined) {
        mats[drop.nameStr] = drop.quantity;
      } else {
        mats[drop.nameStr] += drop.quantity;
      }
    }, this);
    var finStr = '';
    var commasRemaining = _.keys(mats).length - 1;
    _.each(mats, function(amt, mat) {
      finStr += utils.prettifyNum(amt) + ' ' + mat;
      if (commasRemaining > 0) {
        finStr += ', ';
        commasRemaining -= 1;
      } else {
        finStr += '.';
      }
    }, this);
    return finStr;
  },
});

// Responds to events, updates state on items
export var NewStateManager = Model.extend({
  initialize : function(inv, cardInv) {
    this.inv = inv;
    this.cardInv = cardInv;
    this.newCardSlots = {
      weapon : false,
      head : false,
      hands : false,
      chest : false,
      legs : false,
      skill : false
    };

    this.listenTo(gl.DirtyListener, 'item:new', this.update);
    this.listenTo(gl.DirtyListener, 'card:new', this.update);
    this.listenTo(gl.DirtyListener, 'removeNew', this.update);
    gl.DirtyQueue.mark('item:new');
  },

  update : function() {
    gl.DirtyQueue.mark('newChange'); // Note: Async
    this.inv.hasNew = false;
    var items = this.inv.getModels();
    for (var i = items.length; i--;) {
      if (items[i].isNew) {
        this.inv.hasNew = true;
        break;
      }
    }
    this.cardInv.hasNew = false;
    var cards = this.cardInv.getModels();
    for (var i = cards.length; i--;) {
      if (cards[i].isNew) {
        this.cardInv.hasNew = true;
        break;
      }
    }
    _.each(this.newCardSlots,
           function(value, key) { this.newCardSlots[key] = false; }, this);
    var m;
    for (var i = cards.length; i--;) {
      m = cards[i];
      this.newCardSlots[m.slot] = this.newCardSlots[m.slot] || m.isNew;
    }
    _.each(this.inv.models,
           function(m) { m.hasNewCards = this.newCardSlots[m.slot]; }, this);
  }
});
