import * as _ from 'underscore';

import * as inventory from './inventory';
import {ref} from './itemref/itemref';
import {Model} from './model';
import {firstCap, prettifyNum} from './utils';

/*
  dropFactory returns an object that implements the Drop interface
  constructor(): takes the monster itemRef data for that drop and saves it
  properly make(): returns the created object, called iff it needs to be made
  message(): returns the message that should be displayed
 */

export function dropFactory(type, refData) {
  // type is card, item, or skill
  if (type === 'card') {
    return new CardDrop(refData);
  }
  if (type === 'item') {
    return new ItemDrop(refData);
  }
  if (type === 'skill') {
    return new SkillDrop(refData);
  }
  if (type === 'material') {
    return new MatDrop(refData);
  }
  throw ('shoot, drop factory called with invalid type argument: ' + type);
}

function MatDrop(refData) {
  this.name = refData[0];
  this.nameStr = ref.materials[this.name]['printed'];
  this.quantity = refData[1];
}

MatDrop.prototype.message = function() {
  return '+' + prettifyNum(this.quantity) + ' ' + this.nameStr;
};

function CardDrop(refData) {
  // ['card name', cardlvl]
  this.name = refData[0];
  this.level = refData[1];
}

CardDrop.prototype.make = function() {
  var card = new inventory.CardModel(this.name);
  card.isNew = true;
  return card;
};

CardDrop.prototype.message =
    function() { return 'New Card: ' + firstCap(this.name); };

function ItemDrop(refData) {
  this.itemType = refData[0];
  this.name = refData[1];
}

ItemDrop.prototype.make = function() {
  var item;
  if (this.itemType === 'weapon') {
    item = new inventory.WeaponModel(this.name);
  }
  if (this.itemType === 'armor') {
    item = new inventory.ArmorModel(this.name);
  }
  item.isNew = true;
  return item;
};

ItemDrop.prototype.message =
    function() { return 'New Item: ' + firstCap(this.name); };

function SkillDrop(refData) {
  // 'skillname'
  this.name = refData;
}

SkillDrop.prototype.make = function() {
  var skill = new inventory.SkillModel(this.name);
  skill.isNew = true;
  return skill;
};

SkillDrop.prototype.message =
    function() { return 'New Skill: ' + firstCap(this.name); };
