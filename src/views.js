import * as Backbone from 'backbone';
import $ from 'jquery';
import * as _ from 'underscore';

import * as entity from './entity';
import {FooterView} from './footer-views';
import {gl} from './globals';
import {ref} from './itemref/itemref';
import log from './log';
import {Model} from './model';
import * as utils from './utils';
import {
  firstCap,
  presentableSlot,
  prettifyNum,
  spaceToUnderscore,
  Throttler
} from './utils';
import {Point, PointFromEvent} from './vectorutils';
import {VisView} from './vis';

var FOOTER_HEIGHT = 114;

export var GameView = Backbone.View.extend({
  el : $('body'),

  initialize : function(options, game) {
    log.info('GameView initialize');

    this.configTab = new ConfigTab({}, game);
    this.statsTab = new StatsTab({}, game);
    this.itemTab = new ItemTab({}, game);
    this.helpTab = new HelpTab({}, game);
    this.accountTab = new AccountTab({}, game);
    this.mapTab = new MapTab({}, game);
    this.cardTab = new CardTab({}, game);
    this.craftTab = new CraftTab({}, game);
    this.recycleTab = new RecycleTab({}, game);
    this.buildTab = new BuildTab({}, game);

    this.leftButtonsView = new LeftButtonsView({}, this.getLeft.bind(this));
    this.rightButtonsView = new RightButtonsView({}, game.inv, game.cardInv,
                                                 this.getRight.bind(this));
    this.footerView = new FooterView({}, game, this.itemTab.dragHandler,
                                     this.cardTab.dragHandler, this.craftTab);
    this.infoBox = new InfoBox({}, this.getRight.bind(this));

    this.visView = new VisView({}, game, this);
    this.$el.append(this.visView.render().el);

    this.$el.append(this.statsTab.render().el);
    this.$el.append(this.mapTab.render().el);
    this.$el.append(this.itemTab.render().el);
    this.$el.append(this.cardTab.render().el);
    this.$el.append(this.craftTab.render().el);
    this.$el.append(this.recycleTab.render().el);

    this.$el.append(this.leftButtonsView.render().el);
    this.$el.append(this.rightButtonsView.render().el);
    this.$el.append(this.infoBox.el);
    this.$el.append(this.footerView.render().el);
    this.$el.append(this.configTab.render().el);
    this.$el.append(this.helpTab.render().el);
    this.$el.append(this.accountTab.render().el);
    this.$el.append(this.buildTab.render().el);

    this.resizeThrottler =
        new Throttler(function() { gl.DirtyQueue.mark('throttledResize'); },
                      500);
    $(window).on('resize', this.resizeThrottler.throttled);
  },

  getLeft : function() {
    var left = 0;
    if (this.statsTab.tvm.visible) {
      left = this.statsTab.$el.width();
    } else if (this.mapTab.tvm.visible) {
      left = this.mapTab.$el.width();
    } else if (this.helpTab.tvm.visible) {
      left = this.helpTab.$el.width();
    } else if (this.configTab.tvm.visible) {
      left = this.configTab.$el.width();
    } else if (this.accountTab.tvm.visible) {
      left = this.accountTab.$el.width();
    }
    return left;
  },

  getRight : function() {
    var right = 0;
    if (this.itemTab.tvm.visible) {
      right = this.itemTab.$el.width();
    } else if (this.cardTab.tvm.visible) {
      right = this.cardTab.$el.width();
    } else if (this.craftTab.tvm.visible) {
      right = this.craftTab.$el.width();
    } else if (this.recycleTab.tvm.visible) {
      right = this.recycleTab.$el.width();
    } else if (this.buildTab.tvm.visible) {
      right = this.buildTab.$el.width();
    }
    return right;
  },

  getCenter : function() {
    var left = this.getLeft();
    var right = this.getRight();

    return new Point((window.innerWidth - left - right) / 2 + left,
                     (window.innerHeight - FOOTER_HEIGHT) / 2);
  },
});

var TabVisibilityManager = Model.extend({
  // This is a handy class which handles events triggered by clicking the
  // tab-opening buttons in the footer,
  //   shows or hides the appropriate tabs, and triggers tabShow and tabHide
  //   events for others to listen to.
  // Each TabView has an instance

  initialize : function(
      name, $el, render,
      showEventStr) { // hideEventStr1, hideEventStr2, ..., hideEventStrN
    this.name = name;
    this.$el = $el;
    this.render =
        render; // Make sure render is bound to the correct this context
    this.visible = false;
    this.$el.addClass('hidden');
    this.listenTo(gl.UIEvents, showEventStr, this.toggleVisible);
    for (var i = 4; i < arguments.length; i++) {
      this.listenTo(gl.UIEvents, arguments[i], this.hide);
    }
  },

  show : function() {
    log.UI('Showing %s tab', this.name);
    this.visible = true;
    this.$el.removeClass('hidden');

    // TODO - little janky
    if (this.name === 'cards' || this.name === 'craft') {
      gl.cardSort(this.name);
    }

    this.render();
    gl.UIEvents.trigger('tabShow', this.name);
  },

  hide : function() {
    log.info('Hiding %s tab', this.name);
    this.visible = false;
    this.$el.addClass('hidden');
    gl.UIEvents.trigger('tabHide', this.name);
  },

  toggleVisible : function() {
    if (this.visible) {
      this.hide();
    } else {
      this.show();
    }
    gl.DirtyQueue.mark('centerChange');
  }
});

var EntityView = Backbone.View.extend({
  tagName : 'div',

  template : _.template($('#kv-table-template').html()),

  initialize : function(options, zone) {
    // TODO add selective updating
    this.listenTo(gl.DirtyListener, 'computeAttrs', this.render);
    this.zone = zone;
  },

  render : function() {
    var skill, statname;
    var data = {};
    var skilldata = {};
    var matdata = {};
    var body = this.model;
    var spec = body.spec;

    data.body = [
      [ 'Name', spec.name ],
      [ 'Level', spec.level ],
    ];

    var zoneData = this.zone.statResult;

    for (var i = 0; i < this.model.skills.length; i++) {
      var arr = [];
      skill = this.model.skills[i];
      _.each(entity.dmgKeys, function(key) {
        if (key === 'projCount' && skill.spec.projCount <= 1) {
          return;
        }
        if (key === 'decayRange') {
          return;
        }
        if (key === 'radius' || key === 'rate' ||
            key === 'angle') { // todo only if not aoe
          return;
        }
        statname = ref.statnames[key];
        arr.push([ statname, skill.spec[key].toFixed(2) ]);
        if (key === 'accuracy') {
          var dodge = zoneData.avgs['dodge'];
          var alevel = this.model.spec.level;
          var dlevel = this.zone.level;
          var attAcc = skill.spec.accuracy;
          var hitChance = 3 * (attAcc / (attAcc + dodge)) *
                          (0.5 + (alevel / (alevel + dlevel)) / 2);
          var chance = Math.max(Math.min(0.99, hitChance), 0.01);
          chance = (chance * 100).toFixed(2);
          arr.push([ 'Avg % Chance to Hit', chance + '%' ]);
        }
      }, this);

      var coolIn = Math.max(0, skill.coolAt - gl.time);
      arr.push([ 'Cool In', Math.floor(coolIn) ]);
      skilldata[skill.spec.name] = arr;
    }

    data.spec = [];
    var specKeys =
        entity.defKeys.concat(entity.eleResistKeys).concat(entity.thornKeys);
    ;
    var key;

    var resistTypes = {
      'armor' : 'physDmg',
      'fireResist' : 'fireDmg',
      'coldResist' : 'coldDmg',
      'lightResist' : 'lightDmg',
      'poisResist' : 'poisDmg',
    };

    for (var i = 0; i < specKeys.length; i++) {
      key = specKeys[i];
      statname = ref.statnames[key];

      data.spec.push([ statname, this.model.spec[key].toFixed(2) ]);
      if (key === 'dodge') {
        statname = 'Avg % Dodge Chance (this zone)';
        var dodge = this.model.spec.dodge;
        var alevel = this.zone.level;
        var dlevel = this.model.spec.level;
        var attAcc = zoneData.avgs['accuracy'];
        var hitChance = 3 * (attAcc / (attAcc + dodge)) *
                        (0.5 + (alevel / (alevel + dlevel)) / 2);
        var chance = Math.max(Math.min(0.99, 1 - hitChance), 0.01);
        chance = (chance * 100).toFixed(2);
        data.spec.push([ statname, chance + '%' ]);
        statname = 'Min % Dodge Chance (this zone)';
        var dodge = this.model.spec.dodge;
        var alevel = this.zone.level;
        var dlevel = this.model.spec.level;
        var attAcc = zoneData.maxs['accuracy'];
        var hitChance = 3 * (attAcc / (attAcc + dodge)) *
                        (0.5 + (alevel / (alevel + dlevel)) / 2);
        var chance = Math.max(Math.min(0.99, 1 - hitChance), 0.01);
        chance = (chance * 100).toFixed(2);
        data.spec.push([ statname, chance + '%' ]);
      }
      if (Object.keys(resistTypes).indexOf(key) !== -1) {
        var dmgType = resistTypes[key];
        var prettyDmgName = ref.statnames[dmgType];
        prettyDmgName = prettyDmgName.split(' ')[0];
        var avgDmg = zoneData.avgs[dmgType];
        var maxDmg = zoneData.maxs[dmgType];
        var avgRedFactor = avgDmg / (avgDmg + this.model.spec[key]);
        var maxRedFactor = maxDmg / (maxDmg + this.model.spec[key]);
        data.spec.push([
          'Avg % ' + prettyDmgName + ' Taken',
          (avgRedFactor * 100).toFixed(4) + '%'
        ]);
        data.spec.push([
          'Max % ' + prettyDmgName + ' Taken',
          (maxRedFactor * 100).toFixed(4) + '%'
        ]);
      }
    }
    var version = this.model.spec.versionCreated.split('-').join('.');
    data.spec.push([ 'Character Version', version ]);
    data.spec.push([ 'Score', this.zone.unlockedZones ]);
    data.spec.push([ 'Last Death', firstCap(this.model.spec.lastDeath) ]);

    matdata['Materials'] = [];
    // TODO - possible improper use of each
    _.each(ref.materials, function(mat, matname) {
      matdata['Materials'].push(
          [ mat.printed, prettifyNum(spec.matInv[matname]) ]);
    }, this);

    this.$el.html(this.template(Object.assign(
        {data : data, skilldata : skilldata, matdata : matdata}, utils)));
    return this;
  },
});

export var StatsTab = Backbone.View.extend({
  tagName : 'div',
  className : 'stats',

  initialize : function(options, game) {
    log.info('GameView initialize');

    this.zone = game.zone;
    this.last = {};
    this.heroView = new EntityView({model : this.zone.hero}, this.zone);
    this.listenTo(gl.DirtyListener, 'zoneTick', this.render);

    this.tvm = new TabVisibilityManager(
        'stats', this.$el, this.render.bind(this), 'footer:buttons:stats',
        'footer:buttons:map', 'footer:buttons:help', 'footer:buttons:config',
        'footer:buttons:account');

    this.$el.append('<div class="holder"></div>');
    this.$holder = this.$('.holder');

    this.resize();
    this.listenTo(gl.DirtyListener, 'throttledResize', this.resize);
  },

  resize : function() {
    var size = new Point(window.innerWidth, window.innerHeight - FOOTER_HEIGHT);
    this.$el.css({height : size.y});
    this.$('.holder').css({height : size.y});
  },

  diffs : function() {
    return {
      inst_uid : this.zone.iuid,
      heroPos : this.zone.heroPos,
      liveMonsCount : this.zone.liveMons().length
    };
  },

  render : function() {
    if (!this.tvm.visible) {
      return this;
    }
    var diffs = this.diffs();
    this.$holder.html(this.heroView.render().el);
    return this;
  },
});

var InfoBox = Backbone.View.extend({
  tagName : 'div',
  className : 'infoBox',
  template : _.template($('#info-box-template').html(), utils),

  initialize : function(options, getRight) {
    this.getRight = getRight;

    this.listenTo(gl.UIEvents, 'itemSlotMouseenter', this.show);
    this.listenTo(gl.UIEvents, 'itemSlotMouseleave', this.hide);

    this.listenTo(gl.DirtyListener, 'hero:xp', this.render);
    this.listenTo(gl.DirtyListener, 'card:level', this.render);

    this.listenTo(gl.UIEvents, 'tabShow', this.updateRight);
    this.listenTo(gl.UIEvents, 'tabHide', this.updateRight);
  },

  updateRight : function() { this.$el.css('right', this.getRight() + 43); },

  show : function(view) {
    if (view.model !== undefined) {
      this.view = view;
      this.render();
    }
  },

  hide : function() {
    this.view = undefined;
    this.render();
  },

  render : function() {
    if (this.view) {
      this.$el.css('display', 'block');
      // Avoid crashes due to undefineds
      this.$el.html(this.template(Object.assign({}, utils, this.view)));
    } else {
      this.$el.css('display', 'none');
    }

    return this;
  }
});

var LeftButtonsView = Backbone.View.extend({
  tagName : 'div',
  className : 'buttons left',
  template : _.template($('#left-buttons-template').html()),

  events : {
    'mousedown .config-button' : 'clickConfig',
    'mousedown .help-button' : 'clickHelp',
    'mousedown .stats-button' : 'clickStats',
    'mousedown .map-button' : 'clickMap',
    'mousedown .account-button' : 'clickAccount'
  },

  initialize : function(options, getLeft) {
    this.getLeft = getLeft;

    this.listenTo(gl.UIEvents, 'tabShow', this.onTabShow);
    this.listenTo(gl.UIEvents, 'tabHide', this.onTabHide);

    this.listenTo(gl.DirtyListener, 'newChange', this.setNew);
  },

  onTabShow : function(name) {
    this.updatePos();
    this.$('.' + name + '-button').addClass('open');
  },

  onTabHide : function(name) {
    this.updatePos();
    this.$('.' + name + '-button').removeClass('open');
  },

  updatePos : function() { this.$el.css('left', this.getLeft()); },

  clickConfig : function() { gl.UIEvents.trigger('footer:buttons:config') },
  clickHelp : function() { gl.UIEvents.trigger('footer:buttons:help'); },
  clickStats : function() { gl.UIEvents.trigger('footer:buttons:stats'); },
  clickMap : function() { gl.UIEvents.trigger('footer:buttons:map'); },
  clickAccount : function() { gl.UIEvents.trigger('footer:buttons:account'); },

  render : function() {
    this.$el.html(this.template(utils));
    this.updatePos();
    return this;
  },
});

var RightButtonsView = Backbone.View.extend({
  tagName : 'div',
  className : 'buttons right',
  template : _.template($('#right-buttons-template').html()),

  events : {
    'mousedown .inv-button' : 'clickInv',
    'mousedown .cards-button' : 'clickCards',
    'mousedown .craft-button' : 'clickCraft',
    'mousedown .recycle-button' : 'clickRecycle',
    'mousedown .build-button' : 'clickBuild'
  },

  initialize : function(options, inv, cardInv, getRight) {
    this.inv = inv;
    this.cardInv = cardInv;
    this.getRight = getRight;

    this.listenTo(gl.UIEvents, 'tabShow', this.onTabShow);
    this.listenTo(gl.UIEvents, 'tabHide', this.onTabHide);

    this.listenTo(gl.DirtyListener, 'newChange', this.setNew);
  },

  setNew : function() {
    if (this.inv.hasNew) {
      this.$('#invnewflag').show();
    } else {
      this.$('#invnewflag').hide();
    }
    if (this.cardInv.hasNew) {
      this.$('#cardnewflag').show();
    } else {
      this.$('#cardnewflag').hide();
    }
  },

  onTabShow : function(name) {
    this.updatePos();
    this.$('.' + name + '-button').addClass('open');
  },

  onTabHide : function(name) {
    this.updatePos();
    this.$('.' + name + '-button').removeClass('open');
  },

  updatePos : function() { this.$el.css('right', this.getRight()); },

  clickInv : function() { gl.UIEvents.trigger('footer:buttons:inv'); },
  clickCards : function() { gl.UIEvents.trigger('footer:buttons:cards'); },
  clickCraft : function() { gl.UIEvents.trigger('footer:buttons:craft'); },
  clickRecycle : function() { gl.UIEvents.trigger('footer:buttons:recycle'); },
  clickBuild : function() { gl.UIEvents.trigger('footer:buttons:build'); },

  render : function() {
    this.$el.html(this.template(utils));
    this.updatePos();
    return this;
  },
});

var FilterView = Backbone.View.extend({
  tagName : 'span',
  events : {'mousedown' : 'onClick'},
  onClick : function() { this.trigger('click', this); },

  initialize : function(options, text, value) {
    this.el.innerHTML = text;
    this.value = value;
  },

  select : function() {
    this.selected = true;
    this.$el.addClass('selected');
  },

  unselect : function() {
    this.selected = false;
    this.$el.removeClass('selected');
  }
});

var AbstractFilterBarView = Backbone.View.extend({
  tagName : 'div',

  initialize : function(options, texts, values) {
    this.texts = texts;
    this.values = values;
    this.views = [];
  },

  render : function() {
    var frag = document.createDocumentFragment();
    var view;
    for (var i = 0; i < this.texts.length; i++) {
      view = new FilterView({}, this.texts[i], this.values[i]);
      this.listenTo(view, 'click', this.onClick);
      frag.appendChild(view.render().el);
      this.views.push(view);
    }
    this.$el.append(frag);

    this.views[0].select();
    this.selectedValue = this.views[0].value;

    return this;
  },

  onClick : function(view) {
    _.invoke(this.views, 'unselect');
    view.select();
    this.selectedValue = view.value;
    gl.DirtyQueue.mark('filterChange');
  },

  filter : function() { throw ('This is an abstract class'); },
});

var WeaponTypeFilterBarView = AbstractFilterBarView.extend({
  filter : function(items) {
    if (this.selectedValue === undefined) {
      return items;
    }
    return _.filter(items, function(item) {
      if (item.itemType === 'armor') {
        return false;
      }
      if (item.itemType === 'weapon') {
        return item.weaponType === this.selectedValue;
      }
      if (item.itemType === 'skill') {
        return item.skillType === this.selectedValue;
      }
    }, this);
  },
});

var SlotTypeFilterBarView = AbstractFilterBarView.extend({
  filter : function(items) {
    if (this.selectedValue === undefined) {
      return items;
    }
    return _.filter(items,
                    function(item) { return item.slot === this.selectedValue; },
                    this);
  },
});

var EquippedFilterBarView = AbstractFilterBarView.extend({
  filter : function(items) {
    if (this.selectedValue === undefined) {
      return items;
    }
    return _.filter(
        items, function(item) { return item.equipped === this.selectedValue; },
        this);
  }
});

var DragHandler = Backbone.View.extend({
  tagName : 'div',
  template : _.template($('#draggable-template').html()),
  className : 'dragSlot',

  DISABLED : 0,
  DISABLED_WAIT : 1,
  UP_DRAG : 2,
  DOWN_DRAG : 3,

  initialize : function(extraMDF) {
    this.uid = _.uniqueId('mml');
    this.state = this.DISABLED;
    this.extraMDF = extraMDF;
    $('body').append(this.el);
    $('body').on('mousedown ' + this.uid, this.onMousedown.bind(this));
    $('body').on('mousemove ' + this.uid, this.onMousemove.bind(this));
    $('body').on('mouseup ' + this.uid, this.onMouseup.bind(this));

    this.updateBodySize();
    this.listenTo(gl.DirtyListener, 'throttledResize', this.updateBodySize);
  },

  onViewMousedown : function(event, model) {
    if (this.state === this.DISABLED) {
      log.info('disabled to disabled wait');
      this.state = this.DISABLED_WAIT;
      this.model = model;
      this.dragStart = PointFromEvent(event);
      this.$el.html(this.template(Object.assign(this.model, utils)));
    }
  },

  onMousedown : function(event) {
    if (this.extraMDF !== undefined) {
      this.extraMDF(event);
    }
    if (this.state === this.UP_DRAG) {
      log.info('up drag to down drag');
      this.state = this.DOWN_DRAG;
    }
  },

  onMousemove : function(event) {
    if (this.state === this.DISABLED_WAIT) {
      if (this.dragStart.dist2(PointFromEvent(event)) > 25) {
        log.info('disabled wait to down drag');
        this.state = this.DOWN_DRAG;
        this.startDragging(event);
      }
    } else if (this.state === this.DOWN_DRAG || this.state === this.UP_DRAG) {
      this.updatePos(event);
    }
  },

  onMouseup : function(event) {
    if (this.state === this.DISABLED_WAIT) {
      log.info('Disabled wait to up drag');
      this.state = this.UP_DRAG;
      this.startDragging(event);
    } else if (this.state === this.DOWN_DRAG) {
      log.info('down drag to disabled');
      this.state = this.DISABLED;
      this.stopDragging(event);
    }
  },

  isDraggingThis : function(model) {
    return (this.state === this.DOWN_DRAG || this.state === this.UP_DRAG) &&
           this.model.id === model.id;
  },

  startDragging : function(event) {
    this.$el.css('display', 'block');
    this.updatePos(event);

    this.trigger('dragstart');
  },

  stopDragging : function(event) {
    log.info('DROP');

    this.state = this.DISABLED;
    this.$el.css('display', 'none');
    var model = this.model;
    this.model = undefined;
    this.trigger('drop', PointFromEvent(event), model);
  },

  updateBodySize : function() {
    this.bodySize = new Point(window.innerWidth, window.innerHeight);
  },

  updatePos : function(event) {
    var left = event.pageX - 37;
    var top = event.pageY - 37;

    if (left < 0) {
      left = 0;
    }
    if (left + 77 > this.bodySize.x) {
      left = this.bodySize.x - 77;
    }
    if (top < 0) {
      top = 0;
    }
    if (top + 77 > this.bodySize.y) {
      top = this.bodySize.y - 77;
    }

    this.$el.css({top : top, left : left});
  },

  remove : function() {
    Backbone.View.prototype.remove.call(this);
    $('body').off('mousedown ' + this.uid, this.onMousedown.bind(this));
    $('body').off('mousemove ' + this.uid, this.onMousemove.bind(this));
    $('body').off('mouseup ' + this.uid, this.onMouseup.bind(this));
  }
});

var ItemSlot = Backbone.View.extend({
  tagName : 'div',
  className : 'itemSlot',
  template : _.template($('#item-slot-template').html()),

  events : {
    'mousedown' : 'onMousedown',
    'mouseenter' : 'onMouseenter',
    'mouseleave' : 'onMouseleave'
  },

  onMousedown : function(event) {
    if (this.model) {
      this.model.isNew = false;
      this.dragHandler.onViewMousedown(event, this.model);
      this.render();
    }
  },

  onMouseenter : function() {
    gl.UIEvents.trigger('itemSlotMouseenter', this);
    this.trigger('hovering', this);
  },

  onMouseleave : function() {
    if (this.model && this.model.isNew) {
      // log.error('removing is new from model %s', this.model.name);
      this.model.isNew = false;
      this.render();
      gl.DirtyQueue.mark('removeNew');
    }
    this.trigger('hovering');
    gl.UIEvents.trigger('itemSlotMouseleave');
  },

  initialize : function(options, dragHandler, slot, equipper) {
    this.dragHandler = dragHandler;

    if (slot !== undefined) {
      this.slot = slot;
      this.equipper = equipper;
      this.listenTo(this.dragHandler, 'drop', this.onDrop);
    } else {
      this.slot = undefined;
    }
    this.listenTo(this.dragHandler, 'dragstart', this.render);

    this.listenTo(gl.UIEvents, 'itemSlotMouseenter', this.onOtherMouseenter);
    this.listenTo(gl.UIEvents, 'itemSlotMouseleave', this.onOtherMouseleave);
    this.listenTo(gl.DirtyListener, 'newChange', this.showIsNew);
    this.render();
  },

  onOtherMouseenter : function(hoveredSlot) {
    if (hoveredSlot.slot === undefined && this.slot !== undefined) {
      if (this.slot === hoveredSlot.model.slot ||
          (typeof (this.slot) === 'number' &&
           hoveredSlot.model.itemType === 'skill')) {
        this.yellow = true;
        this.$el.addClass('yellow');
      }
    }
  },

  onOtherMouseleave : function() {
    this.yellow = false;
    this.$el.removeClass('yellow');
  },

  dropSuccess : function(dropPos) {
    var off = this.$el.offset();
    var pos = new Point(off.left, off.top);
    var diff = dropPos.sub(pos);

    return diff.x >= 0 && diff.x <= 73 && diff.y >= 0 && diff.y <= 73;
  },

  onDrop : function(dropPos, model) {
    if (this.dropSuccess(dropPos)) {
      this.equipper.equip(model, this.slot);
    }
    // TODO is this here only so that the model that is being dragged (and
    // hidden) can be shown again?
    //   if so, is a full tab re-render necessary?
    gl.DirtyQueue.mark('itemTab');
  },

  // Overwritten by CTItemSlot as logic is different
  showIsNew : function() {
    if (this.model && this.model.isNew) {
      this.$el.addClass('new');
    } else {
      this.$el.removeClass('new');
    }
  },

  render : function() {
    this.$el.html(this.template(Object.assign({}, utils, this)));
    if (this.model && this.model.disabled) {
      this.$el.addClass('red');
    }
    if (this.model && this.dragHandler.isDraggingThis(this.model)) {
      this.$el.addClass('dragging');
    }
    this.showIsNew();
    return this;
  }
});

// CardTab ItemSlot
var CTItemSlot = ItemSlot.extend({
  onMousedown : function(event) {
    if (this.model) {
      if (this.model.itemType === 'card') {
        this.dragHandler.onViewMousedown(event, this.model);
        this.render();
      } else {
        this.trigger('gearMousedown', this);
      }
    }
  },

  select : function() {
    this.selected = true;
    this.$el.addClass('selected');
  },
  unselect : function() {
    this.selected = false;
    this.$el.removeClass('selected');
  },

  onDrop : function(dropPos, model) {
    if (this.dropSuccess(dropPos) && this.equipper) {
      this.equipper.equipCard(model, this.slot);
    }
    // TODO is this here only so that the model that is being dragged (and
    // hidden) can be shown again?
    //   if so, is a full tab re-render necessary?
    gl.DirtyQueue.mark('cardTab');
  },

  showIsNew : function() {
    var isNew = false;
    if (this.model) {
      if (this.model.itemType === 'card') {
        isNew = this.model.isNew;
      } else {
        isNew = this.model.hasNewCards; // Set by NewStateManager in inventory
      }
    }
    if (isNew) {
      this.$el.addClass('new');
    } else {
      this.$el.removeClass('new');
    }
  },

  render : function() {
    ItemSlot.prototype.render.call(this);
    if (this.selected) {
      this.$el.addClass('selected');
    }
  }
});

var ItemTab = Backbone.View.extend({
  tagName : 'div',
  className : 'itemTab',
  template : _.template($('#item-tab-template').html()),

  events : {
    'mouseleave' : 'onMouseleave',
  },

  onMouseleave : function() { gl.UIEvents.trigger('itemSlotMouseleave'); },

  initialize : function(options, game) {
    this.equipped = game.hero.equipped;
    this.skillchain = game.hero.skillchain;
    this.inventory = game.inv;

    this.hovering = undefined;
    this.renderedOnce = false;

    this.allViews = [];
    this.dragHandler = new DragHandler(); // passed to itemSlots, used to
                                          // detect unequip drops
    this.listenTo(this.dragHandler, 'drop', this.onDrop);

    this.fb1 = new WeaponTypeFilterBarView({}, [ 'All', 'Ml', 'Ra', 'Sp' ], [
                 undefined, 'melee', 'range', 'spell'
               ]).render();
    this.fb2 =
        new SlotTypeFilterBarView(
            {}, [ 'All', 'We', 'He', 'Ha', 'Ch', 'Lg', 'Sk' ],
            [ undefined, 'weapon', 'head', 'hands', 'chest', 'legs', 'skill' ])
            .render();

    // Map dirty queue events to itemTab update
    gl.DirtyQueue.mapMark(
        [
          'item:new', 'recycleChange', 'computeAttrs', 'skillComputeAttrs',
          'filterChange'
        ],
        'itemTab');
    // render on itemTab dirty
    this.listenTo(gl.DirtyListener, 'itemTab', this.render);

    this.throttler =
        new Throttler(function() { gl.DirtyQueue.mark('itemTab'); }, 1000);
    this.listenTo(gl.DirtyListener, 'hero:xp', this.throttler.throttled);

    this.tvm = new TabVisibilityManager(
        'inv', this.$el, this.render.bind(this), 'footer:buttons:inv',
        'footer:buttons:craft', 'footer:buttons:cards',
        'footer:buttons:recycle', 'footer:buttons:build');

    this.resize();
    this.listenTo(gl.DirtyListener, 'throttledResize', this.resize);
  },

  resize : function() {
    var size = new Point(window.innerWidth, window.innerHeight - FOOTER_HEIGHT);
    this.$el.css('height', size.y);
    this.holderHeight = size.y;
    this.$('.holder').css('height', size.y);
  },

  onDrop : function(dropPos, model) {
    // log.warning('item tab onDrop');
    var off = this.$('.unequipped').offset();
    if (model.equipped && dropPos.x >= off.left && dropPos.y >= off.top) {
      if (model.itemType === 'skill') {
        // unequip a skill, must find out what slot it was in
        var slot = this.skillchain.getSkillSlot(model);
        this.skillchain.equip(undefined, slot);
      } else {
        this.equipped.equip(undefined, model.slot);
      }
      // log.warning('item tab onDrop unequipping');
    }
  },

  onHover : function(itemSlot) { this.hovering = itemSlot; },

  newItemSlot : function(model, slot, parent) {
    var view = new ItemSlot({model : model}, this.dragHandler, slot, parent);
    this.listenTo(view, 'hovering', this.onHover);
    this.allViews.push(view);
    return view;
  },

  render : function() {
    if (!this.tvm.visible) {
      return this;
    }

    if (!this.renderedOnce) {
      this.$el.html(this.template(utils));
      this.$('.filters').append(this.fb1.el);
      this.$('.filters').append(this.fb2.el);
      this.$equipped = this.$('.equipped');
      this.$skillchain = this.$('.skillchain');
      this.$unequipped = this.$('.unequipped');
      this.renderedOnce = true;
      this.resize();
    }

    // properly remove all views
    _.each(this.allViews, function(view) {
      this.stopListening(view);
      view.remove();
    }, this);
    this.allViews = [];

    _.each(this.equipped.slots, function(slot) {
      var view = this.newItemSlot(this.equipped[slot], slot, this.equipped);
      this.$equipped.append(view.el);
    }, this);

    _.each(this.skillchain.skills, function(skill, i) {
      var view = this.newItemSlot(skill, i, this.skillchain);
      this.$skillchain.append(view.el);
    }, this);

    var items = _.where(this.inventory.getModels(), {equipped : false});
    items = this.fb1.filter(items);
    items = this.fb2.filter(items);
    _.each(items, function(model) {
      var view = this.newItemSlot(model);
      this.$unequipped.append(view.el);
    }, this);

    if (this.hovering && this.hovering.model) {
      this.hovering = _.find(this.allViews, function(view) {
        return view.model && this.hovering.model.id === view.model.id;
      }, this);
      if (this.hovering) {
        this.hovering.onMouseenter();
      }
    }

    return this;
  },
});

var CardTab = Backbone.View.extend({
  tagName : 'div',
  className : 'itemTab',
  template : _.template($('#card-tab-template').html()),

  events : {'mouseleave' : 'onMouseleave'},

  onMouseleave : function() { gl.UIEvents.trigger('itemSlotMouseleave'); },

  initialize : function(options, game) {
    this.equipped = game.hero.equipped;
    this.skillchain = game.hero.skillchain;
    this.cardInv = game.cardInv;

    this.renderedOnce = false;

    this.hovering = undefined;
    this.selected = undefined;

    this.allViews = [];
    this.dragHandler = new DragHandler(this.onBodyMousedown.bind(this));
    this.listenTo(this.dragHandler, 'drop', this.onDrop);

    // Map dirty queue events to itemTab update
    gl.DirtyQueue.mapMark(
        [
          'card:new', 'card:levelup', 'recycleChange', 'computeAttrs',
          'skillComputeAttrs'
        ],
        'cardTab');
    gl.DirtyQueue.mapMark([ 'equipChange' ], 'hardRenderCardTab');

    this.listenTo(gl.DirtyListener, 'cardTab', this.render);
    this.listenTo(gl.DirtyListener, 'hardRenderCardTab', this.hardRender);

    this.throttler =
        new Throttler(function() { gl.DirtyQueue.mark('cardTab'); }, 1000);
    this.listenTo(gl.DirtyListener, 'material:new', this.throttler.throttled);
    this.listenTo(gl.DirtyListener, 'hero:xp', this.throttler.throttled);

    this.tvm = new TabVisibilityManager(
        'cards', this.$el, this.render.bind(this), 'footer:buttons:cards',
        'footer:buttons:craft', 'footer:buttons:inv', 'footer:buttons:recycle',
        'footer:buttons:build');

    var unselect = (function() { this.selected = undefined; }).bind(this);

    this.listenTo(gl.UIEvents, 'footer:buttons:cards', unselect);
    this.listenTo(gl.UIEvents, 'footer:buttons:inv', unselect);

    this.resize();
    this.listenTo(gl.DirtyListener, 'throttledResize', this.resize);
  },

  onDrop : function(dropPos, cardModel) {
    if (cardModel.equipped) {
      var off = this.$('.unequipped').offset();
      if (dropPos.x < off.left ||
          (dropPos.x >= off.left && dropPos.y >= off.top &&
           dropPos.y < window.innerHeight - FOOTER_HEIGHT)) {
        var gear = cardModel.gearModel;
        var slot = gear.getCardSlot(cardModel);
        gear.equipCard(undefined, slot);
      }
    }
  },

  onBodyMousedown : function(event) {
    // if the mousedown is in the body and not on the card tab, deselect the
    // selected gear piece
    if (event.pageX <= this.$el.offset().left) {
      this.hardRender();
    }
  },

  resize : function() {
    var size = new Point(window.innerWidth, window.innerHeight - FOOTER_HEIGHT);
    this.$el.css({left : size.x - 405, height : size.y});
    this.holderHeight = size.y;
    this.$('.holder').css('height', size.y);
  },

  onGearMousedown : function(view) {
    if (this.selected) {
      this.selected.unselect();

      if (this.selected.model.id === view.model.id) {
        this.selected = undefined;
        gl.DirtyQueue.mark('cardTab');
        return;
      }
    }
    this.selected = view;
    this.selected.select();
    gl.DirtyQueue.mark('cardTab');
  },

  onHover : function(hoveredView) { this.hovering = hoveredView; },

  newItemSlot : function(model, slot, equipper) {
    // TODO: fix the args:
    var view =
        new CTItemSlot({model : model}, this.dragHandler, slot, equipper);

    this.listenTo(view, 'gearMousedown', this.onGearMousedown);
    this.listenTo(view, 'hovering', this.onHover);

    this.allViews.push(view);
    return view;
  },

  hardRender : function() {
    this.selected = undefined;
    return this.render();
  },

  render : function() {
    if (!this.tvm.visible) {
      return this;
    }

    if (!this.renderedOnce) {
      this.$el.html(
          this.template(Object.assign({selected : this.selected}, utils)));
      this.$('.holder').css('height', this.holderHeight);
      this.renderedOnce = true;
    }

    if (this.selected) {
      this.$('.equipped-cards')
          .find('.header')
          .html('Equipped ' + presentableSlot(this.selected.model.slot) +
                ' Cards');
      this.$('.unequipped')
          .find('.header')
          .html('Unequipped ' + presentableSlot(this.selected.model.slot) +
                ' Cards');
    } else {
      this.$('.equipped-cards')
          .find('.header')
          .html('Click an item above to equip cards');
      this.$('.unequipped').find('.header').html('All Unequipped Cards');
    }

    // call remove() on all views, and stopListening on all views
    _.each(this.allViews, function(view) {
      this.stopListening(view);
      view.remove();
    }, this);
    this.allViews = [];

    var frag = document.createDocumentFragment();
    _.each(this.equipped.slots, function(slot) {
      // if model, can select, cannot unequip, is not card
      var view = this.newItemSlot(this.equipped[slot], slot);
      frag.appendChild(view.el);
    }, this);
    this.$('.equipped').append(frag);

    frag = document.createDocumentFragment();
    _.each(this.skillchain.skills, function(skill, i) {
      // if model, can select, cannot unequip, is not card
      var view = this.newItemSlot(skill, i);
      frag.appendChild(view.el);
    }, this);
    this.$('.skillchain').append(frag);

    // If item selected, show item's cards
    var cards;
    if (this.selected) {
      frag = document.createDocumentFragment();
      _.each(this.selected.model.cards, function(card, slot) {
        // cannot select, if model can unequip, is card
        var view = this.newItemSlot(card, slot, this.selected.model);
        frag.appendChild(view.el);
      }, this);
      this.$('.equipped-cards').append(frag);

      var slot = typeof (this.selected.slot) === 'number' ? 'skill'
                                                          : this.selected.slot;
      cards = _.where(this.cardInv.getModels(),
                      {slot : slot}); // get filtered cards
    } else {
      cards = this.cardInv.getModels(); // get all cards
    }
    cards = _.filter(cards, function(card) { return !card.equipped; });

    frag = document.createDocumentFragment();
    _.each(cards, function(card) {
      // no slot, no parent, can select, cannot unequip, is card
      var view = this.newItemSlot(card, card.slot);
      frag.appendChild(view.el);
    }, this);
    this.$('.unequipped').append(frag);

    // selected slot is an ItemSlot holding a equippedGear or skill model
    if (this.selected) {
      var selectedView = _.find(this.allViews, function(view) {
        return view.model && this.selected.model.id === view.model.id;
      }, this);
      if (selectedView && selectedView.model.equipped) {
        selectedView.select();
        this.selected = selectedView;
      } else {
        this.selected = undefined;
      }
    }
    if (this.hovering && this.hovering.model) {
      this.hovering = _.find(this.allViews, function(view) {
        return view.model && this.hovering.model.id === view.model.id;
      }, this);
      if (this.hovering) {
        this.hovering.onMouseenter();
      }
    }

    return this;
  },
});

var FocusedTabSlot = Backbone.View.extend({
  tagName : 'div',
  className : 'itemSlot',
  template : _.template($('#focused-tab-item-slot-template').html()),
  events : {
    'mousedown' : 'onMousedown',
    'mouseenter' : 'onMouseenter',
    'mouseleave' : 'onMouseleave'
  },
  onMousedown : function(event) { this.parent.onChildMousedown(this); },
  select : function() {
    this.selected = true;
    this.render();
  },
  unselect : function() {
    this.selected = false;
    this.render();
  },
  onMouseenter :
      function() { gl.UIEvents.trigger('itemSlotMouseenter', this); },
  onMouseleave : function() { gl.UIEvents.trigger('itemSlotMouseleave'); },
  initialize : function(options, parent) { this.parent = parent; },
  render : function() {
    this.$el.html(this.template(Object.assign({}, utils, this)));
    if (this.selected) {
      this.$el.addClass('selected');
    } else {
      this.$el.removeClass('selected');
    }
    return this;
  },
});

var FocusedArea = Backbone.View.extend({
  tagName : 'div',
  className : 'focused-area',

  select : function(model) {
    this.model = model;
    this.render();
  },

  unselect : function() {
    this.model = undefined;
    this.render();
  },

  render : function() {
    this.$el.html(this.template(Object.assign({}, this, utils)));
    return this;
  },
});

var CraftFocusedArea = FocusedArea.extend({
  template : _.template($('#craft-tab-focus-area-template').html()),

  events : {'mousedown .upgrade' : 'onUpgrade'},

  onUpgrade : function() {
    if (this.matInv.tryLevelCard(this.model, 1)) {
      this.render();
    }
    log.error('Upgraded %s to level %d', this.model.name, this.model.level);
    this.cardInv.sort('craft');
    gl.DirtyQueue.mark('card:levelup');
    gl.EquipEvents.trigger('change');
  },

  initialize : function(options, matInv, cardInv) {
    this.matInv = matInv;
    this.cardInv = cardInv;
    this.ref = ref;
    this.listenTo(gl.DirtyListener, 'material:new', this.render);
  },
});

var RecycleFocusedArea = FocusedArea.extend({
  template : _.template($('#recycle-tab-focus-area-template').html()),

  initialize : function(options, matInv, recycleManager) {
    this.matInv = matInv;
    this.recycleManager = recycleManager;
  }
});

var FocusedTab = Backbone.View.extend({
  tagName : 'div',
  className : 'itemTab',
  template : _.template($('#focused-tab-template').html()),
  FocusedAreaClass : FocusedArea,

  events : {'mouseleave' : 'onMouseleave'},

  onMouseleave : function() { gl.UIEvents.trigger('itemSlotMouseleave'); },

  onChildMousedown : function(view, force) {
    if (this.selected) {
      this.selected.unselect();
      this.focusedAreaView.unselect();
    }
    // selected needs to remain here

    if (this.selected && this.selected.model.id === view.model.id && !force) {
      this.selected = undefined;
    } else {
      this.selected = view;
      this.selected.select();
      this.focusedAreaView.select(view.model);
    }
  },

  tryUnselect : function() {
    if (this.selected) {
      this.selected.unselect();
      this.focusedAreaView.unselect();
      this.selected = undefined;
    }
  },

  initialize : function(options, game) {
    this.renderedOnce = false;
    this.selected = undefined;

    this.allViews = [];

    this.listenTo(gl.DirtyListener, 'throttledResize', this.resize);
  },

  resize : function() {
    var size = new Point(window.innerWidth, window.innerHeight - FOOTER_HEIGHT);
    this.$el.css('height', size.y);
    this.holderHeight = size.y;
    this.$('.holder').css('height', size.y);
  },

  getItems : function() { throw ('This is an abstract class'); },

  render : function() {
    if (!this.tvm.visible) {
      return this;
    }

    if (!this.renderedOnce) {
      this.$el.html(this.template(utils));
      this.$('.focused-area').html(this.focusedAreaView.render().el);
      this.$inv = this.$('.inventory');

      var $filters = this.$('.filters');
      _.each(this.fbs, function(fb) { $filters.append(fb.el); });
      this.resize();
      this.renderedOnce = true;
    }

    _.invoke(this.allViews, 'remove');
    this.allViews = [];

    var items = this.getItems();

    var frag = document.createDocumentFragment();
    _.each(items, function(model) {
      var view = new FocusedTabSlot({model : model}, this);
      this.allViews.push(view);
      if (this.selected && this.selected.model.id === model.id) {
        this.selected = view;
        view.selected = true;
      }
      frag.appendChild(view.render().el);
    }, this);

    this.$inv.append(frag);
    this.focusedAreaView.render();

    return this;
  },
});

var CraftTab = FocusedTab.extend({
  initialize : function(options, game) {
    FocusedTab.prototype.initialize.call(this, options, game);

    this.cardInv = game.cardInv;
    this.matInv = game.matInv;

    this.fbs = [
      new EquippedFilterBarView({}, [ 'All', 'Eq', 'Ueq' ],
                                [ undefined, true, false ])
          .render(),
      new SlotTypeFilterBarView(
          {}, [ 'All', 'We', 'He', 'Ha', 'Ch', 'Lg', 'Sk' ],
          [ undefined, 'weapon', 'head', 'hands', 'chest', 'legs', 'skill' ])
          .render()
    ];

    gl.DirtyQueue.mapMark([ 'filterChange', 'card' ], 'craftTab');
    this.listenTo(gl.DirtyListener, 'craftTab', this.render);

    this.throttler =
        new Throttler(function() { gl.DirtyQueue.mark('sortCraftTab'); }, 1000);
    this.listenTo(gl.DirtyListener, 'material:new', this.throttler.throttled);
    this.listenTo(gl.DirtyListener, 'sortCraftTab', this.sortRender);

    this.tvm = new TabVisibilityManager(
        'craft', this.$el, this.render.bind(this), 'footer:buttons:craft',
        'footer:buttons:inv', 'footer:buttons:cards', 'footer:buttons:recycle',
        'footer:buttons:build');

    this.focusedAreaView = new CraftFocusedArea({}, this.matInv, this.cardInv);
  },

  sortRender : function() {
    var sortStyle = this.tvm.visible ? 'craft' : 'card';
    this.cardInv.sort(sortStyle);
    this.render();
  },

  getItems : function() {
    var items = this.fbs[0].filter(this.cardInv.getModels());
    items = this.fbs[1].filter(items);
    return items;
  },

  forceFocus : function(model) {
    var view =
        _.find(this.allViews,
               function(view) { return view.model.id === model.id; }, this);

    // force a mousedown
    this.onChildMousedown(view, true);
  },

  forceDeselect : function(model) {
    if (this.selected && this.selected.model.id === model.id) {
      this.selected.unselect();
      this.focusedAreaView.unselect();
      this.selected = undefined;
    }
  },
});

var RecycleTab = FocusedTab.extend({
  events : {
    'click .recycle-restore-button' : 'onRestore',
    'click .recycle-this-button' : 'onRecycle',
    'click .recycle-all-button' : 'onRecycleAll'
  },

  onRestore : function() {
    log.warning('restore');
    if (this.selected) {
      this.selected.model.inRecycle = false;
      this.selected.unselect();
      this.focusedAreaView.unselect();
      this.selected = undefined;

      gl.DirtyQueue.mark('recycleChange');
    }
  },

  onAnyRecycle : function(matSlots) {
    // Handle the unselecting of focused item, trigger change events
    if (this.selected) {
      this.selected.unselect();
      this.focusedAreaView.unselect();
      this.selected = undefined;
    }

    var drops = [];
    _.each(matSlots,
           function(
               slot) { drops.push(this.recycleManager.getRecycleValue(slot)); },
           this);

    this.matInv.addDrops(drops);

    gl.DirtyQueue.mark('recycleChange');
  },

  onRecycle : function() {
    log.warning('recycle');
    if (this.selected) {
      var slot = this.selected.model.recycle();
      this.onAnyRecycle([ slot ]);
    }
  },

  onRecycleAll : function() {
    log.warning('recycle all');
    var slots = [];
    _.each(this.allViews, function(view) { slots.push(view.model.recycle()); });
    this.onAnyRecycle(slots);
  },

  initialize : function(options, game) {
    FocusedTab.prototype.initialize.call(this, options, game);

    this.inv = game.inv;
    this.cardInv = game.cardInv;
    this.matInv = game.matInv;
    this.recycleManager = game.recycleManager;

    this.fbs = [
      new SlotTypeFilterBarView(
          {}, [ 'All', 'We', 'He', 'Ha', 'Ch', 'Lg', 'Sk' ],
          [ undefined, 'weapon', 'head', 'hands', 'chest', 'legs', 'skill' ])
          .render()
    ];

    gl.DirtyQueue.mapMark(
        [
          'item:new', 'card:new', 'filterChange', 'recycleChange',
          'zone:unlocked'
        ],
        'recycleTab');
    this.listenTo(gl.DirtyListener, 'recycleTab', this.render);

    this.tvm = new TabVisibilityManager(
        'recycle', this.$el, this.render.bind(this), 'footer:buttons:recycle',
        'footer:buttons:inv', 'footer:buttons:cards', 'footer:buttons:craft',
        'footer:buttons:build');

    this.focusedAreaView =
        new RecycleFocusedArea({}, this.matInv, this.recycleManager);
  },

  getItems : function() {
    var items = this.recycleManager.getModels();
    items = this.fbs[0].filter(items);
    return items;
  },
});

var ZoneMapTab = Backbone.View.extend({
  tagName : 'div',
  className : 'zone noselect',
  template : _.template($('#zone-map-tab-template').html()),

  events : {
    'mousedown' : 'onClick',
  },

  onClick : function() { this.trigger('click', this.model.zoneNum); },

  render : function() {
    if (this.model.running) {
      this.$el.addClass('running');
    }
    this.$el.html(this.template(Object.assign({}, utils, this.model)));
    return this;
  },
});

var MapTab = Backbone.View.extend({
  tagName : 'div',
  className : 'map',

  events : {
    'click #autoAdvance' : 'toggleAutoAdvance',
  },

  initialize : function(options, game) {
    this.zone = game.zone;
    this.settings = game.settings;

    this.tvm = new TabVisibilityManager(
        'map', this.$el, this.render.bind(this), 'footer:buttons:map',
        'footer:buttons:stats', 'footer:buttons:help', 'footer:buttons:config',
        'footer:buttons:account');

    this.$el.html($('#map-tab-template').html());
    this.$holder = this.$('.holder');

    this.resize();
    this.listenTo(gl.DirtyListener, 'throttledResize', this.resize);
    this.listenTo(gl.DirtyListener, 'zone:unlocked', this.render);
    this.listenTo(gl.DirtyListener, 'zone:start', this.render);
  },

  resize : function() {
    this.$el.css({height : window.innerHeight - FOOTER_HEIGHT});
    this.$holder.css({height : window.innerHeight - FOOTER_HEIGHT});
  },

  toggleAutoAdvance : function() {
    this.settings['autoAdvance'] = this.$('#autoAdvance').prop('checked');
  },

  zoneClick : function(zoneName) {
    log.UI('MapTab: Clicked on zone: %s', zoneName);
    this.zone.nextZone = zoneName;
    this.zone.newZone(zoneName);
    this.render();
  },

  render : function() {
    if (!this.tvm.visible) {
      return this;
    }
    _.each(this.subs, function(sub) {
      sub.remove();
      this.stopListening(sub);
    }, this);
    this.subs = [];

    var frag = document.createDocumentFragment();
    var preTag = document.createElement('div');
    preTag.innerHTML =
        '<p><input type="checkbox" id="autoAdvance" /> Auto-advance on zone clear</p>';
    frag.appendChild(preTag);
    var data, sub, name, zoneRef;

    var len = this.zone.unlockedZones + 1;
    for (var i = len - 1; i >= 0; i--) {
      var currentZone = this.zone.getZoneFromNum(i);

      var zoneCount = this.zone.zoneOrder.length;
      var upgradeCount = currentZone.upgradeCount;
      var zoneI = currentZone.zoneI;
      var level = Math.max(1, i * 5); // 5 is constant for Zone spacing

      var name = currentZone.name;
      var zoneRef = this.zone.allZones[name];
      var nameStr = currentZone.nameStr;
      data = _.extend({
        name : nameStr,
        level : level,
        running : i === this.zone.nextZone,
        zoneNum : i
      },
                      zoneRef);
      sub = new ZoneMapTab({model : data});
      this.listenTo(sub, 'click', this.zoneClick);
      this.subs.push(sub);
      frag.appendChild(sub.render().el);
    }

    this.$holder.html(frag);
    $('#autoAdvance').prop('checked', this.settings.autoAdvance);
    return this;
  }
});

var ConfigTab = Backbone.View.extend({
  tagName : 'div',
  className : 'config',
  template : _.template($('#config-template').html()),

  events : {
    'click #wipebutton' : 'wipe',
    'click #namebutton' : 'nameButton',
    'click #devbutton' : 'devButton',
    'click #donateButton' : 'donate',
    'click #enableBuildHotkeys' : 'toggleEnableBuildHotkeys',
    'click #autoCraft' : 'toggleEnableAutoCraft',
    'click #pauseOnDeath' : 'pauseOnDeath',
    'click #enableHeroDmgMsgs' : 'enableHeroDmgMsgs',
    'click #enableMonDmgMsgs' : 'enableMonDmgMsgs',
    'click #enableMatMsgs' : 'enableMatMsgs',
    'click #backOnDeath' : 'backOnDeath',
    'click #bossPause' : 'bossPause',
    'change #moveAngle' : 'moveAngle',
    'change #zonesBack' : 'zonesBack',
    'click #disable-shake' : 'disableShake'
  },

  initialize : function(options, game) {
    this.zone = game.zone;
    this.settings = game.settings;
    this.hero = game.hero;

    this.tvm = new TabVisibilityManager(
        'config', this.$el, this.render.bind(this), 'footer:buttons:config',
        'footer:buttons:map', 'footer:buttons:help', 'footer:buttons:stats',
        'footer:buttons:account');

    this.$el.html('<div class="holder"></div>');
    this.$holder = this.$('.holder');

    this.resize();
    this.listenTo(gl.DirtyListener, 'throttledResize', this.resize);

    // Close Checkout on page navigation
    $(window).on('popstate', function() {
      if (this.handler) {
        this.handler.close();
      }
    });
  },

  resize : function() {
    this.$el.css({height : window.innerHeight - FOOTER_HEIGHT});
    this.$holder.css({height : window.innerHeight - FOOTER_HEIGHT});
  },

  render : function() {
    if (!this.tvm.visible) {
      return this;
    }
    this.$holder.html(this.template(Object.assign({}, utils, this)));
    $('#enableBuildHotkeys').prop('checked', !!gl.settings.enableBuildHotkeys);
    $('#disable-shake').prop('checked', !!gl.settings.disableShake);
    $('#autoCraft').prop('checked', !!gl.settings.autoCraft);
    $('#pauseOnDeath').prop('checked', !!gl.settings.pauseOnDeath);
    $('#enableHeroDmgMsgs').prop('checked', !!gl.settings.enableHeroDmgMsgs);
    $('#enableMonDmgMsgs').prop('checked', !!gl.settings.enableMonDmgMsgs);
    $('#enableMatMsgs').prop('checked', !!gl.settings.enableMatMsgs);
    $('#backOnDeath').prop('checked', !!gl.settings.backOnDeath);
    $('#bossPause').prop('checked', !!gl.settings.bossPause);
    $('#moveAngle').val(this.hero.moveAngle ? this.hero.moveAngle : 0);
    $('#zonesBack').val(gl.settings.zonesBack);
    return this;
  },

  donate : function(e) {
    var amount = Math.round(parseFloat($('#donationamount').val()) * 100);
    amount = Math.max(100, amount);
    log.donateAttempt(amount);
    this.handler = StripeCheckout.configure({
      key : 'pk_live_Udj2pXdBbHxWllQWuAzempnY',
      bitcoin : true,
      token : function(token) { return log.handleDonationToken(token, amount); }
    });
    this.handler.open({
      name : 'DungeonsOfDerp',
      description : 'Donate towards development',
      amount : amount
    });
    e.preventDefault();
  },

  toggleEnableBuildHotkeys : function() {
    gl.settings['enableBuildHotkeys'] =
        this.$('#enableBuildHotkeys').prop('checked');
  },

  toggleEnableAutoCraft : function() {
    gl.settings['autoCraft'] = this.$('#autoCraft').prop('checked');
  },

  pauseOnDeath : function() {
    gl.settings['pauseOnDeath'] = this.$('#pauseOnDeath').prop('checked');
  },

  enableHeroDmgMsgs : function() {
    gl.settings['enableHeroDmgMsgs'] =
        this.$('#enableHeroDmgMsgs').prop('checked');
  },

  enableMonDmgMsgs : function() {
    gl.settings['enableMonDmgMsgs'] =
        this.$('#enableMonDmgMsgs').prop('checked');
  },

  enableMatMsgs : function() {
    gl.settings['enableMatMsgs'] = this.$('#enableMatMsgs').prop('checked');
  },

  backOnDeath : function() {
    gl.settings['backOnDeath'] = this.$('#backOnDeath').prop('checked');
  },

  bossPause : function() {
    gl.settings['bossPause'] = this.$('#bossPause').prop('checked');
  },

  moveAngle : function() { gl.setMoveAngle(this.$('#moveAngle').val()); },

  zonesBack : function() {
    if (isNaN(parseInt(this.$('#zonesBack').val())) ||
        parseInt(this.$('#zonesBack').val()) < 0) {
      return;
    }
    gl.settings['zonesBack'] = parseInt(this.$('#zonesBack').val());
  },

  wipe : function() {
    log.error('Wiping save and creating new char with version: %s',
              gl.VERSION_NUMBER);
    log.reportNewBuild();
    localStorage.removeItem('data');
    location.reload();
  },

  nameButton : function() {
    var userInput = $('#charname').val();
    this.hero.name = userInput.length < 64 ? userInput : 'SMARTASS';
    gl.DirtyQueue.mark('rename');
  },

  devButton : function() {
    var msg = $('#devmsg').val();
    log.feedback(this.hero.name + ' says: ' + msg);
    $('#devmsg').val('');
  },

  disableShake : function() {
    gl.settings.disableShake = $('#disable-shake').prop('checked');
  }
});

var HelpTab = Backbone.View.extend({
  tagName : 'div',
  className : 'help',

  initialize : function(options, game) {
    this.template = _.template($('#help-template').html()),
    this.zone = game.zone;

    this.tvm = new TabVisibilityManager(
        'help', this.$el, this.render.bind(this), 'footer:buttons:help',
        'footer:buttons:map', 'footer:buttons:config', 'footer:buttons:stats',
        'footer:buttons:account');

    this.$el.html('<div class="holder"></div>');
    this.$holder = this.$('.holder');

    this.resize();
    this.listenTo(gl.DirtyListener, 'throttledResize', this.resize);
  },

  resize : function() {
    this.$el.css({height : window.innerHeight - FOOTER_HEIGHT});
    this.$holder.css({height : window.innerHeight - FOOTER_HEIGHT});
  },

  render : function() {
    if (!this.tvm.visible) {
      return this;
    }
    this.$holder.html(this.template(Object.assign({gl : gl}, utils)));
    return this;
  }
});

var BuildTab = Backbone.View.extend({
  tagName : 'div',
  className : 'build',
  template : _.template($('#build-template').html()),

  initialize : function(options, game) {
    this.zone = game.zone;

    this.tvm = new TabVisibilityManager(
        'build', this.$el, this.render.bind(this), 'footer:buttons:build',
        'footer:buttons:recycle', 'footer:buttons:inv', 'footer:buttons:cards',
        'footer:buttons:craft');

    this.$el.html('<div class="holder"></div>');
    this.$holder = this.$('.holder');

    this.resize();
    this.listenTo(gl.DirtyListener, 'throttledResize', this.resize);
    this.listenTo(gl.UIEvents, 'buildsave', this.render);
    this.listenTo(gl.UIEvents, 'buildload', this.render);
  },

  resize : function() {
    this.$el.css({height : window.innerHeight - FOOTER_HEIGHT});
    this.$holder.css({height : window.innerHeight - FOOTER_HEIGHT});
  },

  render : function() {
    if (!this.tvm.visible) {
      return this;
    }

    let buildNames = [];
    for (let i = 0; i < 21; i++) {
      let build = gl.builds[i];
      if (!build) {
        buildNames[i] = 'Empty';
        continue;
      }
      buildNames[i] = !!build.name ? build.name : buildNames[i] = 'Build ' + i;
    }

    this.$holder.html(this.template(Object.assign({
      lastBuildLoaded : gl.lastBuildLoaded,
      buildNames : buildNames,
    },
                                                  utils)));

    for (var i = 0; i < 21; i++) {
      this.$holder.find('#load-build-' + i)
          .on('click', gl.loadBuild.bind(null, i));
      this.$holder.find('#save-build-' + i)
          .on('click', gl.saveBuild.bind(null, i));
    }
    this.$holder.find('#rename-build-button').on('click', gl.renameBuild);
    return this;
  }
});

var AccountTab = Backbone.View.extend({
  tagName : 'div',
  className : 'account',

  events : {
    'click #signInSubmit' : 'signInSubmit',
    'click #newAccSubmit' : 'newAccSubmit',
    'click #fbFullSave' : 'fbFullSave',
    'click #fbFullLoad' : 'fbFullLoad',
    'click #signOut' : 'signOut',
    'click #prestige' : 'prestige',
    'click .spendPrestige' : 'spendPrestige',
  },

  initialize : function(options, game) {
    this.template = _.template($('#account-template').html());
    this.zone = game.zone;
    this.game = game;
    this.tvm = new TabVisibilityManager(
        'account', this.$el, this.render.bind(this), 'footer:buttons:account',
        'footer:buttons:map', 'footer:buttons:config', 'footer:buttons:stats',
        'footer:buttons:help');

    this.$el.html('<div class="holder"></div>');
    this.$holder = this.$('.holder');

    this.prestigeStats = [
      'strength',   'dexterity',   'wisdom',     'vitality',     'maxHp',
      'maxMana',    'armor',       'dodge',      'eleResistAll', 'fireResist',
      'coldResist', 'lightResist', 'poisResist', 'meleeDmg',     'rangeDmg',
      'spellDmg',   'physDmg',     'fireDmg',    'coldDmg',      'lightDmg',
      'poisDmg',    'accuracy'
    ];
    this.remainingPrestige;
    if (this.game.hero.prestige === undefined) {
      this.game.hero.prestige = {};
      _.each(this.prestigeStats,
             function(stat) { this.game.hero.prestige[stat] = 0; }, this);
    } else {
      _.each(this.prestigeStats, function(stat) {
        if (this.game.hero.prestige[stat] === undefined) {
          this.game.hero.prestige[stat] = 0;
        }
      }, this);
    }

    this.resize();
    this.listenTo(gl.DirtyListener, 'throttledResize', this.resize);
    this.listenTo(gl.DirtyListener, 'hero:xp', this.render);
  },

  signInSubmit : function() {
    // alert('sign in');
    gl.FB.authWithPassword({
      email : $('#signInUsername').val(),
      password : $('#signInPassword').val()
    },
                           function(error, authData) {
                             console.log('err', error);
                             if (error !== undefined && error !== null) {
                               alert(error.message);
                             }
                             console.log('a', authData);
                             if (authData !== undefined) {
                               gl.accountId = authData.uid;
                               localStorage.setItem('accountId', authData.uid);
                               this.render();
                             } else {
                               console.log(error);
                             }
                           }.bind(this),
                           {});
  },

  newAccSubmit : function() {
    gl.FB.createUser(
        {
          email : $('#newAccUsername').val(),
          password : $('#newAccPassword').val(),
        },
        function(error, userData) {
          if (error) {
            switch (error.code) {
            case "EMAIL_TAKEN":
              alert(
                  "The new user account cannot be created because the email is already in use.");
              break;
            case "INVALID_EMAIL":
              alert("The specified email is not a valid email.");
              break;
            default:
              alert("Error creating user:", error);
            }
          } else {
            alert("Successfully created user account with uid:" + userData.uid +
                  " - Please sign in");
          }
        });
  },

  fbFullSave : function() { this.game.fbFullSave(); },

  fbFullLoad : function() { this.game.fbFullLoad(); },

  signOut : function() {
    gl.FB.unauth();
    gl.accountId = undefined;
    localStorage.removeItem('accountId');
    this.render();
  },

  resize : function() {
    this.$el.css({height : window.innerHeight - FOOTER_HEIGHT});
    this.$holder.css({height : window.innerHeight - FOOTER_HEIGHT});
  },

  prestige : function() {
    var prestigeAmount = this.lvlPrestigeVal(this.game.hero.level);
    log.error('Prestiging for ' + prestigeAmount + ' points');
    log.prestige(prestigeAmount);
    localStorage.removeItem('data');
    localStorage.setItem('prestigeTotal', prestigeAmount);
    location.reload();
  },

  spendPrestige : function() {
    if (arguments[0].shiftKey) {
      recurse = true;
    }
    var stat = arguments[0].currentTarget.id;
    while (this.game.hero.prestige[stat] !== undefined &&
           this.remainingPrestige > this.game.hero.prestige[stat]) {
      this.game.hero.prestige[stat] += 1;
      this.game.hero.computeAttrs();
      this.render();
      if (recurse !== true) {
        return
      }
    }
  },

  render : function() {
    if (!this.tvm.visible) {
      return this;
    }
    $('#signInUsername').val("bobtony@firebase.com")
    $('#signInPassword').val("correcthorsebatterystaple")

    let prestigeNext = this.lvlPrestigeVal(this.game.hero.level);
    let statSelectors = '';
    let totalAllocatedPoints = 0;
    let statObjs = [];

    _.map(this.prestigeStats, function(stat) {
      let points = this.game.hero.prestige[stat];
      statObjs.push({
        stat : stat,
        pretty : ref.statnames[stat],
        points : points,
        cost : points + 1,
      });
      totalAllocatedPoints += _.range(points + 1).sum();
    }, this);

    let remainingPrestige = this.game.hero.prestigeTotal - totalAllocatedPoints;

    this.$holder.html(this.template(Object.assign({
      gl : gl,
      statObjs : statObjs,
      remainingPrestige : remainingPrestige,
      prestigeNext : prestigeNext,
      prestigeTotal : this.game.hero.prestigeTotal
    },
                                                  utils)));

    return this;
  },

  lvlPrestigeVal : function(lvl) { return Math.pow(Math.max(0, lvl - 100), 2); }
});

/* exports.extend({
 *   GameView : GameView,
 *   StatsTab : StatsTab,
 * });*/
