# Makefile for transpiling with Babel in a Node app, or in a client- or
# server-side shared library.

.PHONY: all clean clean_some user final_scripts all_styles dev

clean_some:
	rm -f public/scripts/*
	rm -f public/styles/*
	mkdir -p public/scripts public/styles lib/styles dist/styles

clean:
	rm -rf lib/*
	rm -f public/scripts/*
	rm -f public/styles/*
	mkdir -p public/scripts public/styles lib/styles dist/styles

# Install `babel-cli` in a project to get the transpiler.
babel := node_modules/.bin/babel
browserify := node_modules/.bin/browserify
uglifyjs := node_modules/.bin/uglifyjs

src_js := $(shell find src/ -name '*.js')
src_jsx := $(shell find src/ -name '*.jsx')

# All output js files.  Change .jsx file extensions to .js because browserify is
# stupid and shits itself when it tries to bundle a .jsx file.
transpiled_js := $(patsubst src/%,lib/%,$(src_js)) $(patsubst src/%.jsx,lib/%.js,$(src_jsx))

#posts := $(shell find public/posts/ -name post.html)

all_js: $(transpiled_js)

# How to get a transpiled js file from a jsx file
lib/%.js: src/%.jsx
	mkdir -p $(dir $@)
	$(babel) --plugins transform-react-jsx $< --out-file $@ --source-maps

# How to get a transpiled js file from a js file
lib/%.js: src/%.js
	mkdir -p $(dir $@)
	$(babel) --plugins transform-react-jsx $< --out-file $@ --source-maps

dist/bundle.js: $(transpiled_js)
	$(browserify) lib/main.js -s GLOBAL >dist/bundle.js

# TODO Fix bundling
#dist/bundle.min.js: $(transpiled_js)
#	$(uglifyjs) -c dist/bundle.js >dist/bundle.min.js

src_styles := $(shell find src/styles/ -name '*.less')

transpiled_styles := $(patsubst src/styles/%.less,lib/styles/%.lessc,$(src_styles))

lib/styles/%.lessc: src/styles/%.less
	mkdir -p $(dir $@)
	lessc -sm=on $< >$@

all_styles: $(transpiled_styles)

dist/styles/main.css: all_styles
	$(shell cat src/styles/reset.css $(shell find lib/styles/ -name '*.lessc') >$@)

#dist/generated_query_db.js: $(posts) tools/build_query_index.py
#	$(shell python tools/build_query_index.py public)

final_scripts:
	$(shell cp dist/styles/main.css public/styles/main.css)
	$(shell cp dist/bundle.js public/scripts/bundle.js)
#	$(shell cp dist/bundle.min.js public/scripts/bundle.min.js)

user: clean_some final_scripts

# dist/bundle.min.js
dev: clean_some dist/bundle.js dist/styles/main.css

all: dev final_scripts
