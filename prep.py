#!/usr/bin/python

from subprocess import check_output, call, CalledProcessError
import re

css = '/* LESS PREPROCESSOR OUTPUT, EDITS WILL BE OVERWRITTEN */' +\
    check_output('lessc styles/main.less'.split(' '))

open('styles/main.css', 'w').write(css)


def do_stuff(html_name, html_out_name, js_name):
    data = open(html_name).read().split('<!--WAT-->')

    bef = data[0]
    data = data[1].split('<!--ENDWAT-->')
    scripts = data[0]
    aft = data[1]

    scripts = re.findall('src=[\"\']{1}(.+)[\"\']{1}', scripts)
    print scripts

    combined = []
    for script in scripts:
        combined.append(open(script).read())

    combined = '\n'.join(combined)

    print len(combined)

    open('scripts/%s.js' % js_name, 'w').write(combined)

    # open('scripts/%s.min.js' % js_name, 'w').write(check_output(('uglifyjs scripts/%s.js' % js_name).split(' ')))

    f = open(html_out_name, 'w')
    f.write(bef)
    f.write('<script type="text/javascript" src="scripts/%s.min.js"></script>' % js_name)
    f.write(aft)

do_stuff('dev.html', 'index.html', 'combined')
do_stuff('leaderboard-dev.html', 'leaderboard.html', 'lombined')
print 'exporting refdump'
execfile('pytochrome.py')
print 'done'
