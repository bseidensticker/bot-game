#!/usr/bin/python

from firebase import firebase
import json
import sys

version = '0-3-0'

print 'connecting to firebase'

firebase = firebase.FirebaseApplication('https://fiery-heat-4226.firebaseio.com/', None)

leaderboard = firebase.get('leaderboard/', 'BARRACKS')

print 'got leaderboard'

itemref =  json.loads(open("refdump.json").read()[5:])

killers = {}
killer_list = []

for uid in leaderboard:
    print uid + ' ',
    print 'fetching name: ',
    name = firebase.get('logs/' + uid + '/' + version, 'name')
    if name == None:
        continue
    print name + ' getting death objects:', 
    deaths = firebase.get('logs/' + uid + '/' + version, 'deaths')
    print 'done'

    if deaths == None:
        print 'bad deaths obj'
        continue
    
    for key in deaths:
        killer = str(deaths[key].split('killed by ')[1].split(' at ')[0])
        if(killer in killers):
            killers[killer] += 1
        else:
            killers[killer] = 1
    #break

total_deaths = 0



for killer in itemref['monster']:
    if killer in killers:
        killer_list.append({'name': killer, 'score': killers[killer]})
        total_deaths += killers[killer]
    else:
        killer_list.append({'name': killer, 'score': 0})

killer_list.sort(key=lambda x: -x['score'])

print 'Overall'
print 'Name|Kills|%'
print '---|---|---'

for x in xrange(len(killer_list)):
    print "%s|%d|%.2f%%" % (killer_list[x]['name'], killer_list[x]['score'], (100.0 * killer_list[x]['score'] / total_deaths))
    #print killer_list[x]['name'] + "|" + str(killer_list[x]['score']) + "|(" + str(100.0 * killer_list[x]['score'] / total_deaths) + "%)"

zones = []
for zone in itemref['zoneOrder']['order']:
    print ' '
    print zone
    choices = []
    zone_deaths = 0
    mons = itemref['zone'][zone]['choices']
    mons.append(itemref['zone'][zone]['boss'])
    for choice in mons:
        if choice in killers:
            choices.append({'name': choice, 'score': killers[str(choice)]})
            zone_deaths += killers[str(choice)]
        else :
            choices.append({'name': choice, 'score': 0})
        
    choices.sort(key=lambda x: -x['score'])
    
    print 'Name|Kills|%'
    print '---|---|---'


    if zone_deaths == 0:
        zone_deaths += 1
    
    for x in xrange(len(choices)):
        print '%s|%d|%.2f%%' % (choices[x]['name'], choices[x]['score'], (100.0 * choices[x]['score'] / zone_deaths))
        #print choices[x]['name'] + "|" + str(choices[x]['score']) + "|(" + str(100.0 * choices[x]['score'] / zone_deaths) + "%)"
    zones.append({'name': zone, 'score': zone_deaths})

zones.sort(key=lambda x: -x['score'])
print ''    
print 'Overall'
print 'Name|Kills|%'
print '---|---|---'

for x in xrange(len(zones)):
    print '%s|%d|%.2f%%' % (zones[x]['name'], zones[x]['score'], 100.0 * zones[x]['score'] / total_deaths)
    
