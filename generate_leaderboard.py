#!/usr/bin/python

from firebase import firebase
import json
import sys
import re

version = '0-3-0'
racename = 'BARRACKS'
baseLeaderboardPositions = 11

print 'connecting to firebase'
firebase = firebase.FirebaseApplication('https://fiery-heat-4226.firebaseio.com/', None)

leaderboard = firebase.get('leaderboard/', racename)

print 'got leaderboard'
print leaderboard

leaderlist = [];

for uid in leaderboard:
    splits = leaderboard[uid].split(':')
    print 'reading ' + str(uid)
    score = int(splits[0])
    zone = leaderboard[uid].split(':')[1]
    if(splits[2] == ''):
        print 'weird'
        print splits
        continue
    leaderlist.append({'uid': uid,
                       'score': int(splits[2]), #int(splits[0]),
                       'zone': str(splits[3]),
                       'name': splits[1],
                       'level': int(splits[2]),
                   })

leaderlist.sort(key=lambda x: -x['score'])            

lastScore = -1
lastRank = -1
finalLeaders = [];

print 'sorted'

for user in leaderlist:
    if re.search('[a-zA-Z]', str(user['uid'])):
        continue
    if len(finalLeaders) >= 10 and user['score'] != lastScore:
        break
    if user['score'] == lastScore:
        user['rank'] = lastRank
    else:
        user['rank'] = len(finalLeaders) + 1
        lastRank = len(finalLeaders) + 1
    finalLeaders.append(user)
    lastScore = user['score']


finStr = 'Pos | Level | Name | Highest Zone\n'
finStr += '---|---|---|---\n'

for user in finalLeaders:
    finStr += str(user['rank']) + '|' + str(user['score']) + '|' + user['name'] +  '|' + user['zone'] + '\n'

print 'result: '
print ''
print finStr
    
