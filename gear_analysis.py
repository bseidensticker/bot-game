#!/usr/bin/python

from firebase import firebase
import json
import sys

version = '0-3-0'

print 'connecting to firebase'
firebase = firebase.FirebaseApplication('https://fiery-heat-4226.firebaseio.com/', None)

leaderboard = firebase.get('leaderboard/', 'BARRACKS');

print 'got leaderboard'

itemref =  json.loads(open("refdump.json").read()[5:])


gear = {}
gear_list = []
weapon_list = []
armor_list = []
skill_list = []
card_list = []
total_clears = 0

for uid in leaderboard:
    print 'fetching name: ',
    name = firebase.get('logs/' + uid + '/' + version, 'name')
    if name == None:
        print 'no name'
        continue
    print name + ' getting clear objects:', 
    clears = firebase.get('logs/' + uid + '/' + version, 'clearLogs')
    print 'done'
    if clears == None:
        print 'no clears'
        continue
    
    
    for clear in clears:
        gears = clear.split(':')[3].split(',');
        total_clears += 1
        for thing in gears:
            if thing in gear: 
                gear[thing]['count'] += 1
                gear[thing]['score'] += int(clear.split(':')[1])
            else:
                gear[thing] = {'name': thing, 'count': 1, 'score': int(clear.split(':')[1])}
    #break


    
for thing in gear:
    gear_list.append(gear[thing])

for weapon in itemref['weapon']:
    name = weapon
    if (not name in gear):
        gear_list.append({'name': name, 'count': 0, 'score': 0})
        weapon_list.append({'name': name, 'count': 0, 'score': 0})
    else:
        weapon_list.append({'name': name, 'count': gear[name]['count'], 'score': gear[name]['score']})
        
for armor in itemref['armor']:
    name = armor
    if (not name in gear):
        gear_list.append({'name': name, 'count': 0, 'score': 0})
        armor_list.append({'name': name, 'count': 0, 'score': 0})
    else:
        armor_list.append({'name': name, 'count': gear[name]['count'], 'score': gear[name]['score']})
for skill in itemref['skill']:
    name = skill
    if (not name in gear):
        gear_list.append({'name': name, 'count': 0, 'score': 0})
        skill_list.append({'name': name, 'count': 0, 'score': 0})
    else:
        skill_list.append({'name': name, 'count': gear[name]['count'], 'score': gear[name]['score']})
for card in itemref['card']:
    name = card
    if name[:5] == "proto":
        continue
    if (not name in gear):
        gear_list.append({'name': name, 'count': 0, 'score': 0})
        card_list.append({'name': name, 'count': 0, 'score': 0})
    else:
        card_list.append({'name': name, 'count': gear[name]['count'], 'score': gear[name]['score']})
        
#gear_list.sort(key=lambda x: -x['score'])
#
#print 'All Gear'
#print 'Name|Score|Pct Used'
#print '---|---|---'

#for x in xrange(len(gear_list)):
    #print '%s|%d|%.2f%%' % (gear_list[x]['name'], gear_list[x]['score'], (100.0 * gear_list[x]['count'] / total_clears))

weapon_list.sort(key=lambda x: -x['score'])
    
print 'Weapons'
print 'Name|Score|Pct Used'
print '---|---|---'

for x in xrange(len(weapon_list)):
        print '%s|%d|%.2f%%' % (weapon_list[x]['name'], weapon_list[x]['score'], (100.0 * weapon_list[x]['count'] / total_clears))

armor_list.sort(key=lambda x: -x['score'])

print 'Armors'
print 'Name|Score|Pct Used'
print '---|---|---'

for x in xrange(len(armor_list)):
            print '%s|%d|%.2f%%' % (armor_list[x]['name'], armor_list[x]['score'], (100.0 * armor_list[x]['count'] / total_clears))

skill_list.sort(key=lambda x: -x['score'])

print 'Skills'
print 'Name|Score|Pct Used'
print '---|---|---'

for x in xrange(len(skill_list)):
            print '%s|%d|%.2f%%' % (skill_list[x]['name'], skill_list[x]['score'], (100.0 * skill_list[x]['count'] / total_clears))

card_list.sort(key=lambda x: -x['score'])

print 'Cards'
print 'Name|Score|Pct Used'
print '---|---|---'

for x in xrange(len(card_list)):
            print '%s|%d|%.2f%%' % (card_list[x]['name'], card_list[x]['score'], (100.0 * card_list[x]['count'] / total_clears))
