#!/usr/bin/python

from firebase import firebase
import json
import sys

#try:
#    filename = sys.argv[1]
#except:
#    print 'No args given, defaulting to log.json'
#    filename = 'log.json'
uid = '33555816'
version = '0-2-25'
raceName = 'WICKEDWEEK'
maxPositiveLocal = 100000


firebase = firebase.FirebaseApplication('https://fiery-heat-4226.firebaseio.com/', None);
leaderboard = firebase.get('leaderboard/', raceName)



def accurate_local_time(time):
    times = []
    for sid in time:
        if type(sid) != 'int':
            print 'weird one'
            break;
        records = time[sid]
        for gltime in records:
            try:
                lt = gltime['local-time']
                st = gltime['servertime']
            except:
                lt = records[gltime]['local-time']
                st = records[gltime]['servertime']
            if(lt - st > maxPositiveLocal or lt - st < -maxPositiveLocal):
                times.append(lt - st)

    return times

def check_gltime_hack(time):
    ret = []
    for sid in time:
        records = time[sid]
        if type(records) == type([]):
            continue
        first = sorted([int(d['local-time']) for gltime, d in records.iteritems()])[0]
        results = [(int(gltime) / 1000,
                    (int(d['local-time']) - first) / 1000,
                    (int(d['servertime']) - first) / 1000)
                   for gltime, d in records.iteritems()]
        for x in results:
            ret.append(x)

    return ret

for uid in leaderboard:
    time = firebase.get('logs/' + uid + '/' + version + '/', 'time')
    if time == None:
        continue
    acc = accurate_local_time(time)
    gl = check_gltime_hack(time)
    name = firebase.get('logs/' + uid + '/' + version, 'name')
    if name == None:
        continue
    print name + " ",
    if len(acc) != 0:
        print uid,
        print " - accurate local times?: ",
        print acc
    else:
        print "ok"
    #print "gl.time hacking?: ",
    #print gl
